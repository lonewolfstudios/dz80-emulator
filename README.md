# DZ80 Emulator

This is an emulator written to test code for my DZ80 SBC. It includes an emulator for the Z80 and its two busses. Additional I/O devices could be developed and added. This source is released under a give me credit license, but you can feel free to use it in your own projects.
