// 64Emul Main.cpp file
// This file includes WinMain and provides the functionality and basic
// operations for the emulator.

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <imgui.h>
#include "Windows/GUI.h"
//#include "Input/KeyMap.h"

const char windowName[] = "DZ80 Emulator";

static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_F12 && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

int main(int argc, char* argv[])
{
	Console* theConsole = Console::Instance();
	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	window = glfwCreateWindow(1280, 1024, windowName, NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	//KeyMap::Initialize();

	int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	if (!status)
	{
		glfwTerminate();
		fprintf(stderr, "Failed to load OpenGL!");
		getchar();
		exit(EXIT_FAILURE);
	}
	GUI::Instance()->InitializeGUI(window, theConsole);
	glfwSwapInterval(1);
	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT);
		if (GUI::Instance()->DrawGUI())
		{
			glfwSetWindowShouldClose(window, GLFW_TRUE);
		}
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	GUI::Instance()->ShutdownGUI();
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}