#pragma once

#include <cstring>

class InstructionDecoder
{

private:
	static InstructionDecoder* _instance;
	InstructionDecoder();

public:
	static InstructionDecoder* Instance() { if (_instance == nullptr) { _instance = new InstructionDecoder(); } return _instance; }
	unsigned char Decode(char* disassembly, unsigned short addr, int& cycles, int& bytes, bool& undocumented, bool& legal);
};