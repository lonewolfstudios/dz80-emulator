#include "FileReader.h"
#include "FileStream.h"
#include <sstream>
#include <string>
#include <vector>

#define INTEL_MAX_ENTRY_SIZE	524

unsigned char* FileReader::ReadBinary(char* inFile)
{
	ifstream fin;
	fin.open(inFile, std::ios::in | std::ios::binary);
	if (!fin.is_open())
	{
		return nullptr;
	}
	auto length = fin.length();
	unsigned char* data = new unsigned char[8192];
	memset(data, 0, 8192);
	fin.read((char*)data, length < 8192 ? length : 8192);
	fin.close();
	return data;
}

unsigned char* FileReader::ReadIntel(char* inFile, unsigned short fileOffset)
{
	ifstream fin(inFile);
	if (!fin.is_open())
		return nullptr;
	char buff[INTEL_MAX_ENTRY_SIZE];

	unsigned char* data = new unsigned char[8192];
	memset(data, 0, 8192);

	while (true)
	{
		std::vector<unsigned char> inData;
		fin.getline(buff, INTEL_MAX_ENTRY_SIZE);
		if (!fin) break; //End of the file
		std::string str(buff);

		unsigned char count = 2 * (unsigned char)HexToNum(str.substr(1, 2));
		unsigned char lowAddress = (unsigned char)HexToNum(str.substr(5, 2));
		unsigned char highAddress = (unsigned char)HexToNum(str.substr(3, 2));
		unsigned short address = (highAddress << 8) | lowAddress;
		unsigned char recordType = (unsigned char)HexToNum(str.substr(7, 2));
		if (count != str.length() - 12 || recordType > 1) //We will not support anything but 0 (data) and 1(eof)
		{
			fin.close();
			delete[] data;
			return nullptr;
		}
		//if (recordType == 1) break;
		unsigned char check = (count / 2) + lowAddress + highAddress + recordType;
		for (int i = 0; i < count; i += 2)
		{
			unsigned char v = (unsigned char)HexToNum(str.substr(9 + i, 2));
			inData.push_back(v);
			check += v;
		}
		unsigned char checksum = (unsigned char)HexToNum(str.substr(str.length() - 3, 2));
		if ((unsigned char)(check + checksum) != (unsigned char)0)
		{
			fin.close();
			delete[] data;
			return nullptr;
		}
		if (count > 0)
		{
			//We have now parsed a line of the file, let's place it in the buffer
			if (address - fileOffset < 8192 && (address + inData.size() - fileOffset) < 8192)
			{
				unsigned short start = address - fileOffset;
				for (int i = 0; i < inData.size(); ++i)
				{
					data[start++] = inData[i];
				}
			}
			else
			{
				fin.close();
				delete[] data;
				return nullptr;
			}
		}
	}
	fin.close();
	return data;
}

uint64_t FileReader::HexToNum(std::string s)
{
	std::stringstream str;
	str << std::hex << s;
	uint64_t out;
	str >> out;
	return out;
}

unsigned char* FileReader::ReadFile(char* inFile, unsigned short fileOffset, FileReader::FileReaderType type)
{
	if (type == FileReaderType::BINARY)
		return ReadBinary(inFile);
	else if (type == FileReaderType::INTEL_HEX)
		return ReadIntel(inFile, fileOffset);
	return nullptr;
}
