#include "InstructionDecoder.h"
#include "SystemBus/SystemBus.h"

InstructionDecoder* InstructionDecoder::_instance = nullptr;

#define ReadMemory(a) (SystemBus::Instance()->AddressBus()->ReadAddress(a))
#define ReadNN(addr) \
	nn = ReadMemory(addr + bytes); \
	bytes++; \
	nn |= ((unsigned short)ReadMemory(addr + bytes) << 8); \
	bytes++;

InstructionDecoder::InstructionDecoder()
{
}

char* cc[] =
{
	"NZ", "Z", "NC", "C", "PO", "PE", "P", "M"
};

char* r[] =
{
	"B", "C", "D", "E", "H", "L", "(HL)", "A"
};

char* rp[] =
{
	"BC", "DE", "HL", "SP"
};

char* rp2[] =
{
	"BC", "DE", "HL", "AF"
};

char* alu[] =
{
	"ADD A,", "ADC A,", "SUB", "SBC A,", "AND", "XOR", "OR", "CP"
};

char* rot[] =
{
	"RLC", "RRC", "RL", "RR", "SLA", "SRA", "SLL", "SRL"
};

char* im[] =
{
	"0", "0/1", "1", "2", "0", "0/1", "1", "2"
};

char* bli[4][4] =
{
	{ "LDI", "CPI", "INI", "OUTI"},
	{ "LDD", "CPD", "IND", "OUTD"},
	{ "LDIR", "CPIR", "INIR", "OTIR"},
	{ "LDDR", "CPDR", "INDR", "OTDR"}
};

char* idx[] =
{
	"IX", "IY"
};

char* r_repl[2][8] =
{
	{"B", "C", "D", "E", "IXH", "IXL", "(HL)", "A"},
	{"B", "C", "D", "E", "IYH", "IYL", "(HL)", "A"}
};

char* rp_repl[2][4] =
{
	{"BC", "DE", "IX", "SP"},
	{"BC", "DE", "IY", "SP"}
};

char* rp2_repl[2][4] =
{
	{"BC", "DE", "IX", "AF"},
	{"BC", "DE", "IY", "AF"}
};

unsigned char InstructionDecoder::Decode(char* disassembly, unsigned short addr, int& cycles, int& bytes, bool& undocumented, bool& legal)
{
	unsigned char instr = ReadMemory(addr);
	unsigned char additionalCycles = 0;
	bytes = 1;
	undocumented = false;
	legal = true;
	unsigned short prefix = 0;
	if (instr == 0xdd || instr == 0xfd) //Take care of DD and FD prefixes, including DD CB and FD CB
	{
		unsigned char nextInstr = ReadMemory(addr + bytes);
		if (nextInstr == 0xdd || nextInstr == 0xed || nextInstr == 0xfd)
		{
			sprintf(disassembly, "NONI");
			cycles = 4;
			undocumented = true;
			legal = true;
		}
		else if (nextInstr == 0xcb)
		{
			prefix = (unsigned short)instr << 8 | nextInstr;
			bytes++;
			instr = ReadMemory(addr + bytes);
			bytes++;
		}
		else
		{
			prefix = instr;
			bytes++;
			instr = nextInstr;
		}
	}
	else if (instr == 0xcb) //Take care of CB xx instructions
	{
		prefix = 0xcb;
		instr = ReadMemory(addr + bytes);
		bytes++;
	}
	//Now, we are ready to decode the actual instruction
	unsigned char x = instr >> 6;
	unsigned char y = (instr >> 3) & 0x07;
	unsigned char z = instr & 0x07;
	unsigned char p = y >> 1;
	unsigned char q = y & 1;
	unsigned char d;
	unsigned char n;
	unsigned short nn;
	if (prefix == 0) //Unprefixed opcodes
	{
		switch (x)
		{
		case 0:
			switch (z)
			{
			case 0:
				switch (y)
				{
				case 0:
					sprintf(disassembly, "NOP");
					cycles = 4;
					break;
				case 1:
					sprintf(disassembly, "EX AF, AF'");
					cycles = 4;
					break;
				case 2:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "DJNZ %.4x", addr + (char)d + bytes);
					//TODO: Do we want to check if the jump will be taken?
					cycles = 13;
					break;
				case 3:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "JR %.4x", addr + (char)d + bytes);
					cycles = 12;
					break;
				default:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "JR %s, %.4x", cc[y - 4], addr + (char)d + bytes);
					//TODO: Do we want to check if the jump will be taken?
					cycles = 13;
					break;
				}
				break;
			case 1:
				switch (q)
				{
				case 0:
					ReadNN(addr);
					cycles = 10;
					sprintf(disassembly, "LD %s, %.4x", rp[p], nn);
					break;
				case 1:
					cycles = 15;
					sprintf(disassembly, "ADD HL, %s", rp[p]);
					break;
				}
				break;
			case 2:
				switch (q)
				{
				case 0:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "LD (BC), A");
						cycles = 7;
						break;
					case 1:
						sprintf(disassembly, "LD (DE), A");
						cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						sprintf(disassembly, "LD (%.4x), HL", nn);
						cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						sprintf(disassembly, "LD (%.4x), A", nn);
						cycles = 13;
						break;
					}
					break;
				case 1:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "LD A, (BC)");
						cycles = 7;
						break;
					case 1:
						sprintf(disassembly, "LD A, (DE)");
						cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						sprintf(disassembly, "LD HL, (%.4x)", nn);
						cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						sprintf(disassembly, "LD A, (%.4x)", nn);
						cycles = 13;
						break;
					}
					break;
				}
				break;
			case 3:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "INC %s", rp[p]);
					cycles = 6;
					break;
				case 1:
					sprintf(disassembly, "DEC %s", rp[p]);
					cycles = 6;
					break;
				}
				break;
			case 4:
				sprintf(disassembly, "INC %s", r[y]);
				cycles = 4;
				break;
			case 5:
				sprintf(disassembly, "DEC %s", r[y]);
				cycles = 4;
				break;
			case 6:
				n = ReadMemory(addr + bytes);
				bytes++;
				sprintf(disassembly, "LD %s, %.2x", r[y], n);
				cycles = 7;
				break;
			case 7:
				switch (y)
				{
				case 0:
					sprintf(disassembly, "RLCA");
					cycles = 4;
					break;
				case 1:
					sprintf(disassembly, "RRCA");
					cycles = 4;
					break;
				case 2:
					sprintf(disassembly, "RLA");
					cycles = 4;
					break;
				case 3:
					sprintf(disassembly, "RRA");
					cycles = 4;
					break;
				case 4:
					sprintf(disassembly, "DAA");
					cycles = 4;
					break;
				case 5:
					sprintf(disassembly, "CPL");
					cycles = 4;
					break;
				case 6:
					sprintf(disassembly, "SCF");
					cycles = 4;
					break;
				case 7:
					sprintf(disassembly, "CCF");
					cycles = 4;
					break;
				}
				break;
			}
			break;
		case 1:
			if (z == 6 && y == 6)
			{
				sprintf(disassembly, "HALT");
				cycles = 4;
			}
			else
			{
				sprintf(disassembly, "LD %s, %s", r[y], r[z]);
				cycles = 4;
			}
			break;
		case 2:
			sprintf(disassembly, "%s %s", alu[y], r[z]);
			cycles = 4;
			break;
		case 3:
			switch (z)
			{
			case 0:
				sprintf(disassembly, "RET %s", cc[y]);
				cycles = 11;
				break;
			case 1:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "POP %s", rp2[p]);
					cycles = 10;
					break;
				case 1:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "RET");
						cycles = 10;
						break;
					case 1:
						sprintf(disassembly, "EXX");
						cycles = 4;
						break;
					case 2:
						sprintf(disassembly, "JP (HL)");
						cycles = 4;
						break;
					case 3:
						sprintf(disassembly, "LD SP, HL");
						cycles = 6;
						break;
					}
					break;
				}
				break;
			case 2:
				ReadNN(addr);
				sprintf(disassembly, "JP %s, %.4x", cc[y], nn);
				cycles = 10;
				break;
			case 3:
				switch (y)
				{
				case 0:
					ReadNN(addr);
					sprintf(disassembly, "JP %.4x", nn);
					cycles = 10;
					break;
				case 1:
					//This is the CB prefix and we should never hit this!
					break;
				case 2:
					n = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "OUT (%.2x), A", n);
					cycles = 11;
					break;
				case 3:
					n = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "IN A, (%.2x)", n);
					cycles = 11;
					break;
				case 4:
					sprintf(disassembly, "EX (SP), HL");
					cycles = 4;
					break;
				case 5:
					sprintf(disassembly, "EX DE, HL");
					cycles = 4;
					break;
				case 6:
					sprintf(disassembly, "DI");
					cycles = 4;
					break;
				case 7:
					sprintf(disassembly, "EI");
					cycles = 4;
					break;
				}
				break;
			case 4:
				ReadNN(addr);
				sprintf(disassembly, "CALL %s, %.4x", cc[y], nn);
				cycles = 17;
				break;
			case 5:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "PUSH %s", rp2[p]);
					cycles = 11;
					break;
				case 1:
					switch (p)
					{
					case 0:
						ReadNN(addr);
						sprintf(disassembly, "CALL %.4x", nn);
						cycles = 17;
						break;
					default: //These are the DD, ED, and FD prefix that we should never reach
						break;
					}
					break;
				}
				break;
			case 6:
				n = ReadMemory(addr + bytes);
				bytes++;
				sprintf(disassembly, "%s %.2x", alu[y], n);
				cycles = 4;
				break;
			case 7:
				sprintf(disassembly, "RST %.2x", y * 8);
				cycles = 11;
				break;
			}
			break;
		}
	}
	else if (prefix == 0xcb)
	{
		switch (x)
		{
		case 0:
			sprintf(disassembly, "%s %s", rot[y], r[z]);
			cycles = 8;
			break;
		case 1:
			sprintf(disassembly, "BIT %d, %s", y, r[z]);
			cycles = 8;
			break;
		case 2:
			sprintf(disassembly, "RES %d, %s", y, r[z]);
			cycles = 8;
			break;
		case 3:
			sprintf(disassembly, "SET %d, %s", y, r[z]);
			cycles = 8;
			break;
		}
	}
	else if (prefix == 0xed) //Opcodes with JUST an ED prefix
	{
		switch (x)
		{
		case 0:
		case 3: //These are invalid
			legal = false;
			undocumented = true;
			cycles = 4;
			sprintf(disassembly, "NONI");
			break;
		case 1:
			switch (z)
			{
			case 0:
				if (y == 6)
				{
					sprintf(disassembly, "IN (C)");
					cycles = 12;
				}
				else
				{
					sprintf(disassembly, "IN %s, (C)", r[y]);
					cycles = 12;
				}
				break;
			case 1:
				if (y == 6)
				{
					sprintf(disassembly, "OUT (C), 0");
					cycles = 12;
				}
				else
				{
					sprintf(disassembly, "OUT (C), %s", r[y]);
					cycles = 12;
				}
				break;
			case 2:
				if (q == 0)
				{
					sprintf(disassembly, "SBC HL, %s", rp[p]);
					cycles = 15;
				}
				else
				{
					sprintf(disassembly, "ADC HL, %s", rp[p]);
					cycles = 15;
				}
				break;
			case 3:
				ReadNN(addr);
				if (q == 0)
				{
					sprintf(disassembly, "LD (%x), %s", nn, rp[p]);
					cycles = 15;
				}
				else
				{
					sprintf(disassembly, "LD %s, (%x)", rp[p], nn);
					cycles = 15;
				}
				break;
			case 4:
				sprintf(disassembly, "NEG");
				cycles = 8;
				break;
			case 5:
				if (y == 1)
				{
					sprintf(disassembly, "RETI");
					cycles = 14;
				}
				else
				{
					sprintf(disassembly, "RETN");
					cycles = 14;
				}
				break;
			case 6:
				sprintf(disassembly, "IM %s", im[y]);
				cycles = 8;
				break;
			case 7:
				switch (y)
				{
				case 0:
					sprintf(disassembly, "LD I, A");
					cycles = 9;
					break;
				case 1:
					sprintf(disassembly, "LD R, A");
					cycles = 9;
					break;
				case 2:
					sprintf(disassembly, "LD A, I");
					cycles = 9;
					break;
				case 3:
					sprintf(disassembly, "LD A, R");
					cycles = 9;
					break;
				case 4:
					sprintf(disassembly, "RRD");
					cycles = 9; //?
					break;
				case 5:
					sprintf(disassembly, "RLD");
					cycles = 9;
					break;
				case 6:
					sprintf(disassembly, "NOP");
					cycles = 4; //?
					break;
				case 7:
					sprintf(disassembly, "NOP");
					cycles = 4; //?
					break;
				}
				break;
			}
			break;
		case 2:
			if (z < 4 && y > 3)
			{
				sprintf(disassembly, bli[y][z]);
				cycles = 16;
			}
			else
			{
				sprintf(disassembly, "NONI");
				cycles = 4;
			}
			break;
		}
	}
	else if (prefix == 0xdd || prefix == 0xfd)
	{
		unsigned char repl = (prefix == 0xdd) ? 0 : 1;

		switch (x)
		{
		case 0:
			switch (z)
			{
			case 0:
				switch (y)
				{
				case 0:
					sprintf(disassembly, "NOP");
					cycles = 4;
					break;
				case 1:
					sprintf(disassembly, "EX AF, AF'");
					cycles = 4;
					break;
				case 2:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "DJNZ %.4x", addr + (char)d + bytes);
					//TODO: Do we want to check if the jump will be taken?
					cycles = 13;
					break;
				case 3:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "JR %.4x", addr + (char)d + bytes);
					cycles = 12;
					break;
				default:
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "JR %s, %.4x", cc[y - 4], addr + (char)d + bytes);
					//TODO: Do we want to check if the jump will be taken?
					cycles = 13;
					break;
				}
				break;
			case 1:
				switch (q)
				{
				case 0:
					ReadNN(addr);
					cycles = 10;
					sprintf(disassembly, "LD %s, %.4x", rp_repl[repl][p], nn);
					break;
				case 1:
					cycles = 15;
					sprintf(disassembly, "ADD HL, %s", rp_repl[repl][p]);
					break;
				}
				break;
			case 2:
				switch (q)
				{
				case 0:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "LD (BC), A");
						cycles = 7;
						break;
					case 1:
						sprintf(disassembly, "LD (DE), A");
						cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						sprintf(disassembly, "LD (%.4x), HL", nn);
						cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						sprintf(disassembly, "LD (%.4x), A", nn);
						cycles = 13;
						break;
					}
					break;
				case 1:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "LD A, (BC)");
						cycles = 7;
						break;
					case 1:
						sprintf(disassembly, "LD A, (DE)");
						cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						sprintf(disassembly, "LD HL, (%.4x)", nn);
						cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						sprintf(disassembly, "LD A, (%.4x)", nn);
						cycles = 13;
						break;
					}
					break;
				}
				break;
			case 3:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "INC %s", rp_repl[repl][p]);
					cycles = 6;
					break;
				case 1:
					sprintf(disassembly, "DEC %s", rp_repl[repl][p]);
					cycles = 6;
					break;
				}
				break;
			case 4:
				if (y == 6)
				{
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "INC (%s+%.2x)", idx[repl], d);
				}
				else
				{
					sprintf(disassembly, "INC %s", r_repl[repl][y]);
				}
				cycles = 4;
				break;
			case 5:
				if (y == 6)
				{
					d = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "DEC (%s+%.2x)", idx[repl], d);
				}
				else
				{
					sprintf(disassembly, "DEC %s", r_repl[repl][y]);
				}
				cycles = 4;
				break;
			case 6:
				if (y == 6)
				{
					d = ReadMemory(addr + bytes);
					bytes++;
				}
				n = ReadMemory(addr + bytes);
				bytes++;
				if (y == 6)
				{
					sprintf(disassembly, "LD (%s+%.2x), %.2x", idx[repl], d, n);
				}
				else
				{
					sprintf(disassembly, "LD %s, %.2x", r_repl[repl][y], n);
				}
				cycles = 7;
				break;
			case 7:
				switch (y)
				{
				case 0:
					sprintf(disassembly, "RLCA");
					cycles = 4;
					break;
				case 1:
					sprintf(disassembly, "RRCA");
					cycles = 4;
					break;
				case 2:
					sprintf(disassembly, "RLA");
					cycles = 4;
					break;
				case 3:
					sprintf(disassembly, "RRA");
					cycles = 4;
					break;
				case 4:
					sprintf(disassembly, "DAA");
					cycles = 4;
					break;
				case 5:
					sprintf(disassembly, "CPL");
					cycles = 4;
					break;
				case 6:
					sprintf(disassembly, "SCF");
					cycles = 4;
					break;
				case 7:
					sprintf(disassembly, "CCF");
					cycles = 4;
					break;
				}
				break;
			}
			break;
		case 1:
			if (z == 6 && y == 6)
			{
				sprintf(disassembly, "HALT");
				cycles = 4;
			}
			else
			{
				if (y == 6 || z == 6)
				{
					d = ReadMemory(addr + bytes);
					bytes++;
				}
				if (y == 6)
				{
					sprintf(disassembly, "LD (%s+%.2x), %s", idx[repl], d, r[z]);
				}
				else if (z == 6)
				{
					sprintf(disassembly, "LD %s, (%s+%.2x)", r[y], idx[repl], d);
				}
				else
					sprintf(disassembly, "LD %s, %s", r_repl[repl][y], r_repl[repl][z]);
				cycles = 4;
			}
			break;
		case 2:
			if (z == 6)
			{
				d = ReadMemory(addr + bytes);
				bytes++;
				sprintf(disassembly, "%s, (%s+%.2x)", alu[y], idx[repl], d);
			}
			else
			{
				sprintf(disassembly, "%s %s", alu[y], r_repl[repl][z]);
			}
			cycles = 4;
			break;
		case 3:
			switch (z)
			{
			case 0:
				sprintf(disassembly, "RET %s", cc[y]);
				cycles = 11;
				break;
			case 1:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "POP %s", rp2_repl[repl][p]);
					cycles = 10;
					break;
				case 1:
					switch (p)
					{
					case 0:
						sprintf(disassembly, "RET");
						cycles = 10;
						break;
					case 1:
						sprintf(disassembly, "EXX");
						cycles = 4;
						break;
					case 2:
						sprintf(disassembly, "JP (HL)");
						cycles = 4;
						break;
					case 3:
						sprintf(disassembly, "LD SP, HL");
						cycles = 6;
						break;
					}
					break;
				}
				break;
			case 2:
				ReadNN(addr);
				sprintf(disassembly, "JP %s, %.4x", cc[y], nn);
				cycles = 10;
				break;
			case 3:
				switch (y)
				{
				case 0:
					ReadNN(addr);
					sprintf(disassembly, "JP %.4x", nn);
					cycles = 10;
					break;
				case 1:
					//This is the CB prefix and we should never hit this!
					break;
				case 2:
					n = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "OUT (%.2x), A", n);
					cycles = 11;
					break;
				case 3:
					n = ReadMemory(addr + bytes);
					bytes++;
					sprintf(disassembly, "IN A, (%.2x)", n);
					cycles = 11;
					break;
				case 4:
					sprintf(disassembly, "EX (SP), HL");
					cycles = 4;
					break;
				case 5:
					sprintf(disassembly, "EX DE, HL");
					cycles = 4;
					break;
				case 6:
					sprintf(disassembly, "DI");
					cycles = 4;
					break;
				case 7:
					sprintf(disassembly, "EI");
					cycles = 4;
					break;
				}
				break;
			case 4:
				ReadNN(addr);
				sprintf(disassembly, "CALL %s, %.4x", cc[y], nn);
				cycles = 17;
				break;
			case 5:
				switch (q)
				{
				case 0:
					sprintf(disassembly, "PUSH %s", rp2_repl[repl][p]);
					cycles = 11;
					break;
				case 1:
					switch (p)
					{
					case 0:
						ReadNN(addr);
						sprintf(disassembly, "CALL %.4x", nn);
						cycles = 17;
						break;
					default: //These are the DD, ED, and FD prefix that we should never reach
						break;
					}
					break;
				}
				break;
			case 6:
				n = ReadMemory(addr + bytes);
				bytes++;
				sprintf(disassembly, "%s %.2x", alu[y], n);
				cycles = 4;
				break;
			case 7:
				sprintf(disassembly, "RST %.2x", y * 8);
				cycles = 11;
				break;
			}
			break;
		}
	}
	else if (prefix == 0xddcb || prefix == 0xfdcb)
	{
		unsigned char repl = ((prefix & 0xdd00) == 0xdd00 ? 0 : 1);
		d = ReadMemory(addr + bytes); //These all use a displacement
		bytes++;
		switch (x)
		{
		case 0:
			if (z != 6)
			{
				sprintf(disassembly, "LD %s, %s (%s+%.2x)", r[z], rot[y], idx[repl], d);
			}
			else
			{
				sprintf(disassembly, "%s (%s+%.2x)", rot[y], idx[repl], d);
			}
			cycles = 8;
			break;
		case 1:
			sprintf(disassembly, "BIT %d, (%s+%.2x)", y, idx[repl], d);
			cycles = 8;
			break;
		case 2:
			if (z == 6)
			{
				sprintf(disassembly, "RES %d, (%s+%.2x)", y, idx[repl], d);
			}
			else
			{
				sprintf(disassembly, "LD %s, RES %d, (%s+%.2x)", r[z], y, idx[repl], d);
			}
			cycles = 8;
			break;
		case 3:
			if (z == 6)
			{
				sprintf(disassembly, "SET %d, (%s+%.2x)", y, idx[repl], d);
			}
			else
			{
				sprintf(disassembly, "LD %s, SET %d, (%s+%.2x)", r[z], y, idx[repl], d);
			}
			cycles = 8;
			break;
		}
	}
	return bytes;
}
