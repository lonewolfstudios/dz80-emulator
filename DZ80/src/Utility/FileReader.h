#pragma once

#include <string>

class FileReader
{
private:

	static unsigned char* ReadBinary(char* inFile);
	static unsigned char* ReadIntel(char* inFile, unsigned short fileOffset);
	static uint64_t HexToNum(std::string s);

public:

	enum class FileReaderType { BINARY, INTEL_HEX };

	static unsigned char* ReadFile(char* inFile, unsigned short fileOffset, FileReaderType type);
	
};

