#pragma once

#include "DecoderStructs.h"
#include <SystemBus/Bus.h>

class Z80;

class Decoder
{

private:
	static Decoder* _instance;
	Decoder();

public:
	static Decoder* Instance() { if (_instance == nullptr) { _instance = new Decoder(); } return _instance; }
	InstructionInfo Decode(unsigned short addr, Z80* z80, Bus* AddressBus);

};