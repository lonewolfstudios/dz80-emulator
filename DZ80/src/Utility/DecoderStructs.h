#pragma once

enum DataSource
{
	Implied,
	Direct,
	A,
	B,
	C,
	D,
	E,
	H,
	L,
	HL8,
	IXH,
	IXL,
	IYH,
	IYL,
	Mem8,
	I,
	R,
	AF,
	BC,
	DE,
	HL,
	IX,
	IY,
	SP,
	Mem16
};

enum Condition
{
	None,
	NZ,
	Z,
	NC,
	C,
	PO,
	PE,
	P,
	M
};

enum OpCode
{
	ADC, ADD, AND, CP, OR, SBC, SUB, XOR,
	RLC, RRC, RL, RR, SLA, SRA, SLL, SRL,
	LDI, CPI, INI, OUTI, LDD, CPD, IND, OUTD, LDIR, CPIR, INIR, OTIR, LDDR, CPDR, INDR, OTDR,
	IM, NOP, EX,
	DJNZ, JR, LD, INC, DEC, RLCA, RRCA, RLA, RRA, DAA, CPL, SCF, CCF, HALT, RET, POP, PUSH, EXX,
	JP, OUT, IN, DI, EI, CALL, RST, BIT, RES, SET, NONI, NEG, RETI, RETN,
	RRD, RLD,
	JAM
};

typedef struct
{
public:
	OpCode opcode = JAM;
	OpCode secOpcode = JAM;
	unsigned char cycles = 0;
	unsigned char bytes = 1;
	int data = 0;
	unsigned char data2 = 0;
	DataSource source = DataSource::Implied;
	DataSource destination = DataSource::Implied;
	Condition condition = None;
} InstructionInfo;