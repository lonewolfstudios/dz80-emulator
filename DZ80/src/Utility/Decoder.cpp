#include "Decoder.h"
#include "SystemBus/SystemBus.h"
#include "Z80.h"

Decoder* Decoder::_instance = nullptr;

#define ReadMemory(a) (addressBus->ReadAddress(a))
#define ReadNN(addr) \
	nn = ReadMemory(addr + info.bytes); \
	info.bytes++; \
	nn |= ((unsigned short)ReadMemory(addr + info.bytes) << 8); \
	info.bytes++;

Decoder::Decoder()
{
}

Condition cc[] =
{
	NZ, Z, NC, C, PO, PE, P, M
};

DataSource r[] =
{
	DataSource::B, DataSource::C, DataSource::D, DataSource::E, DataSource::H, DataSource::L, DataSource::HL8, DataSource::A
};

DataSource rp[] =
{
	BC, DE, HL, SP
};

DataSource rp2[] =
{
	BC, DE, HL, AF
};

OpCode alu[] =
{
	ADD, ADC, SUB, SBC, AND, XOR, OR, CP
};

OpCode rot[] =
{
	RLC, RRC, RL, RR, SLA, SRA, SLL, SRL
};

//char* im[] =
//{
//	"0", "0/1", "1", "2", "0", "0/1", "1", "2"
//};

unsigned char im[] =
{
	0, 3, 1, 2, 0, 3, 1, 2
};

OpCode bli[4][4] =
{
	{LDI, CPI, INI, OUTI},
	{LDD, CPD, IND, OUTD},
	{LDIR, CPIR, INIR, OTIR},
	{LDDR, CPDR, INDR, OTDR}
};

DataSource idx[] =
{
	IX, IY
};

DataSource r_repl[2][8] =
{
	{B, DataSource::C, D, E, IXH, IXL, HL, A},
	{B, DataSource::C, D, E, IYH, IYL, HL, A}
};

DataSource rp_repl[2][4] =
{
	{BC, DE, IX, SP},
	{BC, DE, IY, SP}
};

DataSource rp2_repl[2][4] =
{
	{BC, DE, IX, AF},
	{BC, DE, IY, AF}
};

InstructionInfo Decoder::Decode(unsigned short addr, Z80* z80, Bus* addressBus)
{
	InstructionInfo info;
	unsigned char instr = ReadMemory(addr);
	unsigned char additionalCycles = 0;
	info.bytes = 1;
	unsigned short prefix = 0;
	if (instr == 0xdd || instr == 0xfd) //Take care of DD and FD prefixes, including DD CB and FD CB
	{
		unsigned char nextInstr = ReadMemory(addr + info.bytes);
		if (nextInstr == 0xdd || nextInstr == 0xed || nextInstr == 0xfd)
		{
			info.cycles = 4;
		}
		else if (nextInstr == 0xcb)
		{
			prefix = (unsigned short)instr << 8 | nextInstr;
			info.bytes++;
			instr = ReadMemory(addr + info.bytes);
			info.bytes++;
		}
		else
		{
			prefix = instr;
			info.bytes++;
			instr = nextInstr;
		}
	}
	else if (instr == 0xcb) //Take care of CB xx instructions
	{
		prefix = 0xcb;
		instr = ReadMemory(addr + info.bytes);
		info.bytes++;
	}
	//Now, we are ready to decode the actual instruction
	unsigned char x = instr >> 6;
	unsigned char y = (instr >> 3) & 0x07;
	unsigned char z = instr & 0x07;
	unsigned char p = y >> 1;
	unsigned char q = y & 1;
	unsigned char d;
	unsigned char n;
	unsigned short nn;
	if (prefix == 0) //Unprefixed opcodes
	{
		switch (x)
		{
		case 0:
			switch (z)
			{
			case 0:
				switch (y)
				{
				case 0:
					info.opcode = OpCode::NOP;
					info.cycles = 4;
					break;
				case 1:
					info.opcode = OpCode::EX;
					info.source = AF;
					info.destination = AF;
					info.cycles = 4;
					break;
				case 2:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.data = d;
					info.destination = Mem16;
					info.opcode = DJNZ;
					info.cycles = 13;
					break;
				case 3:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = JR;
					info.data = d;
					info.cycles = 12;
					break;
				default:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = JR;
					info.condition = cc[y - 4];
					info.data = d;
					//TODO: Do we want to check if the jump will be taken?
					info.cycles = 13;
					break;
				}
				break;
			case 1:
				switch (q)
				{
				case 0:
					ReadNN(addr);
					info.cycles = 10;
					info.data = nn;
					info.source = rp[p];
					info.destination = Mem16;
					info.opcode = LD;
					break;
				case 1:
					info.cycles = 15;
					info.opcode = ADD;
					info.destination = HL;
					info.source = rp[p];
					break;
				}
				break;
			case 2:
				info.opcode = LD;
				switch (q)
				{
				case 0:
					switch (p)
					{
					case 0:
						info.destination = Mem8;
						info.data = z80->BC();
						info.source = A;
						info.cycles = 7;
						break;
					case 1:
						info.destination = Mem8;
						info.data = z80->DE();
						info.source = A;
						info.cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						info.destination = Mem16;
						info.source = HL;
						info.data = nn;
						//sprintf(disassembly, "LD (%.4x), HL", nn);
						info.cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						info.destination = Mem8;
						info.source = A;
						info.data = nn;
						//sprintf(disassembly, "LD (%.4x), A", nn);
						info.cycles = 13;
						break;
					}
					break;
				case 1:
					switch (p)
					{
					case 0:
						info.source = Mem8;
						info.data = z80->BC();
						info.destination = A;
						//sprintf(disassembly, "LD A, (BC)");
						info.cycles = 7;
						break;
					case 1:
						info.source = Mem8;
						info.data = z80->DE();
						info.destination = A;
						//sprintf(disassembly, "LD A, (DE)");
						info.cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						info.source = Mem16;
						info.data = nn;
						info.destination = HL;
						//sprintf(disassembly, "LD HL, (%.4x)", nn);
						info.cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						info.source = Mem8;
						info.data = nn;
						info.destination = A;
						//sprintf(disassembly, "LD A, (%.4x)", nn);
						info.cycles = 13;
						break;
					}
					break;
				}
				break;
			case 3:
				switch (q)
				{
				case 0:
					info.opcode = INC;
					info.source = rp[p];
					//sprintf(disassembly, "INC %s", rp[p]);
					info.cycles = 6;
					break;
				case 1:
					info.opcode = DEC;
					info.source = rp[p];
					//sprintf(disassembly, "DEC %s", rp[p]);
					info.cycles = 6;
					break;
				}
				break;
			case 4:
				info.opcode = INC;
				info.source = r[y];
				//sprintf(disassembly, "INC %s", r[y]);
				info.cycles = 4;
				break;
			case 5:
				info.opcode = DEC;
				info.source = r[y];
				//sprintf(disassembly, "DEC %s", r[y]);
				info.cycles = 4;
				break;
			case 6:
				n = ReadMemory(addr + info.bytes);
				info.bytes++;
				info.opcode = LD;
				info.source = DataSource::Direct;
				info.data = n;
				info.destination = r[y];
				//sprintf(disassembly, "LD %s, %.2x", r[y], n);
				info.cycles = 7;
				break;
			case 7:
				switch (y)
				{
				case 0:
					info.opcode = RLCA;
					//sprintf(disassembly, "RLCA");
					info.cycles = 4;
					break;
				case 1:
					info.opcode = RRCA;
					//sprintf(disassembly, "RRCA");
					info.cycles = 4;
					break;
				case 2:
					info.opcode = RLA;
					//sprintf(disassembly, "RLA");
					info.cycles = 4;
					break;
				case 3:
					info.opcode = RRA;
					//sprintf(disassembly, "RRA");
					info.cycles = 4;
					break;
				case 4:
					info.opcode = DAA;
					//sprintf(disassembly, "DAA");
					info.cycles = 4;
					break;
				case 5:
					info.opcode = CPL;
					//sprintf(disassembly, "CPL");
					info.cycles = 4;
					break;
				case 6:
					info.opcode = SCF;
					//sprintf(disassembly, "SCF");
					info.cycles = 4;
					break;
				case 7:
					info.opcode = CCF;
					//sprintf(disassembly, "CCF");
					info.cycles = 4;
					break;
				}
				break;
			}
			break;
		case 1:
			if (z == 6 && y == 6)
			{
				info.opcode = HALT;
				//sprintf(disassembly, "HALT");
				info.cycles = 4;
			}
			else
			{
				info.opcode = LD;
				info.source = r[z];
				info.destination = r[y];
				//sprintf(disassembly, "LD %s, %s", r[y], r[z]);
				info.cycles = 4;
			}
			break;
		case 2:
			info.opcode = alu[y];
			info.source = r[z];
			//sprintf(disassembly, "%s %s", alu[y], r[z]);
			info.cycles = 4;
			break;
		case 3:
			switch (z)
			{
			case 0:
				info.opcode = RET;
				info.condition = cc[y];
				//sprintf(disassembly, "RET %s", cc[y]);
				info.cycles = 11;
				break;
			case 1:
				switch (q)
				{
				case 0:
					info.opcode = POP;
					info.destination = rp2[p];
					//sprintf(disassembly, "POP %s", rp2[p]);
					info.cycles = 10;
					break;
				case 1:
					switch (p)
					{
					case 0:
						info.opcode = RET;
						//sprintf(disassembly, "RET");
						info.cycles = 10;
						break;
					case 1:
						info.opcode = EXX;
						//sprintf(disassembly, "EXX");
						info.cycles = 4;
						break;
					case 2:
						info.opcode = JP;
						info.destination = Mem16;
						info.data = z80->HL();
						//sprintf(disassembly, "JP (HL)");
						info.cycles = 4;
						break;
					case 3:
						info.opcode = LD;
						info.destination = SP;
						info.source = HL;
						//sprintf(disassembly, "LD SP, HL");
						info.cycles = 6;
						break;
					}
					break;
				}
				break;
			case 2:
				ReadNN(addr);
				info.opcode = JP;
				info.condition = cc[y];
				info.destination = Mem16;
				info.data = nn;
				//sprintf(disassembly, "JP %s, %.4x", cc[y], nn);
				info.cycles = 10;
				break;
			case 3:
				switch (y)
				{
				case 0:
					ReadNN(addr);
					info.opcode = JP;
					info.destination = Mem16;
					info.data = nn;
					//sprintf(disassembly, "JP %.4x", nn);
					info.cycles = 10;
					break;
				case 1:
					//This is the CB prefix and we should never hit this!
					break;
				case 2:
					n = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = OUT;
					info.destination = Mem8;
					info.data = n;
					info.source = A;
					//sprintf(disassembly, "OUT (%.2x), A", n);
					info.cycles = 11;
					break;
				case 3:
					n = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = IN;
					info.source = Mem8;
					info.data = n;
					info.destination = A;
					//sprintf(disassembly, "IN A, (%.2x)", n);
					info.cycles = 11;
					break;
				case 4:
					info.opcode = EX;
					info.source = HL;
					info.destination = Mem16;
					info.data = z80->SP();
					//sprintf(disassembly, "EX (SP), HL");
					info.cycles = 4;
					break;
				case 5:
					info.opcode = EX;
					info.source = HL;
					info.destination = DE;
					//sprintf(disassembly, "EX DE, HL");
					info.cycles = 4;
					break;
				case 6:
					info.opcode = DI;
					//sprintf(disassembly, "DI");
					info.cycles = 4;
					break;
				case 7:
					info.opcode = EI;
					//sprintf(disassembly, "EI");
					info.cycles = 4;
					break;
				}
				break;
			case 4:
				ReadNN(addr);
				info.opcode = CALL;
				info.destination = Mem16;
				info.data = nn;
				info.condition = cc[y];
				//sprintf(disassembly, "CALL %s, %.4x", cc[y], nn);
				info.cycles = 17;
				break;
			case 5:
				switch (q)
				{
				case 0:
					info.opcode = PUSH;
					info.source = rp2[p];
					//sprintf(disassembly, "PUSH %s", rp2[p]);
					info.cycles = 11;
					break;
				case 1:
					switch (p)
					{
					case 0:
						ReadNN(addr);
						info.opcode = CALL;
						info.destination = Mem16;
						info.data = nn;
						//sprintf(disassembly, "CALL %.4x", nn);
						info.cycles = 17;
						break;
					default: //These are the DD, ED, and FD prefix that we should never reach
						break;
					}
					break;
				}
				break;
			case 6:
				n = ReadMemory(addr + info.bytes);
				info.bytes++;
				info.opcode = alu[y];
				info.source = Direct;
				info.data = n;
				//sprintf(disassembly, "%s %.2x", alu[y], n);
				info.cycles = 4;
				break;
			case 7:
				info.opcode = RST;
				info.source = Direct;
				info.data = y * 8;
				//sprintf(disassembly, "RST %.2x", y * 8);
				info.cycles = 11;
				break;
			}
			break;
		}
	}
	else if (prefix == 0xcb)
	{
		switch (x)
		{
		case 0:
			info.opcode = rot[y];
			info.source = r[z];
			//sprintf(disassembly, "%s %s", rot[y], r[z]);
			info.cycles = 8;
			break;
		case 1:
			info.opcode = BIT;
			info.source = r[z];
			info.destination = Direct;
			info.data = y;
			//sprintf(disassembly, "BIT %d, %s", y, r[z]);
			info.cycles = 8;
			break;
		case 2:
			info.opcode = RES;
			info.source = r[z];
			info.destination = Direct;
			info.data = y;
			//sprintf(disassembly, "RES %d, %s", y, r[z]);
			info.cycles = 8;
			break;
		case 3:
			info.opcode = SET;
			info.source = r[z];
			info.destination = Direct;
			info.data = y;
			//sprintf(disassembly, "SET %d, %s", y, r[z]);
			info.cycles = 8;
			break;
		}
	}
	else if (prefix == 0xed) //Opcodes with JUST an ED prefix
	{
		switch (x)
		{
		case 0:
		case 3: //These are invalid
			info.cycles = 4;
			info.opcode = NONI;
			//sprintf(disassembly, "NONI");
			break;
		case 1:
			switch (z)
			{
			case 0:
				if (y == 6)
				{
					info.opcode = IN;
					info.source = Mem8;
					info.data = z80->C();
					info.destination = A;
					//sprintf(disassembly, "IN (C)");
					info.cycles = 12;
				}
				else
				{
					info.opcode = IN;
					info.source = Mem8;
					info.data = z80->C();
					info.destination = r[y];
					//sprintf(disassembly, "IN %s, (C)", r[y]);
					info.cycles = 12;
				}
				break;
			case 1:
				if (y == 6)
				{
					info.opcode = OUT;
					info.source = Direct;
					info.data = z80->C();
					info.destination = Mem8;
					//sprintf(disassembly, "OUT (C), 0");
					info.cycles = 12;
				}
				else
				{
					info.opcode = OUT;
					info.source = r[y];
					info.data = z80->C();
					info.destination = Mem8;
					//sprintf(disassembly, "OUT (C), %s", r[y]);
					info.cycles = 12;
				}
				break;
			case 2:
				if (q == 0)
				{
					info.opcode = SBC;
					info.source = rp[p];
					info.destination = HL;
					//sprintf(disassembly, "SBC HL, %s", rp[p]);
					info.cycles = 15;
				}
				else
				{
					info.opcode = ADC;
					info.source = rp[p];
					info.destination = HL;
					//sprintf(disassembly, "ADC HL, %s", rp[p]);
					info.cycles = 15;
				}
				break;
			case 3:
				ReadNN(addr);
				if (q == 0)
				{
					info.opcode = LD;
					info.source = rp[p];
					info.destination = Mem16;
					info.data = nn;
					//sprintf(disassembly, "LD (%x), %s", nn, rp[p]);
					info.cycles = 15;
				}
				else
				{
					info.opcode = LD;
					info.source = Mem16;
					info.data = nn;
					info.destination = rp[p];
					//sprintf(disassembly, "LD %s, (%x)", rp[p], nn);
					info.cycles = 15;
				}
				break;
			case 4:
				info.opcode = NEG;
				info.source = info.destination = Implied;
				//sprintf(disassembly, "NEG");
				info.cycles = 8;
				break;
			case 5:
				if (y == 1)
				{
					info.opcode = RETI;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "RETI");
					info.cycles = 14;
				}
				else
				{
					info.opcode = RETN;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "RETN");
					info.cycles = 14;
				}
				break;
			case 6:
				info.opcode = IM;
				info.source = info.destination = Implied;
				info.data = im[y];
				//sprintf(disassembly, "IM %s", im[y]);
				info.cycles = 8;
				break;
			case 7:
				switch (y)
				{
				case 0:
					info.opcode = LD;
					info.source = A;
					info.destination = I;
					//sprintf(disassembly, "LD I, A");
					info.cycles = 9;
					break;
				case 1:
					info.opcode = LD;
					info.source = A;
					info.destination = R;
					//sprintf(disassembly, "LD R, A");
					info.cycles = 9;
					break;
				case 2:
					info.opcode = LD;
					info.source = I;
					info.destination = A;
					//sprintf(disassembly, "LD A, I");
					info.cycles = 9;
					break;
				case 3:
					info.opcode = LD;
					info.source = R;
					info.destination = A;
					//sprintf(disassembly, "LD A, R");
					info.cycles = 9;
					break;
				case 4:
					info.opcode = RRD;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "RRD");
					info.cycles = 9; //?
					break;
				case 5:
					info.opcode = RLD;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "RLD");
					info.cycles = 9;
					break;
				case 6:
					info.opcode = NOP;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "NOP");
					info.cycles = 4; //?
					break;
				case 7:
					info.opcode = NOP;
					info.source = info.destination = Implied;
					//sprintf(disassembly, "NOP");
					info.cycles = 4; //?
					break;
				}
				break;
			}
			break;
		case 2:
			if (z < 4 && y > 3)
			{
				info.opcode = bli[y][z];
				info.source = info.destination = Implied;
				//sprintf(disassembly, bli[y][z]);
				info.cycles = 16;
			}
			else
			{
				info.opcode = NONI;
				info.source = info.destination = Implied;
				//sprintf(disassembly, "NONI");
				info.cycles = 4;
			}
			break;
		}
	}
	else if (prefix == 0xdd || prefix == 0xfd)
	{
		unsigned char repl = (prefix == 0xdd) ? 0 : 1;

		switch (x)
		{
		case 0:
			switch (z)
			{
			case 0:
				switch (y)
				{
				case 0:
					info.opcode = NOP;
					//sprintf(disassembly, "NOP");
					info.cycles = 4;
					break;
				case 1:
					info.opcode = EX;
					info.source = AF;
					info.destination = AF;
					//sprintf(disassembly, "EX AF, AF'");
					info.cycles = 4;
					break;
				case 2:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = DJNZ;
					info.destination = Mem16;
					info.data = d;
					//sprintf(disassembly, "DJNZ %.4x", addr + (char)d + info.bytes);
					info.cycles = 13;
					break;
				case 3:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = JR;
					info.destination = Mem16;
					info.data = d;
					//sprintf(disassembly, "JR %.4x", addr + (char)d + info.bytes);
					info.cycles = 12;
					break;
				default:
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = JR;
					info.destination = Mem16;
					info.data = d;
					info.condition = cc[y - 4];
					//sprintf(disassembly, "JR %s, %.4x", cc[y - 4], addr + (char)d + info.bytes);
					//TODO: Do we want to check if the jump will be taken?
					info.cycles = 13;
					break;
				}
				break;
			case 1:
				switch (q)
				{
				case 0:
					ReadNN(addr);
					info.cycles = 10;
					info.opcode = LD;
					info.destination = rp_repl[repl][p];
					info.source = Mem16;
					info.data = nn;
					//sprintf(disassembly, "LD %s, %.4x", rp_repl[repl][p], nn);
					break;
				case 1:
					info.cycles = 15;
					info.opcode = ADD;
					info.destination = HL;
					info.source = rp_repl[repl][p];
					//sprintf(disassembly, "ADD HL, %s", rp_repl[repl][p]);
					break;
				}
				break;
			case 2:
				switch (q)
				{
				case 0:
					switch (p)
					{
					case 0:
						info.opcode = LD;
						info.source = A;
						info.destination = Mem8;
						info.data = z80->BC();
						//sprintf(disassembly, "LD (BC), A");
						info.cycles = 7;
						break;
					case 1:
						info.opcode = LD;
						info.source = A;
						info.destination = Mem8;
						info.data = z80->DE();
						//sprintf(disassembly, "LD (DE), A");
						info.cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						info.opcode = LD;
						info.source = HL;
						info.destination = Mem16;
						info.data = nn;
						//sprintf(disassembly, "LD (%.4x), HL", nn);
						info.cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						info.opcode = LD;
						info.source = A;
						info.destination = Mem8;
						info.data = nn;
						//sprintf(disassembly, "LD (%.4x), A", nn);
						info.cycles = 13;
						break;
					}
					break;
				case 1:
					switch (p)
					{
					case 0:
						info.opcode = LD;
						info.source = Mem8;
						info.data = z80->BC();
						info.destination = A;
						//sprintf(disassembly, "LD A, (BC)");
						info.cycles = 7;
						break;
					case 1:
						info.opcode = LD;
						info.source = Mem8;
						info.data = z80->DE();
						info.destination = A;
						//sprintf(disassembly, "LD A, (DE)");
						info.cycles = 7;
						break;
					case 2:
						ReadNN(addr);
						info.opcode = LD;
						info.source = Mem16;
						info.data = nn;
						info.destination = HL;
						//sprintf(disassembly, "LD HL, (%.4x)", nn);
						info.cycles = 16;
						break;
					case 3:
						ReadNN(addr);
						info.opcode = LD;
						info.source = Mem8;
						info.data = nn;
						info.destination = A;
						//sprintf(disassembly, "LD A, (%.4x)", nn);
						info.cycles = 13;
						break;
					}
					break;
				}
				break;
			case 3:
				switch (q)
				{
				case 0:
					info.opcode = INC;
					info.source = rp_repl[repl][p];
					//sprintf(disassembly, "INC %s", rp_repl[repl][p]);
					info.cycles = 6;
					break;
				case 1:
					info.opcode = DEC;
					info.source = rp_repl[repl][p];
					//sprintf(disassembly, "DEC %s", rp_repl[repl][p]);
					info.cycles = 6;
					break;
				}
				break;
			case 4:
				if (y == 6)
				{
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = INC;
					info.source = idx[repl];
					info.data2 = d;
					//sprintf(disassembly, "INC (%s+%.2x)", idx[repl], d);
				}
				else
				{
					info.opcode = INC;
					info.source = rp_repl[repl][y];
					//sprintf(disassembly, "INC %s", r_repl[repl][y]);
				}
				info.cycles = 4;
				break;
			case 5:
				if (y == 6)
				{
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = DEC;
					info.source = idx[repl];
					info.data2 = d;
					//sprintf(disassembly, "DEC (%s+%.2x)", idx[repl], d);
				}
				else
				{
					info.opcode = DEC;
					info.source = r_repl[repl][y];
					//sprintf(disassembly, "DEC %s", r_repl[repl][y]);
				}
				info.cycles = 4;
				break;
			case 6:
				if (y == 6)
				{
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
				}
				n = ReadMemory(addr + info.bytes);
				info.bytes++;
				if (y == 6)
				{
					info.opcode = LD;
					info.source = idx[repl];
					info.data2 = d;
					info.data = n;
					//sprintf(disassembly, "LD (%s+%.2x), %.2x", idx[repl], d, n);
				}
				else
				{
					info.opcode = LD;
					info.source = r_repl[repl][y];
					info.data = n;
					//sprintf(disassembly, "LD %s, %.2x", r_repl[repl][y], n);
				}
				info.cycles = 7;
				break;
			case 7:
				switch (y)
				{
				case 0:
					info.opcode = RLCA;
					//sprintf(disassembly, "RLCA");
					info.cycles = 4;
					break;
				case 1:
					info.opcode = RRCA;
					//sprintf(disassembly, "RRCA");
					info.cycles = 4;
					break;
				case 2:
					info.opcode = RLA;
					//sprintf(disassembly, "RLA");
					info.cycles = 4;
					break;
				case 3:
					info.opcode = RRA;
					//sprintf(disassembly, "RRA");
					info.cycles = 4;
					break;
				case 4:
					info.opcode = DAA;
					//sprintf(disassembly, "DAA");
					info.cycles = 4;
					break;
				case 5:
					info.opcode = CPL;
					//sprintf(disassembly, "CPL");
					info.cycles = 4;
					break;
				case 6:
					info.opcode = SCF;
					//sprintf(disassembly, "SCF");
					info.cycles = 4;
					break;
				case 7:
					info.opcode = CCF;
					//sprintf(disassembly, "CCF");
					info.cycles = 4;
					break;
				}
				break;
			}
			break;
		case 1:
			if (z == 6 && y == 6)
			{
				info.opcode = HALT;
				//sprintf(disassembly, "HALT");
				info.cycles = 4;
			}
			else
			{
				if (y == 6 || z == 6)
				{
					d = ReadMemory(addr + info.bytes);
					info.bytes++;
				}
				if (y == 6)
				{
					info.opcode = LD;
					info.source = r[z];
					info.destination = idx[repl];
					info.data2 = d;
					//sprintf(disassembly, "LD (%s+%.2x), %s", idx[repl], d, r[z]);
				}
				else if (z == 6)
				{
					info.opcode = LD;
					info.source = idx[repl];
					info.data2 = d;
					info.destination = r[y];
					//sprintf(disassembly, "LD %s, (%s+%.2x)", r[y], idx[repl], d);
				}
				else
				{
					info.opcode = LD;
					info.source = r_repl[repl][z];
					info.destination = r_repl[repl][y];
					//sprintf(disassembly, "LD %s, %s", r_repl[repl][y], r_repl[repl][z]);
					info.cycles = 4;
				}
			}
			break;
		case 2:
			if (z == 6)
			{
				d = ReadMemory(addr + info.bytes);
				info.bytes++;
				info.opcode = alu[y];
				info.source = idx[repl];
				info.data2 = d;
				//sprintf(disassembly, "%s, (%s+%.2x)", alu[y], idx[repl], d);
			}
			else
			{
				info.opcode = alu[y];
				info.source = idx[repl];
				//sprintf(disassembly, "%s %s", alu[y], r_repl[repl][z]);
			}
			info.cycles = 4;
			break;
		case 3:
			switch (z)
			{
			case 0:
				info.opcode = RET;
				info.condition = cc[y];
				//sprintf(disassembly, "RET %s", cc[y]);
				info.cycles = 11;
				break;
			case 1:
				switch (q)
				{
				case 0:
					info.opcode = POP;
					info.destination = rp2_repl[repl][p];
					//sprintf(disassembly, "POP %s", rp2_repl[repl][p]);
					info.cycles = 10;
					break;
				case 1:
					switch (p)
					{
					case 0:
						info.opcode = RET;
						//sprintf(disassembly, "RET");
						info.cycles = 10;
						break;
					case 1:
						info.opcode = EXX;
						//sprintf(disassembly, "EXX");
						info.cycles = 4;
						break;
					case 2:
						info.opcode = JP;
						info.destination = Mem16;
						info.data = z80->HL();
						//sprintf(disassembly, "JP (HL)");
						info.cycles = 4;
						break;
					case 3:
						info.opcode = LD;
						info.source = HL;
						info.destination = SP;
						//sprintf(disassembly, "LD SP, HL");
						info.cycles = 6;
						break;
					}
					break;
				}
				break;
			case 2:
				ReadNN(addr);
				info.opcode = JP;
				info.condition = cc[y];
				info.destination = Mem16;
				info.data = nn;
				//sprintf(disassembly, "JP %s, %.4x", cc[y], nn);
				info.cycles = 10;
				break;
			case 3:
				switch (y)
				{
				case 0:
					ReadNN(addr);
					info.opcode = JP;
					info.destination = Mem16;
					info.data = nn;
					//sprintf(disassembly, "JP %.4x", nn);
					info.cycles = 10;
					break;
				case 1:
					//This is the CB prefix and we should never hit this!
					break;
				case 2:
					n = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = OUT;
					info.source = A;
					info.destination = Mem8;
					info.data = n;
					//sprintf(disassembly, "OUT (%.2x), A", n);
					info.cycles = 11;
					break;
				case 3:
					n = ReadMemory(addr + info.bytes);
					info.bytes++;
					info.opcode = IN;
					info.destination = A;
					info.source = Mem8;
					info.data = n;
					//sprintf(disassembly, "IN A, (%.2x)", n);
					info.cycles = 11;
					break;
				case 4:
					info.opcode = EX;
					info.source = HL;
					info.destination = Mem16;
					info.data = z80->SP();
					//sprintf(disassembly, "EX (SP), HL");
					info.cycles = 4;
					break;
				case 5:
					info.opcode = EX;
					info.source = HL;
					info.destination = DE;
					//sprintf(disassembly, "EX DE, HL");
					info.cycles = 4;
					break;
				case 6:
					info.opcode = DI;
					//sprintf(disassembly, "DI");
					info.cycles = 4;
					break;
				case 7:
					info.opcode = EI;
					//sprintf(disassembly, "EI");
					info.cycles = 4;
					break;
				}
				break;
			case 4:
				ReadNN(addr);
				info.opcode = CALL;
				info.condition = cc[y];
				info.destination = Mem16;
				info.data = nn;
				//sprintf(disassembly, "CALL %s, %.4x", cc[y], nn);
				info.cycles = 17;
				break;
			case 5:
				switch (q)
				{
				case 0:
					info.opcode = PUSH;
					info.source = rp2_repl[repl][p];
					//sprintf(disassembly, "PUSH %s", rp2_repl[repl][p]);
					info.cycles = 11;
					break;
				case 1:
					switch (p)
					{
					case 0:
						ReadNN(addr);
						info.opcode = CALL;
						info.destination = Mem16;
						info.data = nn;
						//sprintf(disassembly, "CALL %.4x", nn);
						info.cycles = 17;
						break;
					default: //These are the DD, ED, and FD prefix that we should never reach
						break;
					}
					break;
				}
				break;
			case 6:
				n = ReadMemory(addr + info.bytes);
				info.bytes++;
				info.opcode = alu[y];
				info.source = Direct;
				info.data = n;
				//sprintf(disassembly, "%s %.2x", alu[y], n);
				info.cycles = 4;
				break;
			case 7:
				info.opcode = RST;
				info.destination = Mem16;
				info.data = y * 8;
				//sprintf(disassembly, "RST %.2x", y * 8);
				info.cycles = 11;
				break;
			}
			break;
		}
	}
	else if (prefix == 0xddcb || prefix == 0xfdcb)
	{
		unsigned char repl = ((prefix & 0xdd00) == 0xdd00 ? 0 : 1);
		d = ReadMemory(addr + info.bytes); //These all use a displacement
		info.bytes++;
		switch (x)
		{
		case 0:
			if (z != 6)
			{
				info.opcode = LD;
				info.source = idx[repl];
				info.data2 = d;
				info.destination = r[z];
				info.secOpcode = rot[y];
				//sprintf(disassembly, "LD %s, %s (%s+%.2x)", r[z], rot[y], idx[repl], d);
			}
			else
			{
				info.opcode = rot[y];
				info.source = idx[repl];
				info.data2 = d;
				//sprintf(disassembly, "%s (%s+%.2x)", rot[y], idx[repl], d);
			}
			info.cycles = 8;
			break;
		case 1:
			info.opcode = BIT;
			info.source = idx[repl];
			info.data2 = d;
			info.destination = Direct;
			info.data = d;
			//sprintf(disassembly, "BIT %d, (%s+%.2x)", y, idx[repl], d);
			info.cycles = 8;
			break;
		case 2:
			if (z == 6)
			{
				info.opcode = RES;
				info.source = idx[repl];
				info.data2 = d;
				info.destination = Direct;
				info.data = d;
				//sprintf(disassembly, "RES %d, (%s+%.2x)", y, idx[repl], d);
			}
			else
			{
				info.opcode = LD;
				info.source = idx[repl];
				info.data2 = d;
				info.destination = r[z];
				info.secOpcode = RES;
				info.data = y;
				//sprintf(disassembly, "LD %s, RES %d, (%s+%.2x)", r[z], y, idx[repl], d);
			}
			info.cycles = 8;
			break;
		case 3:
			if (z == 6)
			{
				info.opcode = SET;
				info.destination = Direct;
				info.data = y;
				info.source = idx[repl];
				info.data2 = d;
				//sprintf(disassembly, "SET %d, (%s+%.2x)", y, idx[repl], d);
			}
			else
			{
				info.opcode = LD;
				info.destination = r[z];
				info.source = idx[repl];
				info.data2 = d;
				info.secOpcode = SET;
				//sprintf(disassembly, "LD %s, SET %d, (%s+%.2x)", r[z], y, idx[repl], d);
			}
			info.cycles = 8;
			break;
		}
	}
	return info;
}
