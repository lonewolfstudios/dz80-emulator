#pragma once

#include <fstream>

class ifstream : public std::ifstream
{

public:

	ifstream() : std::ifstream() {}
	ifstream(char* file, std::ios_base::openmode mode = std::ios::binary, int prot = 64) : std::ifstream(file, mode, prot) {}

	unsigned long length()
	{
		//This is C++'s stupid way of getting a file size
		auto currentPos = this->tellg();
		this->ignore(std::numeric_limits<std::streamsize>::max());
		auto length = this->gcount();
		this->clear();
		this->seekg(currentPos, std::ios::beg);
		return (unsigned long)length;
	}
};

class ofstream : public std::ofstream
{

public:
	ofstream() : std::ofstream() {}
	ofstream(char* file, std::ios_base::openmode mode = std::ios::binary, int prot = 64) : std::ofstream(file, mode, prot) {}
};
