#include "SystemConfig.h"
//#include "Input/InputManager.h"

SystemConfig* SystemConfig::instance = nullptr;

SystemConfig::SystemConfig()
{
	for (int i = 0; i < 4; ++i)
		switches[i] = false;
	bankSize = BankSize::Sixteen;
}

SystemConfig* SystemConfig::Instance()
{
	if (instance == nullptr)
		instance = new SystemConfig();
	return instance;
}

bool SystemConfig::GetSwitch(int num)
{
	if (num < 0 || num > 3)
		return false;
	return Instance()->switches[num];
}

void SystemConfig::ToggleSwitch(int num)
{
	if (num < 0 || num > 3)
		return;
	Instance()->switches[num] = !Instance()->switches[num];
}

SystemConfig::BankSize SystemConfig::GetBankSize()
{
	return Instance()->bankSize;
}

void SystemConfig::ToggleBankSize()
{
	if (Instance()->bankSize == BankSize::Sixteen)
		Instance()->bankSize = BankSize::ThirtyTwo;
	else
		Instance()->bankSize = BankSize::Sixteen;
}
