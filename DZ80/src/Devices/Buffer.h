#pragma once

#include <stdlib.h>

template <class T>
class Buffer
{

private:

	T* _data;
	int width, height;

public:

	Buffer(int width, int height)
	{
		_data = new T[width * height];
		this->width = width;
		this->height = height;
	}

	Buffer(int width, int height, T defaultData) : Buffer(width, height)
	{
		for (int i = 0; i < width * height; ++i)
			_data[i] = defaultData;
	}

	~Buffer()
	{
		delete[] _data;
	}

	T& operator[](int index)
	{
		if (index >= width * height)
			exit(0);
		return _data[index];
	}

	int Width() { return width; }
	int Height() { return height; }

};