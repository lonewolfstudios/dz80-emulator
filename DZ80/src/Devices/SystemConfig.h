#pragma once

class SystemConfig
{
	friend class GUISystemConfigStatusWindow;

public:

	enum BankSize
	{
		Sixteen,
		ThirtyTwo
	};

private:

	SystemConfig();
	static SystemConfig* instance;
	BankSize bankSize;
	bool switches[4];
	static SystemConfig* Instance();

public:

	static bool GetSwitch(int num);
	static void ToggleSwitch(int num);
	static BankSize GetBankSize();
	static void ToggleBankSize();

};