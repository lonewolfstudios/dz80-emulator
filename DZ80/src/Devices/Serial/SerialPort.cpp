#include "SerialPort.h"

#include <tchar.h>
#include <strsafe.h>

//void DisplayError(LPTSTR lpszFunction);


void SerialPort::EnumeratePorts()
{
	availablePorts.clear();
	portPaths.clear();
	char lpTargetPath[5000];

	for (int i = 0; i < 255; i++)
	{
		std::string str = "COM" + std::to_string(i);
		DWORD test = QueryDosDeviceA(str.c_str(), lpTargetPath, 5000);

		if (test != 0)
		{
			availablePorts.push_back(str);
			portPaths.push_back(std::string(lpTargetPath));
		}
	}
}

void SerialPort::readThread()
{
	DWORD status = 0;
	DWORD event = 0;
	OVERLAPPED overlapped;
	overlapped.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	overlapped.Internal = 0;
	overlapped.InternalHigh = 0;
	overlapped.Offset = 0;
	overlapped.OffsetHigh = 0;
	while (!cancelThread)
	{
		DWORD bytesRead = 0;
		do
		{
			unsigned char c;
			bool good = ReadFile(handle, &c, 1, &bytesRead, &overlapped);
			if (good && bytesRead > 0 && byteReceived != nullptr)
				byteReceived(c);
		} while (bytesRead > 0);
		Sleep(1);
	}
}

SerialPort::SerialPort(std::function<void(unsigned char)> byteReceivedCallback)
{
	byteReceived = byteReceivedCallback;
	EnumeratePorts();
	serialParams.DCBlength = sizeof(serialParams);
	serialParams.fBinary = true;
	SetBaudRate(9600);
	SetParity(0);
	SetByteSize(8);
	SetStopBits(1);
}

SerialPort::~SerialPort()
{
	if (handle != nullptr)
		Close();
}

bool SerialPort::Open(int portNum)
{
	if (portNum < 0 || portNum >= availablePorts.size())
		return false;
	return Open("\\\\.\\" + availablePorts[portNum]);
}

bool SerialPort::Open(std::string portName)
{
	//Is there a port already open?
	if (handle != nullptr)
	{
		Close();
	}

	handle = CreateFileA(portName.c_str(), GENERIC_READ | GENERIC_WRITE,
		0, NULL, OPEN_EXISTING, 0, NULL);
	if (handle == INVALID_HANDLE_VALUE)
	{
		handle = nullptr;
		//DisplayError(TEXT("CreateFileA"));
		return false;
	}
	SetCommState(handle, &serialParams);
	COMMTIMEOUTS timeouts;
	timeouts.ReadIntervalTimeout = 50;
	timeouts.ReadTotalTimeoutConstant = 50;
	timeouts.ReadTotalTimeoutMultiplier = 10;
	timeouts.WriteTotalTimeoutConstant = 50;
	timeouts.WriteTotalTimeoutMultiplier = 10;
	SetCommTimeouts(handle, &timeouts);
	readingThread = new std::thread([&]() { readThread(); });
}


bool SerialPort::Close()
{
	if (readingThread != nullptr)
	{
		cancelThread = true;
		readingThread->join();
		delete readingThread;
		readingThread = nullptr;
	}
	if (handle == nullptr)
		return false;
	CloseHandle(handle);
	handle = nullptr;
	return true;
}

void SerialPort::Send(unsigned char c)
{
	DWORD written;
	WriteFile(handle, &c, 1, &written, NULL);
}

void SerialPort::Send(unsigned char* charArray, size_t count)
{
	DWORD written;
	WriteFile(handle, charArray, count, &written, NULL);
}

std::string SerialPort::GetPort(int index)
{
	if (index < 0 || index >= availablePorts.size())
		return std::string();
	return availablePorts[index];
}

void SerialPort::SetBaudRate(int rate)
{
	serialParams.BaudRate = rate;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}

void SerialPort::SetByteSize(int byteSize)
{
	serialParams.ByteSize = byteSize;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}

void SerialPort::SetParity(int parity)
{
	serialParams.Parity = parity;
	if (parity == 0)
		serialParams.fParity = false;
	else
		serialParams.fParity = true;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}

void SerialPort::EnableParity()
{
	serialParams.fParity = true;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}

void SerialPort::DisableParity()
{
	serialParams.fParity = false;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}

void SerialPort::SetStopBits(int bits)
{
	serialParams.StopBits = bits;
	if (handle != nullptr)
		SetCommState(handle, &serialParams);
}



/*
void DisplayError(LPTSTR lpszFunction)
// Routine Description:
// Retrieve and output the system error message for the last-error code
{
	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL);

	lpDisplayBuf =
		(LPVOID)LocalAlloc(LMEM_ZEROINIT,
			(lstrlen((LPCTSTR)lpMsgBuf)
				+ lstrlen((LPCTSTR)lpszFunction)
				+ 40) // account for format string
			* sizeof(TCHAR));

	if (FAILED(StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error code %d as follows:\n%s"),
		lpszFunction,
		dw,
		lpMsgBuf)))
	{
		printf("FATAL ERROR: Unable to output error code.\n");
	}

	_tprintf(TEXT("ERROR: %s\n"), (LPCTSTR)lpDisplayBuf);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}
*/