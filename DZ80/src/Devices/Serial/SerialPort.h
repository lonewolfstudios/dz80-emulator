#pragma once

#include <windows.h>
#include <string>
#include <vector>
#include <thread>
#include <functional>

class SerialPort
{

private:
	void* handle = nullptr;
	std::vector<std::string> availablePorts;
	std::vector<std::string> portPaths;
	DCB serialParams;
	std::thread* readingThread = nullptr;
	std::function<void(unsigned char)> byteReceived = nullptr;
	bool cancelThread = false;

	void EnumeratePorts();
	void readThread();

public:
	SerialPort(std::function<void(unsigned char)> byteReceivedCallback);
	~SerialPort();
	bool Open(int portNum);
	bool Open(std::string portName);
	bool Close();
	void Send(unsigned char c);
	void Send(unsigned char* charArray, size_t count);
	int AvailablePortCount() { return availablePorts.size(); }
	std::string GetPort(int index);
	void SetBaudRate(int rate);
	void SetByteSize(int byteSize);
	void SetParity(int parity);
	void EnableParity();
	void DisableParity();
	void SetStopBits(int bits);

};