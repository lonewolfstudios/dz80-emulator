#include "BusDevice.h"

class RAM : public BusDevice
{
private:
	unsigned char* memory;

public:

	RAM(unsigned short address, unsigned int bytes);
	~RAM();

	// Inherited via BusDevice
	virtual void WriteData(unsigned short address, unsigned char data) override;
	virtual unsigned char ReadData(unsigned short address) override;

	void LoadFromFile(char* path, unsigned short startAddress);

};