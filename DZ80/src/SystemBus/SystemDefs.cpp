#include "SystemDefs.h"

double SystemDefs::CYCLES_PER_SECOND = 20000000.0; //20 MhZ
double SystemDefs::SECONDS_PER_CYCLE = 1.0 / CYCLES_PER_SECOND;
double SystemDefs::NANOSECS_PER_CYCLE = SECONDS_PER_CYCLE * 1000000000.0;
long long SystemDefs::CYCLE_NANOSECS = (long long)NANOSECS_PER_CYCLE;
double SystemDefs::SCAN_LINES = 200.0;
double SystemDefs::REFRESH_LINES = 56.0;
double SystemDefs::REFRESH_HZ = 60.0;
double SystemDefs::CYCLES_PER_SCREEN = CYCLES_PER_SECOND / REFRESH_HZ;
double SystemDefs::CYCLES_PER_SCREEN_LINE = CYCLES_PER_SCREEN / (SCAN_LINES + REFRESH_LINES);
