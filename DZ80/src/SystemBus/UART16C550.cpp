#include "UART16C550.h"

#define	RHR	0
#define THR	0
#define IER 1
#define ISR 2
#define FCR 2
#define LCR 3
#define MCR 4
#define LSR 5
#define MSR 6
#define SPR 7
#define DLL 8
#define DLM 9

void UART16C550::CharReceived(unsigned char c)
{
	if (FIFO.size() >= FIFOSize)
		return; //FIFO is full, so ignore the character
	FIFO.push_back(c);
	regs[LSR] |= 1;
}

void UART16C550::UpdateBaud()
{
	int divisor = (int)regs[DLM] << 8 | regs[DLL];
	if (divisor != 0)
		serialPort->SetBaudRate(1843200 / divisor / 16);
}

void UART16C550::UpdateDCB(unsigned char data)
{
	serialPort->SetByteSize(5 + (data & 0x03));
	if (data & 0x04)
	{
		if (data & 0x03)
			serialPort->SetStopBits(2);
		else
			serialPort->SetStopBits(1);
	}
	else
		serialPort->SetStopBits(0);
	if (data & 0x08)
	{
		serialPort->EnableParity();
		if (data & 0x10)
			serialPort->SetParity(2);
		else
			serialPort->SetParity(1);
	}
	else
		serialPort->DisableParity();
	//Bit 5 is unhandled as I'm not sure what it does yet
	//Bit 6 is unhandled
	//Bit 7 does nothing here
}

void UART16C550::SendCharacter(unsigned char c)
{
	//printf("%c", c);
	serialPort->Send(c);
}

UART16C550::UART16C550(unsigned short address) : BusDevice(address, 8, true, true)
{
	serialPort = new SerialPort([&](unsigned char c) {CharReceived(c); });
	regs[RHR] = 0;
	regs[IER] = 0;
	regs[FCR] = 0;
	regs[ISR] = 0;
	regs[LCR] = 0;
	regs[MCR] = 0;
	regs[LSR] = 0x60;
	regs[MSR] = 0;
	regs[SPR] = 0xff;
	regs[DLL] = 0;
	regs[DLM] = 0;
	UpdateBaud();
	serialPort->Open(1);
}

UART16C550::~UART16C550()
{
	serialPort->Close();
	delete serialPort;
}

void UART16C550::WriteData(unsigned short address, unsigned char data)
{
	address -= base;
	if (address > length)
		return;
	switch (address)
	{
	case THR:
		if (regs[LCR] & 0x80)
		{
			regs[DLL] = data;
			UpdateBaud();
		}
		else
			SendCharacter(data);
		break;
	case IER:
		if (regs[LCR] & 0x80)
		{
			regs[DLM] = data;
			UpdateBaud();
		}
		else
			regs[IER] = data;
		break;
	case FCR:
		if ((regs[FCR] & 1) == 0)
		{
			data &= 0x01;
			regs[FCR] &= 0xfe;
			regs[FCR] |= data;
			return;
		}
		regs[FCR] = data;
		if (data & 2) //FIFO receive reset
		{
			FIFO.clear();
			regs[FCR] &= 0b11111101;
		}
		if (data & 4) //FIFO transmit reset (FIFO transmit not implemented)
		{
			regs[FCR] &= 0b11111011;
		}
		data >>= 6;
		switch (data)
		{
		case 0:
			FIFOTrigger = 1;
			break;
		case 1:
			FIFOTrigger = 4;
			break;
		case 2:
			FIFOTrigger = 8;
			break;
		case 3:
			FIFOTrigger = 16;
			break;
		}
		break;
	case LCR:
		regs[LCR] = data;
		UpdateDCB(data);
		break;
	case SPR:
		regs[SPR] = data;
		break;
	}
}

unsigned char UART16C550::ReadData(unsigned short address)
{
	unsigned char c = 0;
	address -= base;
	if (address > length)
		return 0;
	switch (address)
	{
	case RHR:
		if (regs[LCR] & 0x80)
		{
			return regs[DLL];
		}
		else
		{
			if (FIFO.size() == 0)
				return 0;
			c = FIFO[0];
			FIFO.erase(FIFO.begin());
			return c;
		}
		break;
	case IER:
		if (regs[LCR] & 0x80)
		{
			return regs[DLM];
		}
		else
			return regs[IER];
		break;
	case ISR:
		//Not implemented yet!
		break;
	case LCR:
		//Not implemented yet!
		break;
	case MCR:
		//Not implemented yet!
		break;
	case LSR:
		return regs[LSR];
	case MSR:
		return regs[MSR];
		break;
	case SPR:
		return regs[SPR];
		break;

	}
	return 0;
}
