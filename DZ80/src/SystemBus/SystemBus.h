#pragma once

#include "SystemBus/Bus.h"

class SystemBus
{
private:
	static SystemBus* _instance;

	Bus* addressBus = nullptr;
	Bus* ioBus = nullptr;
	unsigned long long cyclesElapsed = 0;

public:
	SystemBus();
	~SystemBus();
	static SystemBus* Instance() { if (_instance == nullptr) _instance = new SystemBus(); return _instance; }
	Bus* AddressBus() { return addressBus; }
	Bus* IOBus() { return ioBus; }
	void AdvanceCycles(int cycles);
	unsigned long long CyclesElapsed() { return cyclesElapsed; }
	void Reset();

};

