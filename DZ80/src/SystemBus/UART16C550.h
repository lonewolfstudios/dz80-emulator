#pragma once

#include "BusDevice.h"
#include "Devices/Serial/SerialPort.h"

class UART16C550 : public BusDevice
{
	
private:

	SerialPort* serialPort;
	void CharReceived(unsigned char c);
	unsigned char regs[10];
	void UpdateBaud();
	void UpdateDCB(unsigned char data);
	void SendCharacter(unsigned char c);
	int FIFOSize = 16;
	int FIFOTrigger = 1;
	std::vector<unsigned char> FIFO;

public:

	UART16C550(unsigned short address);
	~UART16C550();

	virtual void WriteData(unsigned short address, unsigned char data) override;
	virtual unsigned char ReadData(unsigned short address) override;

};