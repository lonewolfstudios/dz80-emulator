#pragma once

#include <vector>
#include "BusDevice.h"

class Bus
{

private:
	std::vector<BusDevice*> busDevices;
	//unsigned char memory[65536];
	
public:
	unsigned char ReadAddress(const unsigned short address);
	void WriteAddress(const unsigned short address, const unsigned char value);

	void AttachDevice(BusDevice* device, bool InBack = false);
	void DetachDevice(BusDevice* device);
	BusDevice* GetDeviceAt(const unsigned short address);
	void AdvanceCycles(int cycles);
	//This function will ALWAYS read the RAM and should not typically be used unless you
	//know that the address must be RAM
	unsigned char ReadRAM(const unsigned short address);
	void Reset();

};