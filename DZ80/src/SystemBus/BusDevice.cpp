#include "BusDevice.h"

void BusDevice::SetAddress(unsigned short newAddress)
{
	base = newAddress;
}

bool BusDevice::HasAddress(unsigned short address)
{
	return enabled && (address >= this->base && address < this->base + length);
}
