#pragma once

//This is an abstract class this is used to represent devices that can be attached to the bus
//Devices will inherit from this class.

class BusDevice
{
protected:
	unsigned short base;
	unsigned int length;
	bool enabled;
	bool writeable;

	unsigned long long cycleCount = 0;

	BusDevice(unsigned short address, unsigned int numRegisters, bool enable, bool canWrite) : 
		enabled(enable), writeable(canWrite), base(address), length(numRegisters) {}


public:
	void SetAddress(unsigned short newAddress);
	virtual bool HasAddress(unsigned short address);
	virtual bool CanWrite(unsigned short address) { return writeable; }
	virtual bool IsEnabled() { return enabled; }
	void Enable() { enabled = true; }
	void Disable() { enabled = false; }

	virtual void WriteData(unsigned short address, unsigned char data) = 0;
	virtual unsigned char ReadData(unsigned short address) = 0;
	void ResetCycles() { cycleCount = 0; }
	virtual void Reset() { cycleCount = 0; }
	virtual void AdvanceCycles(int cycleCount) {}
};