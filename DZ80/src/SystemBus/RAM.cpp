#include "RAM.h"
#include <memory>
#include <stdio.h>
#include "SystemBus.h"

RAM::RAM(unsigned short address, unsigned int bytes) : BusDevice(address, bytes, true, true)
{
	memory = (unsigned char*)malloc(bytes);
	for (unsigned int i = 0; i < bytes; ++i)
		memory[i] = 0;
	SystemBus::Instance()->AddressBus()->AttachDevice(this, true);
}

RAM::~RAM()
{
	free(memory);
}

void RAM::WriteData(unsigned short address, unsigned char data)
{
	memory[address - base] = data;
}

unsigned char RAM::ReadData(unsigned short address)
{
	return memory[address - base];
}

void RAM::LoadFromFile(char* path, unsigned short startAddress)
{
	FILE* fp;
	fp = fopen(path, "rb");
	if (fp == NULL)
		return;
	fseek(fp, 0, SEEK_END);
	long length = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	unsigned short startOffset = startAddress - base;
	if ((unsigned int)startOffset + (unsigned int)length > this->length)
	{
		length -= (startOffset + length - this->length);
	}
	fread((void*)(&memory[startOffset]), length, 1, fp);
	fclose(fp);
}
