#include "SystemBus.h"

SystemBus* SystemBus::_instance = nullptr;

SystemBus::SystemBus()
{
	addressBus = new Bus();
	ioBus = new Bus();
}

SystemBus::~SystemBus()
{
	delete addressBus;
	delete ioBus;
	_instance = nullptr;
}

void SystemBus::AdvanceCycles(int cycles)
{
	cyclesElapsed += cycles;
	addressBus->AdvanceCycles(cycles);
	ioBus->AdvanceCycles(cycles);
}

void SystemBus::Reset()
{
	AddressBus()->Reset();
	IOBus()->Reset();
}
