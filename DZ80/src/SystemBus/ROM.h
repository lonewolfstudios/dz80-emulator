#pragma once

#include "BusDevice.h"

class ROM : public BusDevice
{
private:
	unsigned char* memory;

public:
	ROM(char* filename, unsigned short address);
	~ROM();
	
	// Inherited via BusDevice
	virtual void WriteData(unsigned short address, unsigned char data) override;
	virtual unsigned char ReadData(unsigned short address) override;
	virtual bool IsEnabled() override;
	virtual bool HasAddress(unsigned short address) override;

};
