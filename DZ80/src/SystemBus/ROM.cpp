#include "ROM.h"
#include "SystemBus.h"
#include <stdlib.h>
#include <stdio.h>
#include "Devices/SystemConfig.h"

ROM::ROM(char* filename, unsigned short address) :
	BusDevice(address, 0, false, false)
{
	FILE* fp = fopen(filename, "rb");
	if (fp == nullptr)
		return;
	fseek(fp, 0, SEEK_END);
	int filelength = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	length = filelength;
	memory = (unsigned char*)malloc(length);
	fread((void*)memory, sizeof(unsigned char), length, fp);
	fclose(fp);
	SystemBus::Instance()->AddressBus()->AttachDevice(this);
	enabled = true;
}

ROM::~ROM()
{
	SystemBus::Instance()->AddressBus()->DetachDevice(this);
	free(memory);
}

void ROM::WriteData(unsigned short address, unsigned char data)
{
	//ROMs are read-only
	return;
}

unsigned char ROM::ReadData(unsigned short address)
{
	if (HasAddress(address))
	{
		int addr = address - base;
		int size = 0x4000;
		if (SystemConfig::GetBankSize() == SystemConfig::BankSize::ThirtyTwo)
			size = 0x8000;
		int bank = 0;
		for (int i = 3; i >= 0; --i)
		{
			bank <<= 1;
			if (SystemConfig::GetSwitch(i))
				bank |= 1;
		}
		addr += size * bank;
		return memory[addr];
	}
	return 0;
}

bool ROM::IsEnabled()
{
	return true;
}

bool ROM::HasAddress(unsigned short address)
{
	unsigned short maxAddress = 0x4000;
	if (SystemConfig::GetBankSize() == SystemConfig::BankSize::ThirtyTwo)
		maxAddress = 0x8000;
	return (address < maxAddress);
}
