#include "Bus.h"
#include <algorithm>
#include "RAM.h"

static unsigned char dummy;

unsigned char Bus::ReadAddress(const unsigned short address)
{
	for (BusDevice* bd : busDevices)
		if (bd->IsEnabled() && bd->HasAddress(address))
			return bd->ReadData(address);
	return (unsigned char)(rand() % 0x100);	//Simulate a disconnected device
}

unsigned char Bus::ReadRAM(const unsigned short address)
{
	RAM* memory = (RAM*)busDevices.back();
	return memory->ReadData(address);
}

void Bus::WriteAddress(const unsigned short address, const unsigned char value)
{
	for (BusDevice* bd : busDevices)
		if (bd->IsEnabled() && bd->HasAddress(address) && bd->CanWrite(address))
		{
			bd->WriteData(address, value);
			return;
		}
}

void Bus::AttachDevice(BusDevice* device, bool inBack)
{
	//Make sure that it is not already attached
	auto bd = std::find(busDevices.begin(), busDevices.end(), device);
	if (bd == busDevices.end())
		if (inBack)
			busDevices.emplace_back(device);
		else
			busDevices.insert(busDevices.begin(), device);
}

void Bus::DetachDevice(BusDevice* device)
{
	//See if it is already attached
	auto bd = std::find(busDevices.begin(), busDevices.end(), device);
	if (bd == busDevices.end())
		return;
	busDevices.erase(bd);
}

BusDevice* Bus::GetDeviceAt(const unsigned short address)
{
	for (auto bd : busDevices)
		if (bd->HasAddress(address))
			return bd;
	return nullptr;
}

void Bus::AdvanceCycles(int cycles)
{
	for (auto bd : busDevices)
		bd->AdvanceCycles(cycles);
}

void Bus::Reset()
{
	for (auto bd : busDevices)
		bd->Reset();
}