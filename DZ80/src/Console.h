#pragma once

#include "SystemBus/SystemBus.h"
#include "SystemBus/RAM.h"
#include "SystemBus/ROM.h"
#include "SystemBus/UART16C550.h"
#include "Z80.h"

class Console
{
private:
	static Console* _instance;
	
	Z80*				cpu = nullptr;
	RAM*				ram = nullptr;
	ROM*				rom = nullptr;
	SystemBus*			systemBus = nullptr;
	UART16C550*			uart = nullptr;

public:
	Console();
	~Console();
	void Run();
	static Console* Instance() { if (_instance == nullptr) _instance = new Console(); return _instance; }
	static Z80* CPU() { return _instance->cpu; }
	void LoadRom(char* path);

};

