#pragma once

#include <vector>
#include <atomic>

#include "SystemBus/SystemDefs.h"

// Define flag constants for use with the F register
// We need six of them: C,N,PV,H,Z, and S.
const unsigned char CF = 1;		// Carry Flag
const unsigned char NF = 2;		// Add/Subtract Flag
const unsigned char PV = 4;		// Parity/Overflow Flag
const unsigned char U1 = 8;		// Unused
const unsigned char HC = 16;		// Half-Carry
const unsigned char U2 = 32;		// Unused
const unsigned char ZF = 64;		// Zero
const unsigned char SF = 128;		// Sign

class Z80
{
	struct Registers
	{
		unsigned short AF, BC, DE, HL;
	};

	typedef unsigned char (Z80::* OpcodeHandler)(unsigned char, unsigned char);

private:
	static Z80* _instance;		// We will assume that there is only one and store a reference to it here.
								// This singleton is NOT enforced in any way.

	static OpcodeHandler opcodeHandlers[256];
	static OpcodeHandler CBHandlers[256];
	static OpcodeHandler EDHandlers[256];
	static OpcodeHandler DDHandlers[256];
	static OpcodeHandler FDHandlers[256];

	//void SetupHandlers();

	Registers Regs, AltRegs;
	unsigned short _IX, _IY, _PC;
	unsigned short _sp;								//The non-doubled registers
	unsigned char _I;								//The interrupt register
	unsigned char R;								//The refresh register
	unsigned long long cycleCount = 0;				//The elapsed cycles since start
	unsigned short cyclesToBurn = 0;				//Cycles needed to be skipped
	bool IFF1 = false;								//Interrupt enable flip flop 1
	bool IFF2 = false;								//Interrupt enable flip flop 2
	unsigned char InterruptMode = 0;

	bool halted = false;
	bool running = false;
	bool singleStep = true;

	bool maskableInterruptWaiting = false;
	bool nonmaskableInterruptWaiting = false;

	inline void HandleMaskableInterrupt();
	inline void HandleNonmaskableInterrupt();
	inline void StartMaskableInterruptRoutine(unsigned short vector);
	inline void StartNonmaskableInterruptRoutine();

	//Store the breakpoints
	std::vector<unsigned short> executeBreakpoints;
	std::atomic<bool> accessingBreakpoints = false;
	inline bool needExecBreak();

	// Variables used for logging to files
	FILE* fp;
	bool logging = false;
	bool memAccessed = true;
	bool firstLoop = true;
	unsigned short lastAddress = 0;
	unsigned short lastData = 0;

public:
	Z80();
	~Z80();
	unsigned char DoNextOpcode();
	unsigned char DoSingleStep();

	void Reset();
	void Run();
	void Stop() { running = false; }
	void SingleStep() { singleStep = true; }
	void CancelSingleStep() { singleStep = false; }
	void CreateFlagsString(char* str);
	void CreateAltFlagsString(char* str);
	bool Halted() { return halted; }
	const bool isRunning() { return running; }
	const bool isSingleStep() { return singleStep; }

	bool MaskableInterrupt();
	void NonmaskableInterrupt();

	inline unsigned long long CycleCount() { return cycleCount; }
	static inline Z80* Instance() { if (_instance == nullptr) _instance = new Z80(); return _instance; }

	//Everything below this point exists only for debugging purposes and does not affect
	//or add to the emulation

	// Some inline methods to get the registers (useful for the built-in debugger)
	unsigned char A() { return (unsigned char)(Regs.AF >> 8); }
	unsigned char B() { return (unsigned char)(Regs.BC >> 8); }
	unsigned char C() { return (unsigned char)(Regs.BC & 0xff); }
	unsigned char D() { return (unsigned char)(Regs.DE >> 8); }
	unsigned char E() { return (unsigned char)(Regs.DE & 0xff); }
	unsigned char F() { return (unsigned char)(Regs.AF & 0xff); }
	unsigned char H() { return (unsigned char)(Regs.HL >> 8); }
	unsigned char L() { return (unsigned char)(Regs.HL & 0xff); }

	unsigned char A2() { return (unsigned char)(AltRegs.AF >> 8); }
	unsigned char B2() { return (unsigned char)(AltRegs.BC >> 8); }
	unsigned char C2() { return (unsigned char)(AltRegs.BC & 0xff); }
	unsigned char D2() { return (unsigned char)(AltRegs.DE >> 8); }
	unsigned char E2() { return (unsigned char)(AltRegs.DE & 0xff); }
	unsigned char F2() { return (unsigned char)(AltRegs.AF & 0xff); }
	unsigned char H2() { return (unsigned char)(AltRegs.HL >> 8); }
	unsigned char L2() { return (unsigned char)(AltRegs.HL & 0xff); }

	unsigned short SP() { return _sp; }
	unsigned short PC() { return _PC; }
	unsigned short IX() { return _IX; }
	unsigned short IY() { return _IY; }

	unsigned char I() { return _I; }

	bool flag_S() { return ((Regs.AF & SF) != 0); };
	bool flag_Z() { return ((Regs.AF & ZF) != 0); };
	bool flag_H() { return ((Regs.AF & HC) != 0); };
	bool flag_P() { return ((Regs.AF & PV) != 0); };
	bool flag_N() { return ((Regs.AF & NF) != 0); };
	bool flag_C() { return ((Regs.AF & CF) != 0); };

	//We will include these as well just in case we want to get at the full register
	unsigned short AF() { return Regs.AF; }
	unsigned short BC() { return Regs.BC; }
	unsigned short DE() { return Regs.DE; }
	unsigned short HL() { return Regs.HL; }
	unsigned short AF2() { return AltRegs.AF; }
	unsigned short BC2() { return AltRegs.BC; }
	unsigned short DE2() { return AltRegs.DE; }
	unsigned short HL2() { return AltRegs.HL; }

	//Breakpoint functions
	void AddExecuteBreakpoint(unsigned short address);
	void RemoveExecuteBreakpoint(unsigned short address);
	void ClearExecuteBreakpoints() { executeBreakpoints.clear(); }
	const std::vector<unsigned short> GetExecBreakpoints() { return executeBreakpoints; }

	//Logging functions
	inline void ActivateLogging() { if (logging) return; fp = fopen("output.log", "w"); logging = (fp != NULL); }
	inline void DeactivateLogging() { if (!logging) return; logging = false; fflush(fp); fclose(fp); fp = NULL; }
	inline bool LoggingEnabled() { return logging; }



private:

	//These are the opcode handlers. There are many of them, so they are listed at the bottom
	//This macro will make defining them much faster and more convenient
#define OPCODE(o) unsigned char o(unsigned char op, unsigned char extOp)
	OPCODE(ADC);
	OPCODE(ADD);
	OPCODE(AND);
	OPCODE(BIT);
	OPCODE(CALL);
	OPCODE(CCF);
	OPCODE(CP);
	OPCODE(CPD);
	OPCODE(CPDR);
	OPCODE(CPI);
	OPCODE(CPIR);
	OPCODE(CPL);
	OPCODE(DAA);
	OPCODE(DEC);
	OPCODE(DI);
	OPCODE(DJNZ);
	OPCODE(EI);
	OPCODE(EX);
	OPCODE(EXX);
	OPCODE(HALT);
	OPCODE(IM);
	OPCODE(INC);
	OPCODE(IND);
	OPCODE(INDR);
	OPCODE(INI);
	OPCODE(INIR);
	OPCODE(INOP);
	OPCODE(JP);
	OPCODE(JR);
	OPCODE(LD);
	OPCODE(LD_ED);
	OPCODE(LDD);
	OPCODE(LDI);
	OPCODE(LDDR);
	OPCODE(LDIR);
	OPCODE(NEG);
	OPCODE(NOP);
	OPCODE(OR);
	OPCODE(OTDR);
	OPCODE(OTIR);
	OPCODE(OUTD);
	OPCODE(OUTOP);
	OPCODE(OUTI);
	OPCODE(POP);
	OPCODE(PUSH);
	OPCODE(RES);
	OPCODE(RET);
	OPCODE(RETI);
	OPCODE(RETN);
	OPCODE(RL);
	OPCODE(RLCA);
	OPCODE(RLA);
	OPCODE(RLC);
	OPCODE(RLD);
	OPCODE(RR);
	OPCODE(RRA);
	OPCODE(RRC);
	OPCODE(RRCA);
	OPCODE(RRD);
	OPCODE(RST);
	OPCODE(SBC);
	OPCODE(SCF);
	OPCODE(SET);
	OPCODE(SLA);
	OPCODE(SLL);
	OPCODE(SRA);
	OPCODE(SRL);
	OPCODE(SUB);
	OPCODE(XOR);


	OPCODE(ExtCB);			//This is not a real opcode, but it is needed to support the CB opcodes
	OPCODE(ExtIR);			//This is not a real opcode, but it is needed to support the DD anf FD opcodes
	OPCODE(ExtED);			//This is not a real opcode, but it is needed to support the ED opcodes
	OPCODE(JAM);			//This is not a real opcode. I am including it for the empty opcode slots

#undef OPCODE
};
