#include "Console.h"
#include <thread>

Console* Console::_instance = nullptr;

static std::thread* CPUThread = nullptr;
static std::thread* VCThread = nullptr;
static std::thread* SCThread = nullptr;

void RunCPU()
{
	Console::CPU()->Run();
}

Console::Console()
{
	_instance = this;
	cpu = Z80::Instance();
	ram = new RAM(0, 65536);
	rom = new ROM("rom.bin", 0);
	systemBus = new SystemBus();
	SystemBus::Instance()->AddressBus()->AttachDevice(ram, true);
	uart = new UART16C550(0);
	SystemBus::Instance()->IOBus()->AttachDevice(uart, true);
	Run();
}

Console::~Console()
{
	if (cpu)
		cpu->Stop();
	if (CPUThread)
		CPUThread->join();
	delete cpu;
	delete ram;
	delete uart;
	delete rom;
	delete systemBus;
}

void Console::Run()
{
	CPUThread = new std::thread(RunCPU);
};

void Console::LoadRom(char* path)
{
	if (rom != nullptr)
	{
		SystemBus::Instance()->AddressBus()->DetachDevice(rom);
		delete rom;
	}
	rom = new ROM(path, 0x0000);
}