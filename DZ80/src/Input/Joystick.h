#pragma once

struct JoystickDefinition
{

public:

	enum JoystickType
	{
		Keyboard,
		Joystick
	};

	char Name[50];
	JoystickType Type;
	int Index;

};

class Joystick
{

private:

	JoystickDefinition JoyDefinition;

public:

	Joystick(JoystickDefinition definition);
	unsigned char GetJoyValue();
	const JoystickDefinition& GetDefinition() { return JoyDefinition; }
	bool selected = false;
	char selectedPort = -1;

public:

	const unsigned char Up = 1;
	const unsigned char Down = 2;
	const unsigned char Left = 4;
	const unsigned char Right = 8;
	const unsigned char A = 16;
	const unsigned char B = 32;
	const unsigned char START = 64;
	const unsigned char SELECT = 128;

};