#pragma once

#include <GLFW/glfw3.h>

class KeyMap
{
public:

	static short Keys[8][8];
	static bool IsKeyPressed(int row, int col);
	static void Initialize();

};