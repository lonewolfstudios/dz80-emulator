#pragma once

#include <GLFW/glfw3.h>

#define NUM_BUTTONS		8

class JoyKeyMap
{
public:

	static short Keys[2][NUM_BUTTONS];
	static unsigned char JoyValue(unsigned char port);
	static void Initialize();

};