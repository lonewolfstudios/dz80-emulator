#include "InputManager.h"
#include "imgui.h"
#include "GLFW/glfw3.h"

InputManager* InputManager::_instance = nullptr;

InputManager::InputManager()
{
	JoystickDefinition def;
	def.Index = 0;
	def.Type = JoystickDefinition::Keyboard;
	strcpy(def.Name, "Keyset 1");
	availableJoysticks.push_back(new Joystick(def));
	def.Index = 1;
	strcpy(def.Name, "Keyset 2");
	availableJoysticks.push_back(new Joystick(def));
	selectedJoystick[0] = selectedJoystick[1] = nullptr;
	for (int i = 0; i < 16; ++i)
	{
		if (glfwJoystickPresent(i))
		{
			def.Index = i;
			def.Type = JoystickDefinition::Joystick;
			const char* joyName = glfwGetJoystickName(i);
			strcpy(def.Name, joyName);
			availableJoysticks.push_back(new Joystick(def));
		}
	}
}

InputManager::~InputManager()
{
}

unsigned char InputManager::GetJoyValue(int port)
{
	if (port != 0 && port != 1)
		return 0x00;
	if (selectedJoystick[port] != nullptr)
		return selectedJoystick[port]->GetJoyValue();
	return 0x00;
}

void InputManager::OnImGUIJoystickMenu()
{
	if (ImGui::BeginMenu("Port 1"))
	{
		if (ImGui::MenuItem("None", 0, selectedJoystick[0] == nullptr))
		{
			if (selectedJoystick[0] != nullptr)
			{
				selectedJoystick[0]->selectedPort = -1;
				selectedJoystick[0]->selected = false;
			}
			selectedJoystick[0] = nullptr;
		}
		for (auto joy : availableJoysticks)
		{
			if (joy->selectedPort != 1 && ImGui::MenuItem(joy->GetDefinition().Name, 0, &joy->selected))
			{
				if (selectedJoystick[0] != joy && selectedJoystick[0] != nullptr)
				{
					selectedJoystick[0]->selected = false;
					selectedJoystick[0]->selectedPort = -1;
				}
				joy->selected = true;
				joy->selectedPort = 0;
				selectedJoystick[0] = joy;
			}
		}
		ImGui::EndMenu();
	}
	if (ImGui::BeginMenu("Port 2"))
	{
		if (ImGui::MenuItem("None", 0, selectedJoystick[1] == nullptr))
		{
			if (selectedJoystick[1] != nullptr)
			{
				selectedJoystick[1]->selectedPort = -1;
				selectedJoystick[1]->selected = false;
			}
			selectedJoystick[1] = nullptr;
		}
		for (auto joy : availableJoysticks)
		{
			if (joy->selectedPort != 0 && ImGui::MenuItem(joy->GetDefinition().Name, 0, &joy->selected))
			{
				if (selectedJoystick[1] != joy && selectedJoystick[1] != nullptr)
				{
					selectedJoystick[1]->selected = false;
					selectedJoystick[1]->selectedPort = -1;
				}
				joy->selected = true;
				joy->selectedPort = 1;
				selectedJoystick[1] = joy;
			}
		}
		ImGui::EndMenu();
	}
	if (ImGui::MenuItem("Swap Joysticks"))
	{
		auto joy1 = selectedJoystick[1];
		selectedJoystick[1] = selectedJoystick[0];
		selectedJoystick[0] = joy1;
		if (selectedJoystick[0] != nullptr)
			selectedJoystick[0]->selectedPort = 0;
		if (selectedJoystick[1] != nullptr)
			selectedJoystick[1]->selectedPort = 1;
	}
	if (ImGui::MenuItem("Clear Joysticks"))
	{
		if (selectedJoystick[0] != nullptr)
		{
			selectedJoystick[0]->selectedPort = -1;
			selectedJoystick[0]->selected = false;
		}
		if (selectedJoystick[1] != nullptr)
		{
			selectedJoystick[1]->selectedPort = -1;
			selectedJoystick[1]->selected = false;
		}
		selectedJoystick[0] = selectedJoystick[1] = nullptr;
	}
}
