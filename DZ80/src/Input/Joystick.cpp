#include "Input/Joystick.h"
#include "Input/JoyKeyMap.h"
#include "GLFW/glfw3.h"
#include <stdio.h>

Joystick::Joystick(JoystickDefinition definition) : JoyDefinition(definition)
{
}

unsigned char Joystick::GetJoyValue()
{
	if (JoyDefinition.Type == JoystickDefinition::JoystickType::Keyboard)
	{
		return JoyKeyMap::JoyValue(JoyDefinition.Index);
	}
	else
	{
		if (!glfwJoystickPresent(JoyDefinition.Index))
			return 0b00000000;
		unsigned char value = 0;
		if (glfwJoystickIsGamepad(JoyDefinition.Index))
		{
			const char* name = glfwGetGamepadName(JoyDefinition.Index);
			GLFWgamepadstate state;
			if (glfwGetGamepadState(JoyDefinition.Index, &state))
			{
				if (state.buttons[GLFW_GAMEPAD_BUTTON_A])
					value |= A;
				if (state.buttons[GLFW_GAMEPAD_BUTTON_B])
					value |= B;
				if (state.buttons[GLFW_GAMEPAD_BUTTON_START])
					value |= START;
				if (state.buttons[GLFW_GAMEPAD_BUTTON_BACK])
					value |= SELECT;
				if (state.axes[GLFW_GAMEPAD_AXIS_LEFT_X] < -0.05f || state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_LEFT])
					value |= Left;
				else if (state.axes[GLFW_GAMEPAD_AXIS_LEFT_X] > 0.05f || state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_RIGHT])
					value |= Right;
				if (state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y] < -0.05f || state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_DOWN])
					value |= Down;
				else if (state.axes[GLFW_GAMEPAD_AXIS_LEFT_Y] > 0.05f || state.buttons[GLFW_GAMEPAD_BUTTON_DPAD_UP])
					value |= Up;
				return value;
			}
		}
		int axesCount;
		const float* axes = glfwGetJoystickAxes(JoyDefinition.Index, &axesCount);
		int buttonCount;
		const unsigned char* buttons = glfwGetJoystickButtons(JoyDefinition.Index, &buttonCount);
		if (axesCount > 1)
		{
			if (axes[0] < -0.05f)
				value |= Left;
			else if (axes[0] > 0.05f)
				value |= Right;
			if (axes[1] < -0.05f)
				value |= Up;
			else if (axes[1] > 0.05f)
				value |= Down;
		}
		if (buttons[0])
			value |= A;
		if (buttons[1])
			value |= B;
		if (buttons[2])
			value |= SELECT;
		if (buttons[3])
			value |= START;
		return value;
	}
}
