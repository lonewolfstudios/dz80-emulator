
#include "JoyKeyMap.h"
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <stdio.h>

short JoyKeyMap::Keys[2][NUM_BUTTONS] =
{
	//{Up, Down, Left, Right, A, B, Select, Start}
	{GLFW_KEY_KP_8, GLFW_KEY_KP_2, GLFW_KEY_KP_4, GLFW_KEY_KP_6, GLFW_KEY_KP_0, GLFW_KEY_KP_DECIMAL, GLFW_KEY_INSERT, GLFW_KEY_DELETE },
	{GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D, GLFW_KEY_N, GLFW_KEY_M, GLFW_KEY_J, GLFW_KEY_K }
};

//TODO: Convert this to an event system
unsigned char JoyKeyMap::JoyValue(unsigned char set)
{
	try
	{
		ImGuiIO& io = ImGui::GetIO();
		if (set > 1) return 0x00;
		unsigned char val = 0;
		for (int i = 0; i < NUM_BUTTONS; ++i)
			if (io.KeysDown[Keys[set][i]])
				val |= (1 << i);
		return val;
	}
	catch (...) { return 0x00; }
};

void JoyKeyMap::Initialize()
{
};
