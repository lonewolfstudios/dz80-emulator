#pragma once

#include "Joystick.h"
#include <vector>

class InputManager
{
private:
	InputManager();

	static InputManager* _instance;
	char JoystickNames[16][50];
	Joystick* selectedJoystick[2];
	std::vector<Joystick*> availableJoysticks;

public:
	~InputManager();
	static InputManager* Instance() { if (_instance == nullptr) _instance = new InputManager(); return _instance; }

	unsigned char GetJoyValue(int port);
	void OnImGUIJoystickMenu();

};