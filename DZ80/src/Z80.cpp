#include "Z80.h"
#include "Utility/stopwatch.h"
#include "SystemBus/SystemBus.h"
#include "Utility/InstructionDecoder.h"
#include <thread>

Z80* Z80::_instance = nullptr;

#ifdef _OSX
#define _sleep(x)	sleep(x / 1000.0f)
#endif

//Some useful macros
#define GetAddr(hi,lo)	(unsigned int)hi<<8 | lo
#define SetCF			Regs.AF = Regs.AF | CF
#define SetNF			Regs.AF = Regs.AF | NF
#define SetPV			Regs.AF = Regs.AF | PV
#define SetHC			Regs.AF = Regs.AF | HC
#define SetZF			Regs.AF |= ZF
#define SetSF			Regs.AF = Regs.AF | SF

#define ClearCF			Regs.AF = Regs.AF & (65535 ^ CF)
#define ClearNF			Regs.AF = Regs.AF & (65535 ^ NF)
#define ClearPV			Regs.AF = Regs.AF & (65535 ^ PV)
#define ClearHC			Regs.AF = Regs.AF & (65535 ^ HC)
#define ClearZF			Regs.AF = Regs.AF & (65535 ^ ZF)
#define ClearSF			Regs.AF = Regs.AF & (65535 ^ SF)

#define CFIsSet			((Regs.AF & CF) != 0)
#define NFIsSet			((Regs.AF & NF) != 0)
#define PVIsSet			((Regs.AF & PV) != 0)
#define HCIsSet			((Regs.AF & HC) != 0)
#define ZFIsSet			((Regs.AF & ZF) != 0)
#define SFIsSet			((Regs.AF & SF) != 0)

#define CFIsClear		((Regs.AF & CF) == 0)
#define NFIsClear		((Regs.AF & NF) == 0)
#define PVIsClear		((Regs.AF & PV) == 0)
#define HCIsClear		((Regs.AF & HC) == 0)
#define ZFIsClear		((Regs.AF & ZF) == 0)
#define SFIsClear		((Regs.AF & SF) == 0)

#define SetA(v)			Regs.AF = ((unsigned char)(v) << 8) | (Regs.AF & 0xff)
#define SetF(v)			Regs.AF = ((unsigned char)(v)) | (Regs.AF & 0xff00)
#define SetB(v)			Regs.BC = ((unsigned char)(v) << 8) | (Regs.BC & 0xff)
#define SetC(v)			Regs.BC = ((unsigned char)(v)) | (Regs.BC & 0xff00)
#define SetD(v)			Regs.DE = ((unsigned char)(v) << 8) | (Regs.DE & 0xff)
#define SetE(v)			Regs.DE = ((unsigned char)(v)) | (Regs.DE & 0xff00)
#define SetH(v)			Regs.HL = ((unsigned char)(v) << 8) | (Regs.HL & 0xff)
#define SetL(v)			Regs.HL = ((unsigned char)(v)) | (Regs.HL & 0xff00)

#define UpdateMemAccessed(addr, data) if (logging) { memAccessed = true; lastAddress = addr; lastData = data; }

//This will store the displacement for some of the (IX+d)/(IY+d) opcodes
static char displacement = 0;

Z80::Z80()
{
	Reset();
}

void Z80::Reset()
{
	Regs.AF = Regs.BC = Regs.DE = Regs.HL = AltRegs.AF = AltRegs.BC = AltRegs.DE = AltRegs.HL = 0;
	_IX = _IY = _sp = _PC = 0;
	_I = 0;
	IFF1 = IFF2 = false;
	SystemBus::Instance()->Reset();
}

Z80::~Z80()
{
	if (logging)
		DeactivateLogging();
	while (accessingBreakpoints) {}
}

void Z80::CreateFlagsString(char* str)
{
	char Flags[] = "SZ-H-PNC";
	for (int i = 0; i < 8; ++i)
		if (Regs.AF & (128 >> i))
			str[i] = Flags[i];
		else
			str[i] = '-';
	str[8] = 0;
}

void Z80::CreateAltFlagsString(char* str)
{
	char Flags[] = "SZ-H-PNC";
	for (int i = 0; i < 8; ++i)
		if (AltRegs.AF & (128 >> i))
			str[i] = Flags[i];
		else
			str[i] = '-';
	str[8] = 0;
}

void Z80::HandleMaskableInterrupt()
{

}

void Z80::HandleNonmaskableInterrupt()
{

}

void Z80::StartMaskableInterruptRoutine(unsigned short vector)
{

}

void Z80::StartNonmaskableInterruptRoutine()
{

}

bool Z80::needExecBreak()
{
	while (accessingBreakpoints)
	{
	}
	accessingBreakpoints = true;
	auto it = std::find(executeBreakpoints.begin(), executeBreakpoints.end(), _PC);
	if (it != executeBreakpoints.end())
	{
		accessingBreakpoints = false;
		return true; //It is in the list
	}
	accessingBreakpoints = false;
	return false; //It isn't in the list
}

void Z80::AddExecuteBreakpoint(unsigned short address)
{
	while (accessingBreakpoints)
	{
	}
	accessingBreakpoints = true;
	auto it = std::find(executeBreakpoints.begin(), executeBreakpoints.end(), address);
	if (it != executeBreakpoints.end())
		return; //It has already been added
	executeBreakpoints.emplace_back(address);
	accessingBreakpoints = false;
}

void Z80::RemoveExecuteBreakpoint(unsigned short address)
{
	while (accessingBreakpoints)
	{
	}
	accessingBreakpoints = true;
	auto it = std::find(executeBreakpoints.begin(), executeBreakpoints.end(), address);
	if (it == executeBreakpoints.end())
	{
		accessingBreakpoints = false;
		return; //It wasn't found
	}
	accessingBreakpoints = false;
	executeBreakpoints.erase(it);
}

bool Z80::MaskableInterrupt()
{
	if (!IFF1 || maskableInterruptWaiting)
		return false;
	if (IFF1)
	{
		maskableInterruptWaiting = true;
	}
	return true;
}

void Z80::NonmaskableInterrupt()
{
	nonmaskableInterruptWaiting = true;
}

//This function is to be run on its own thread and will continuously run the CPU
//It will try to keep it at the appropriate speed but will run each instruction
//as an atomic unit that cannot be interrupted.
void Z80::Run()
{
	running = true;
	while (running)
	{
		while (singleStep) //If we are singleStepping, we do not continue to run
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			if (!running)
				return;
		}
		{
			stopwatch timer;
			unsigned char cycles = DoNextOpcode();
			cycleCount += cycles;
			SystemBus::Instance()->AdvanceCycles(cycles); //Keep the bus devices in step with the CPU
			long long time = (long long)((double)cycles * SystemDefs::CYCLE_NANOSECS);
			long long elapsed = timer.elapsed_time<long long, std::chrono::nanoseconds>();
			while (elapsed < time)
				elapsed = timer.elapsed_time<long long, std::chrono::nanoseconds>();
		}
	}
}

unsigned char Z80::DoSingleStep()
{
	if (!singleStep)
		return 0;
	unsigned char cycles = DoNextOpcode();
	SystemBus::Instance()->AdvanceCycles(cycles); //Keep the bus devices in step with the CPU
	return cycles;
}

unsigned char Z80::DoNextOpcode()
{
	unsigned char op;

	//First, check for waiting interrupts
	if (maskableInterruptWaiting)
		HandleMaskableInterrupt();

	//Is there a breakpoint here?
	if (!singleStep && needExecBreak())
	{
		singleStep = true;
		return 0;
	}

	if (logging && fp != NULL)
	{
		if (!firstLoop)
		{
			if (memAccessed)
			{
				fprintf(fp, " $%.4x (%.5d) = $%.2x (%.3d)\n", lastAddress, lastAddress, lastData, lastData);
			}
			else
				fprintf(fp, "\n");
		}
		else
			firstLoop = false;
		char instruction[20] = "";
		char header[26] = "";
		char flags[10] = "";
		int cycles, bytes;
		bool undocumented;
		bool legal;
		InstructionDecoder::Instance()->Decode(instruction, _PC, cycles, bytes, undocumented, legal);
		CreateFlagsString(flags);
		sprintf(header, "$%.4x -> %s", _PC, instruction);
		while (strlen(header) < 23)
			strcat(header, " ");
		fprintf(fp, "%s : {AF = $%.4x (%.5d) BC = $%.4x (%.5d) DE = $%.4x (%.5d) HL = $%.4x (%.5d) IX = $%.4x (%.5d) IY = $%.4x (%.5d) SP = $%.4x (%.5d) FLAGS = %s}", header, Regs.AF, Regs.AF, Regs.BC, Regs.BC, Regs.DE, Regs.DE, Regs.HL, Regs.HL, _IX, _IX, _IY, _IY, _sp, _sp, flags);
		memAccessed = false;
	}

	op = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	if (opcodeHandlers[op] != nullptr)
		return std::invoke(opcodeHandlers[op], this, op, 0);
	else
		return 0; //There is no opcode handler, so return 0 as an illegal opcode
}

//These are the opcode handler functions

//A helper function used by some of the opcode handlers
static bool charInList(const unsigned char* list, const int& count, const unsigned char& search)
{
	for (int i = 0; i < count; ++i)
		if (list[i] == search)
			return true;
	return false;
}

static unsigned char smallRegLDOpcodes[] = { 0x7f, 0x78, 0x79, 0x7a, 0x7b, 0x7c, 0x7d,
											 0x47, 0x40, 0x41, 0x42, 0x43, 0x44, 0x45,
											 0x4f, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d,
											 0x57, 0x50, 0x51, 0x52, 0x53, 0x54, 0x55,
											 0x5f, 0x58, 0x59, 0x5a, 0x5b, 0x5c, 0x5d,
											 0x67, 0x60, 0x61, 0x62, 0x63, 0x64, 0x65,
											 0x6f, 0x68, 0x69, 0x6a, 0x6b, 0x6c, 0x6d };

static unsigned char reg8bitLD[] = { 0x3e, 0x06, 0x0e, 0x16, 0x1e, 0x26, 0x2e };
static unsigned char FromHLLDOpcodes[] = { 0x7e, 0x46, 0x4e, 0x56, 0x5e, 0x66, 0x6e };
static unsigned char ToHLLDOpcodes[] = { 0x77, 0x70, 0x71, 0x72, 0x73, 0x74, 0x75, 0x36 };
static unsigned char A16BitLDOpcodes[] = { 0x3a, 0x0a, 0x1a, 0x32, 0x02, 0x12 };
static unsigned char RPDirectLDOpcodes[] = { 0x01, 0x11, 0x21, 0x31 };

//This macro will make the declaration easier
#define OPCODE(o) unsigned char Z80::o(unsigned char op, unsigned char op2)

OPCODE(ADC)
{
	unsigned char cycles;
	char value;
	short lvalue;
	if (((op & 0x80) == 0x80) || op == 0xce)
	{
		cycles = 4;
		switch (op)
		{
		case 0x88:
			value = (char)B();
			break;
		case 0x89:
			value = (char)C();
			break;
		case 0x8a:
			value = (char)D();
			break;
		case 0x8b:
			value = (char)E();
			break;
		case 0x8c:
			value = (char)H();
			break;
		case 0x8d:
			value = (char)L();
			break;
		case 0x8e:
			unsigned short address;
			char d;
			switch (op2)
			{
			case 0:
				address = Regs.HL;
				cycles = 7;
				break;
			case 0xdd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IX + d;
				cycles = 19;
				break;
			case 0xfd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IY + d;
				cycles = 19;
				break;
			}
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(address);
			UpdateMemAccessed(address, (unsigned char)value);
			break;
		case 0x8f:
			value = (char)A();
			break;
		case 0xce:
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			cycles = 7;
			break;
		}
		lvalue = (short)(unsigned char)A();
		unsigned char hcCheck = A() & 0x0f;
		lvalue += value + (CFIsSet ? 1 : 0);
		SetA((lvalue & 0xff));
		ClearNF;
		if ((hcCheck + (value & 0x0f) + (CFIsSet ? 1 : 0)) & 0x10) SetHC; else ClearHC;
		if (lvalue > 255) SetCF; else ClearCF;
		if (lvalue > 127 || lvalue < -128) SetPV; else ClearPV;
		if (A() == 0) SetZF; else ClearZF;
		if ((A() & 0x80)) SetSF; else ClearSF;
	}
	else if (op2 == 0xed && (op == 0x4a || op == 0x5a || op == 0x6a || op == 0x7a))
	{
		int llvalue;
		cycles = 15;
		switch (op)
		{
		case 0x09:
			lvalue = (short)Regs.BC;
			break;
		case 0x19:
			lvalue = (short)Regs.DE;
			break;
		case 0x29:
			lvalue = (short)Regs.HL;
			break;
		case 0x39:
			lvalue = (short)_sp;
			break;
		}
		llvalue = (int)(short)Regs.HL;
		int oldll = llvalue;
		llvalue += lvalue + (CFIsSet ? 1 : 0);
		oldll &= 0x0fff;
		oldll += (lvalue & 0xfff) + (CFIsSet ? 1 : 0);
		if (llvalue > 65535) SetCF; else ClearCF;
		llvalue &= 0xffff;
		ClearNF;
		//Check for the half carry
		if (oldll & 0x1000) SetHC; else ClearHC;
		Regs.HL = (unsigned short)llvalue;
	}
	return cycles;
}

OPCODE(ADD)
{
	unsigned char cycles;
	char value;
	short lvalue;
	if (((op & 0x80) == 0x80) || op == 0xc6)
	{
		cycles = 4;
		switch (op)
		{
		case 0x80:
			value = (char)B();
			break;
		case 0x81:
			value = (char)C();
			break;
		case 0x82:
			value = (char)D();
			break;
		case 0x83:
			value = (char)E();
			break;
		case 0x84:
			value = (char)H();
			break;
		case 0x85:
			value = (char)L();
			break;
		case 0x86:
			unsigned short address;
			char d;
			switch (op2)
			{
			case 0:
				address = Regs.HL;
				cycles = 7;
				break;
			case 0xdd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IX + d;
				cycles = 19;
				break;
			case 0xfd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IY + d;
				cycles = 19;
				break;
			}
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(address);
			UpdateMemAccessed(address, (unsigned char)value);
			break;
		case 0x87:
			value = (char)A();
			break;
		case 0xc6:
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			cycles = 7;
			break;
		}
		lvalue = (short)(unsigned char)A();
		unsigned char hcCheck = A() & 0x0f;
		lvalue += value;
		SetA((lvalue & 0xff));
		ClearNF;
		if (lvalue > 255) SetCF; else ClearCF;
		if (lvalue > 127 || lvalue < -128) SetPV; else ClearPV;
		if (A() == 0) SetZF; else ClearZF;
		if ((A() & 0x80)) SetSF; else ClearSF;
		if ((hcCheck + (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	}
	else if (op == 0x09 || op == 0x19 || op == 0x29 || op == 0x39)
	{
		int llvalue;
		switch (op)
		{
		case 0x09:
			lvalue = (short)Regs.BC;
			break;
		case 0x19:
			lvalue = (short)Regs.DE;
			break;
		case 0x29:
			if (op2 == 0) lvalue = (short)Regs.HL;
			else if (op2 == 0xdd) lvalue = (short)_IX;
			else if (op2 == 0xfd) lvalue = (short)_IY;
			else return 0;
			break;
		case 0x39:
			lvalue = (short)_sp;
			break;
		}
		switch (op2)
		{
		case 0:
			llvalue = (int)(short)Regs.HL;
			cycles = 11;
			break;
		case 0xdd:
			llvalue = (int)(short)_IX;
			cycles = 15;
			break;
		case 0xfd:
			llvalue = (int)(short)_IY;
			cycles = 15;
			break;
		}
		int oldll = llvalue;
		llvalue += lvalue;
		if (llvalue > 65535) SetCF; else ClearCF;
		llvalue &= 0xffff;
		ClearNF;
		//Check for the half carry
		oldll &= 0x0fff;
		oldll += (lvalue & 0xfff);
		if (oldll & 0x1000) SetHC; else ClearHC;
		switch (op2)
		{
		case 0:
			Regs.HL = (unsigned short)llvalue;
			break;
		case 0xdd:
			_IX = (unsigned short)llvalue;
			break;
		case 0xfd:
			_IY = (unsigned short)llvalue;
			break;
		}
	}
	return cycles;
}

OPCODE(AND)
{
	unsigned char cycles = 0;
	unsigned char value;
	if ((op & 0xa0) == 0xa0)
	{
		cycles = 15;
		switch (op)
		{
		case 0xa0:
			value = B();
			break;
		case 0xa1:
			value = C();
			break;
		case 0xa2:
			value = D();
			break;
		case 0xa3:
			value = E();
			break;
		case 0xa4:
			value = H();
			break;
		case 0xa5:
			value = L();
			break;
		case 0xa6:
		{
			cycles = 19;
			unsigned short address = 0;
			char d;
			switch (op2)
			{
			case 0:
				address = Regs.HL;
				cycles = 7;
				break;
			case 0xdd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IX + d;
				break;
			case 0xfd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IY + d;
				break;
			}
			value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
			UpdateMemAccessed(address, value);
		}
		break;
		case 0xe6:
			cycles = 7;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			break;
		}
	}
	else
		return 0;
	value &= A();
	SetA(value);
	SetHC;
	ClearNF;
	ClearCF;
	if (value == 0) SetCF; else ClearCF;
	if (value & 0x80) SetSF; else ClearSF;
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	return cycles;
}

OPCODE(CALL)
{
	unsigned short address;
	bool doCall = false;
	unsigned char cycles = 17;
	address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
	switch (op)
	{
	case 0xcd:
		doCall = true;
		break;
	case 0xc4:
		if (ZFIsClear) doCall = true;
		break;
	case 0xcc:
		if (ZFIsSet) doCall = true;
		break;
	case 0xd4:
		if (CFIsClear) doCall = true;
		break;
	case 0xdc:
		if (CFIsSet) doCall = true;
		break;
	case 0xe4:
		if (PVIsClear) doCall = true;
		break;
	case 0xec:
		if (PVIsSet) doCall = true;
		break;
	case 0xf4:
		if (SFIsClear) doCall = true;
		break;
	case 0xfc:
		if (SFIsSet) doCall = true;
		break;
	default: //Should NEVER happen!
		return 0;
	}
	if (!doCall && op != 0xcd) cycles = 10;
	if (doCall)
	{
		//Push the return address
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_PC >> 8));
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_PC & 0xff));
		_PC = address;
	}
	return cycles;
}

OPCODE(CCF)
{
	if (CFIsSet)
		ClearCF;
	else
		SetCF;
	return 4;
}

OPCODE(CP)
{
	unsigned char cycles = 4;
	unsigned char value;
	switch (op)
	{
	case 0xb8:
		value = B();
		break;
	case 0xb9:
		value = C();
		break;
	case 0xba:
		value = D();
		break;
	case 0xbb:
		value = E();
		break;
	case 0xbc:
		value = H();
		break;
	case 0xbd:
		value = L();
		break;
	case 0xbe:
		cycles = 19;
		unsigned short address;
		char d;
		switch (op2)
		{
		case 0:
			address = Regs.HL;
			cycles = 7;
			break;
		case 0xdd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IX + d;
			break;
		case 0xfe:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IY + d;
			break;
		}
		value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, value);
	case 0xbf:
		value = A();
		break;
	case 0xfe:
		cycles = 7;
		value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		break;
	default:
		return 0; // Just in case! But this should never happen!
	}
	char a = (char)A();
	char v = (char)value;
	short pv = (short)a - (short)v;
	char hc = (a & 0x0f) - (v & 0x0f);
	if (hc & 0x10) SetHC; else ClearHC;
	a = a - v;
	if (a < 0) { SetCF; ClearZF; }
	else if (a == 0) { SetZF; ClearCF; }
	else { ClearZF; ClearCF; }
	SetNF;
	if (A() & 0x80) SetSF; else ClearSF;
	if (pv < -128 || pv > 127) SetPV; else ClearPV;
	return cycles;
}

OPCODE(CPD)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	Regs.HL--;
	Regs.BC--;
	if (A() < value)
	{
		SetSF;
		ClearZF;
	}
	else if (A() == value)
	{
		SetZF;
		ClearSF;
	}
	else
	{
		ClearZF;
		ClearSF;
	}
	if (Regs.BC == 0) ClearPV; else SetPV;
	if (((A() & 0x0f) - (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	SetNF;
	return 16;
}

OPCODE(CPDR)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	Regs.HL--;
	Regs.BC--;
	if (A() < value)
	{
		SetSF;
		ClearZF;
	}
	else if (A() == value)
	{
		SetZF;
		ClearSF;
	}
	else
	{
		ClearZF;
		ClearSF;
	}
	if (((A() & 0x0f) - (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	SetNF;
	if (Regs.BC == 0)
	{
		ClearPV;
		return 16;
	}
	else
	{
		SetPV;
		_PC -= 2;
		return 21;
	}
}

OPCODE(CPI)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	Regs.HL++;
	Regs.BC--;
	if (A() < value)
	{
		SetSF;
		ClearZF;
	}
	else if (A() == value)
	{
		SetZF;
		ClearSF;
	}
	else
	{
		ClearZF;
		ClearSF;
	}
	if (Regs.BC == 0) ClearPV; else SetPV;
	if (((A() & 0x0f) - (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	SetNF;
	return 16;
}

OPCODE(BIT)
{
	int bit = (op >> 4) - 4;
	bit *= 2;
	if (op & 0x08)
		bit++;
	bit = 1 << bit;
	bool value = false;
	int cycles = 8;
	switch (op & 0x0f)
	{
	case 0:
	case 8:
		value = (B() & bit) == 0;
		break;
	case 1:
	case 9:
		value = (C() & bit) == 0;
		break;
	case 2:
	case 0xa:
		value = (D() & bit) == 0;
		break;
	case 3:
	case 0xb:
		value = (E() & bit) == 0;
		break;
	case 4:
	case 0xc:
		value = (H() & bit) == 0;
		break;
	case 5:
	case 0xd:
		value = (L() & bit) == 0;
		break;
	case 6:
	case 0xe:
	{
		cycles = 20;
		unsigned short address = 0;
		char d;
		switch (op2)
		{
		case 0x0:
			address = Regs.HL;
			cycles = 12;
			break;
		case 0xdd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IX + d;
			break;
		case 0xfe:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IY + d;
			break;
		}
		d = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, d);
		value = (d & bit) == 0;
	}
	break;
	case 7:
	case 0xf:
		value = (A() & bit) == 0;
		break;
	}
	if (value) SetZF; else ClearZF;
	ClearNF;
	SetHC;
	return cycles;
}

OPCODE(CPIR)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	Regs.HL++;
	Regs.BC--;
	if (A() < value)
	{
		SetSF;
		ClearZF;
	}
	else if (A() == value)
	{
		SetZF;
		ClearSF;
	}
	else
	{
		ClearZF;
		ClearSF;
	}
	if (((A() & 0x0f) - (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	SetNF;
	if (Regs.BC == 0)
	{
		ClearPV;
		return 16;
	}
	else
	{
		SetPV;
		_PC -= 2;
		return 21;
	}
}

OPCODE(CPL)
{
	SetA(~A());
	SetHC;
	SetNF;
	return 0;
}

OPCODE(DAA)
{
	unsigned short value = (unsigned short)A();
	if (((value & 0x0f) > 0x09) || HCIsSet)
	{
		value += 0x06;
		SetHC;
	}
	else
		ClearHC;
	if (((value & 0x0f) > 0x90) || CFIsSet)
	{
		value += 0x60;
		SetCF;
	}
	else
		ClearCF;
	value &= 0xff;
	SetA(value);
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	return 4;
}

OPCODE(DEC)
{
	unsigned char cycles = 0;
	unsigned short address;
	char d;
	char value;
	char oldValue;
	if ((op & 0x0b) == 0x0b) //Is it a register pair or index?
	{
		switch (op)
		{
		case 0x0b:
			Regs.BC--;
			return 6;
		case 0x1b:
			Regs.DE--;
			return 6;
		case 0x3b:
			_sp--;
			return 6;
		case 0x2b:
			switch (op2)
			{
			case 0:
				Regs.HL--;
				return 6;
			case 0xdd:
				_IX--;
				return 10;
			case 0xfd:
				_IY--;
				return 10;
			}
		}
	}
	if (op == 0x35)
	{
		switch (op2)
		{
		case 0:
			address = Regs.HL;
			cycles = 11;
			break;
		case 0xdd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IX + d;
			cycles = 23;
			break;
		case 0xfd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IY + d;
			cycles = 23;
			break;
		}
		value = oldValue = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		value--;
		SystemBus::Instance()->AddressBus()->WriteAddress(address, (unsigned char)value);
		UpdateMemAccessed(address, (unsigned char)value);
		return cycles;
	}
	cycles = 4;
	switch (op)
	{
	case 0x3d:
		oldValue = A();
		value = oldValue - 1;
		SetA(value);
		break;
	case 0x05:
		oldValue = B();
		value = oldValue - 1;
		SetB(value);
		break;
	case 0x0d:
		oldValue = C();
		value = oldValue - 1;
		SetC(value);
		break;
	case 0x15:
		oldValue = D();
		value = oldValue - 1;
		SetD(value);
		break;
	case 0x1d:
		oldValue = E();
		value = oldValue - 1;
		SetE(value);
		break;
	case 0x25:
		oldValue = H();
		value = oldValue - 1;
		SetH(value);
		break;
	case 0x2d:
		oldValue = L();
		value = oldValue - 1;
		SetL(value);
		break;
	}
	if (value == 0) SetZF; else ClearZF;
	if (value < 0) SetSF; else ClearSF;
	SetNF;
	if ((oldValue & 0x0f) == 0) SetHC; else ClearHC;
	if (oldValue == -128) SetPV; else ClearPV;
	return cycles;
}

OPCODE(DI)
{
	IFF1 = IFF2 = false;
	return 4;
}

OPCODE(DJNZ)
{
	unsigned char value = B();
	value--;
	SetB(value);
	if (value == 0)
	{
		_PC++; //Skip a byte
		return 8;
	}
	char offset = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	_PC += offset;
	return 13;
}

OPCODE(EI)
{
	IFF1 = IFF2 = true;
	return 4;
}

OPCODE(EX)
{
	switch (op)
	{
	case 0x08:
		Regs.AF ^= AltRegs.AF;
		AltRegs.AF ^= Regs.AF;
		Regs.AF ^= AltRegs.AF;
		return 4;
	case 0xeb:
		Regs.HL ^= Regs.DE;
		Regs.DE ^= Regs.HL;
		Regs.HL ^= Regs.DE;
		return 4;
	case 0xe3:
		unsigned char cycles = 23;
		unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(_sp);
		switch (op2)
		{
		case 0:
			Regs.HL ^= value;
			value ^= Regs.HL;
			Regs.HL ^= value;
			cycles = 19;
			break;
		case 0xdd:
			_IX ^= value;
			value ^= _IX;
			_IX ^= value;
			break;
		case 0xfd:
			_IY ^= value;
			value ^= _IY;
			_IY ^= value;
			break;
		}
		SystemBus::Instance()->AddressBus()->WriteAddress(_sp, value);
		UpdateMemAccessed(_sp, value);
		return cycles;
	}
	return 0;
}

OPCODE(EXX)
{
	Regs.BC ^= AltRegs.BC;
	AltRegs.BC ^= Regs.BC;
	Regs.BC ^= AltRegs.BC;
	Regs.DE ^= AltRegs.DE;
	AltRegs.DE ^= Regs.DE;
	Regs.DE ^= AltRegs.DE;
	Regs.HL ^= AltRegs.HL;
	AltRegs.HL ^= Regs.HL;
	Regs.HL ^= AltRegs.HL;
	return 4;
}

OPCODE(HALT)
{
	halted = true;
	return 4;
}

OPCODE(IM)
{
	switch (op)
	{
	case 0x46:
		InterruptMode = 0;
		break;
	case 0x56:
		InterruptMode = 1;
		break;
	case 0x5e:
		InterruptMode = 2;
		break;
	}
	return 8;
}

OPCODE(INC)
{
	unsigned char cycles = 0;
	unsigned char oldValue;
	unsigned char value;
	if ((op & 0x03))
	{
		if (op2 == 0)
		{
			switch (op)
			{
			case 0x03:
				Regs.BC++;
				break;
			case 0x13:
				Regs.DE++;
				break;
			case 0x23:
				Regs.HL++;
				break;
			case 0x33:
				_sp++;
				break;
			}
			return 6;
		}
		if (op2 == 0xdd)
			_IX++;
		else if (op2 == 0xfd)
			_IY++;
		return 10;
	}
	else if (op == 0x34)
	{
		unsigned short address;
		char d;
		if (op2 != 0)
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		switch (op2)
		{
		case 0:
			address = Regs.HL;
			cycles = 11;
			break;
		case 0xdd:
			address = _IX + d;
			cycles = 23;
			break;
		case 0xfd:
			address = _IY + d;
			cycles = 23;
			break;
		}
		unsigned char oldValue = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		memAccessed = true;
		lastAddress = address;
		lastData = oldValue + 1;
		SystemBus::Instance()->AddressBus()->WriteAddress(address, (unsigned char)lastData);
		ClearNF;
		if ((oldValue & 0x0f) == 0x0f)
			SetHC;
		else
			ClearHC;
		if (oldValue == 127)
			SetPV;
		else
			ClearPV;
		if (lastData == 0)
			SetZF;
		else
			ClearZF;
		if ((lastData & 0x80) == 0)
			ClearSF;
		else
			SetSF;
	}
	else
	{
		cycles = 4;
		switch (op)
		{
		case 0x3c:
			oldValue = (char)A();
			value = oldValue + 1;
			SetA(value);
			break;
		case 0x04:
			oldValue = (char)B();
			value = oldValue + 1;
			SetB(value);
			break;
		case 0x0c:
			oldValue = (char)C();
			value = oldValue + 1;
			SetC(value);
			break;
		case 0x14:
			oldValue = (char)D();
			value = oldValue + 1;
			SetD(value);
			break;
		case 0x1c:
			oldValue = (char)E();
			value = oldValue + 1;
			SetE(value);
			break;
		case 0x24:
			oldValue = (char)H();
			value = oldValue + 1;
			SetH(value);
			break;
		case 0x2c:
			oldValue = (char)L();
			value = oldValue + 1;
			SetL(value);
			break;
		}
		if (oldValue == 127) SetPV; else ClearPV;
		ClearNF;
		if ((oldValue & 0x0f) == 0x0f) SetHC; else ClearHC;
		if (value == 0) SetZF; else ClearZF;
		if (value < 0) SetSF; else ClearSF;
	}
	return cycles;
}

OPCODE(IND)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->IOBus()->ReadAddress(C());
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, value);
	SetB(B() - 1);
	Regs.HL--;
	SetNF;
	if (B() == 0) SetZF; else ClearZF;
	return 16;
}

OPCODE(INDR)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->IOBus()->ReadAddress(C());
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, value);
	SetB(B() - 1);
	Regs.HL--;
	SetNF;
	if (B() == 0)
	{
		SetZF;
		SetPV;
		return 16;
	}
	else
	{
		ClearZF;
		ClearPV;
		_PC -= 2; //Repeat the instruction
		return 21;
	}
}

OPCODE(INI)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->IOBus()->ReadAddress(C());
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, value);
	SetB(B() - 1);
	Regs.HL++;
	SetNF;
	if (B() == 0) SetZF; else ClearZF;
	return 16;
}

OPCODE(INIR)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->IOBus()->ReadAddress(C());
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, value);
	SetB(B() - 1);
	Regs.HL++;
	SetNF;
	if (B() == 0)
	{
		SetZF;
		SetPV;
		return 16;
	}
	else
	{
		ClearZF;
		ClearPV;
		_PC -= 2; //Repeat the instruction
		return 21;
	}
}

OPCODE(INOP)
{
	unsigned char cycles = 12;
	unsigned char port;
	unsigned char value;
	if (op == 0xdb)
	{
		port = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		cycles = 11;
	}
	else if (op2 == 0xed)
	{
		port = C();
	}
	else
		return 0; //Should NEVER happen!
	value = SystemBus::Instance()->IOBus()->ReadAddress(port);
	switch (op)
	{
	case 0xdb:
	case 0x78:
		SetA(value);
		break;
	case 0x40:
		SetB(value);
		break;
	case 0x48:
		SetC(value);
		break;
	case 0x50:
		SetD(value);
		break;
	case 0x58:
		SetE(value);
		break;
	case 0x60:
		SetH(value);
		break;
	case 0x68:
		SetL(value);
		break;
	}
	return cycles;
}

OPCODE(JP)
{
	unsigned short address;
	bool DoJump = false;
	unsigned char cycles = 10;
	switch (op)
	{
	case 0xc3:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		DoJump = true;
		break;
	case 0xc2:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (ZFIsClear) DoJump = true;
		break;
	case 0xca:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (ZFIsSet) DoJump = true;
		break;
	case 0xd2:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (CFIsClear) DoJump = true;
		break;
	case 0xda:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (CFIsSet) DoJump = true;
		break;
	case 0xe2:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (PVIsClear) DoJump = true;
		break;
	case 0xea:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (PVIsSet) DoJump = true;
		break;
	case 0xf2:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (SFIsClear) DoJump = true;
		break;
	case 0xfa:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8);
		if (SFIsSet) DoJump = true;
		break;
	case 0xe9:
		DoJump = true;
		switch (op2)
		{
		case 0:
			cycles = 4;
			address = Regs.HL;
			break;
		case 0xdd:
			cycles = 8;
			address = _IX;
			break;
		case 0xfd:
			cycles = 8;
			address = _IY;
			break;
		}
		break;
	default: //Should NEVER happen!
		return 0;
	}
	if (DoJump)
		_PC = address;
	return cycles;
}

OPCODE(JR)
{
	char offset = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	if (op == 0x18)
	{
		_PC += offset;
		return 12;
	}
	bool doJump = false;
	switch (op)
	{
	case 0x20:
		doJump = ZFIsClear;
		break;
	case 0x28:
		doJump = ZFIsSet;
		break;
	case 0x30:
		doJump = CFIsClear;
		break;
	case 0x38:
		doJump = CFIsSet;
		break;
	}
	if (doJump)
	{
		_PC += offset;
		return 12;
	}
	return 7;
}

//TODO:: Make sure that the ED table does not call this function!!!
//Some of the opcodes are doubled and would be quite different
OPCODE(LD)
{
	unsigned short value = 0;
	unsigned short address = 0;
	unsigned char cycles = 0;
	//Check to see if this is an ED extended opcode
	if (op2 == 0xed)
	{
		return LD_ED(op, op2);
	}
	//First, we will deal with 8-bit register copies
	if (charInList(smallRegLDOpcodes, 49, op))
	{
		unsigned char toCopy;
		switch (op & 0xf)
		{
		case 0xf:
		case 0x7:
			toCopy = A();
			break;
		case 0x8:
		case 0x0:
			toCopy = B();
			break;
		case 0x9:
		case 0x1:
			toCopy = C();
			break;
		case 0xa:
		case 0x2:
			toCopy = D();
			break;
		case 0xb:
		case 0x3:
			toCopy = E();
			break;
		case 0xc:
		case 0x4:
			toCopy = H();
			break;
		case 0xd:
		case 0x5:
			toCopy = L();
			break;
		}
		switch (op >> 4)
		{
		case 0x7:
			SetA(toCopy);
			break;
		case 0x4:
			if (op & 0x08)
				SetC(toCopy);
			else
				SetB(toCopy);
			break;
		case 0x5:
			if (op & 0x08)
				SetE(toCopy);
			else
				SetD(toCopy);
			break;
		case 0x6:
			if (op & 0x08)
				SetL(toCopy);
			else
				SetH(toCopy);
			break;
		}
		return 4;
	}
	//Then, 8-bit register loads
	if (charInList(reg8bitLD, 7, op))
	{
		unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		switch (op)
		{
		case 0x3e:
			SetA(value);
			break;
		case 0x06:
			SetB(value);
			break;
		case 0x0e:
			SetC(value);
			break;
		case 0x16:
			SetD(value);
			break;
		case 0x1e:
			SetE(value);
			break;
		case 0x26:
			SetH(value);
			break;
		case 0x2e:
			SetL(value);
			break;
		}
		return 7;
	}
	//Then, the from (HL) opcodes (These are also used for the (IX+d) and (IY+d) codes)
	if (charInList(FromHLLDOpcodes, 7, op))
	{
		unsigned char value;
		unsigned char cycles = 7;
		if (op2 == 0)
		{
			value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
			lastAddress = Regs.HL;
		}
		else
		{
			char d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			if (op2 == 0xdd)
			{
				value = SystemBus::Instance()->AddressBus()->ReadAddress(_IX + d);
				lastAddress = _IX + d;
			}
			else if (op2 = 0xfd)
			{
				value = SystemBus::Instance()->AddressBus()->ReadAddress(_IY + d);
				lastAddress = _IY + d;
			}
			else
				return 0; //There was a MAJOR problem!!!
			cycles = 19;
		}
		switch (op)
		{
		case 0x7e:
			SetA(value);
			break;
		case 0x46:
			SetB(value);
			break;
		case 0x4e:
			SetC(value);
			break;
		case 0x56:
			SetD(value);
			break;
		case 0x5e:
			SetE(value);
			break;
		case 0x66:
			SetH(value);
			break;
		case 0x6e:
			SetL(value);
			break;
		}
		memAccessed = true;
		lastData = value;
		return cycles;
	}
	//Now, the to (HL) opcodes (These are also used for the (IX+d) and (IY+d) codes)
	if (charInList(ToHLLDOpcodes, 8, op))
	{
		unsigned char value;
		unsigned char cycles = 7;
		unsigned short address;
		if (op2 == 0)
		{
			address = Regs.HL;
		}
		else
		{
			char d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			if (op2 == 0xdd)
				address = _IX + d;
			else if (op2 = 0xfd)
				address = _IY + d;
			else
				return 0; //There was a MAJOR problem!!!
			cycles = 19;
		}
		switch (op)
		{
		case 0x77:
			value = A();
			break;
		case 0x70:
			value = B();
			break;
		case 0x71:
			value = C();
			break;
		case 0x72:
			value = D();
			break;
		case 0x73:
			value = E();
			break;
		case 0x74:
			value = H();
			break;
		case 0x75:
			value = L();
			break;
		case 0x36:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			if (op2 == 0)
				cycles = 10;
			break;
		}
		SystemBus::Instance()->AddressBus()->WriteAddress(address, value);
		memAccessed = true;
		lastAddress = address;
		lastData = value;
		return cycles;
	}
	//Now, the 16-bit address to/from A opcodes
	if (charInList(A16BitLDOpcodes, 6, op))
	{
		unsigned char cycles = 7;
		if (op == 0x3a || op == 0x32)
		{
			unsigned short address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = address | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8;
			lastAddress = address;
			if (op == 0x3a)
				SetA(SystemBus::Instance()->AddressBus()->ReadAddress(address));
			else
				SystemBus::Instance()->AddressBus()->WriteAddress(address, A());
			cycles = 13;
		}
		else switch (op)
		{
		case 0x0a:
			lastAddress = Regs.BC;
			SetA(SystemBus::Instance()->AddressBus()->ReadAddress(lastAddress));
			break;
		case 0x1a:
			lastAddress = Regs.DE;
			SetA(SystemBus::Instance()->AddressBus()->ReadAddress(lastAddress));
			break;
		case 0x02:
			lastAddress = Regs.BC;
			SystemBus::Instance()->AddressBus()->WriteAddress(lastAddress, A());
			break;
		case 0x12:
			lastAddress = Regs.DE;
			SystemBus::Instance()->AddressBus()->WriteAddress(lastAddress, A());
			break;
		}
		memAccessed = true;
		lastData = A();
		return cycles;
	}
	if (charInList(RPDirectLDOpcodes, 4, op))
	{
		unsigned short value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		value = value | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8;
		lastData = value;
		unsigned char cycles = 10;
		switch (op)
		{
		case 0x01:
			Regs.BC = value;
			break;
		case 0x11:
			Regs.DE = value;
			break;
		case 0x31:
			_sp = value;
			break;
		case 0x21:
			switch (op2)
			{
			case 0:
				Regs.HL = value;
				break;
			case 0xdd:
				_IX = value;
				cycles = 14;
				break;
			case 0xfd:
				_IY = value;
				cycles = 14;
				break;
			}
		}
		return cycles;
	}
	//Use a switch for the remainder
	switch (op)
	{
	case 0x2a:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address = address | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8;
		value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		value = value | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(address + 1) << 8;
		memAccessed = true;
		lastAddress = address;
		lastData = value;
		switch (op2)
		{
		case 0:
			Regs.HL = value;
			return 16;
		case 0xdd:
			_IX = value;
			break;
		case 0xfd:
			_IY = value;
			break;
		}
		return 20;
	case 0x22:
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address = address | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8;
		cycles = 20;
		switch (op2)
		{
		case 0:
			value = Regs.HL;
			cycles = 16;
			break;
		case 0xdd:
			value = _IX;
			break;
		case 0xfd:
			value = _IY;
			break;
		}
		memAccessed = true;
		lastAddress = address;
		lastData = value;
		SystemBus::Instance()->AddressBus()->WriteAddress(address, value & 0xff);
		SystemBus::Instance()->AddressBus()->WriteAddress(address + 1, value >> 8);
		return cycles;
	case 0xf9:
		switch (op2)
		{
		case 0:
			_sp = Regs.HL;
			return 6;
		case 0xdd:
			_sp = _IX;
			return 10;
		case 0xfd:
			_sp = _IY;
			return 10;
		}
	}
	return 0;
}

OPCODE(LD_ED)
{
	unsigned short address;
	unsigned short value;
	if ((op & 0x0f) == 0x0b || (op & 0x0f) == (0x03))
	{
		address = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		address = address | (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++) << 8;
		memAccessed = true;
		lastAddress = address;
		if ((op & 0x0f) == 0x0b)
		{
			value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
			value = value | ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(address + 1) << 8);
			lastData = value;
			switch (op)
			{
			case 0x4b:
				Regs.BC = value;
				return 20;
			case 0x5b:
				Regs.DE = value;
				return 20;
			case 0x6b:
				Regs.HL = value;
				return 20;
			case 0x7b:
				_sp = value;
				return 20;
			}
		}
		else
		{
			value;
			switch (op)
			{
			case 0x4b:
				value = Regs.BC;
				break;
			case 0x5b:
				value = Regs.DE;
				break;
			case 0x6b:
				value = Regs.HL;
				break;
			case 0x7b:
				value = _sp;
				break;
			}
			lastData = value;
			SystemBus::Instance()->AddressBus()->WriteAddress(address, value & 0xff);
			SystemBus::Instance()->AddressBus()->WriteAddress(address, value >> 8);
			return 20;
		}
	}
	switch (op)
	{
	case 0x57:
		SetA(_I);
		return 9;
	case 0x5f:
		SetA(R);
		return 9;
	case 0x47:
		_I = A();
		return 9;
	case 0x4f:
		R = A();
		return 9;
	}
	return 8;
}

OPCODE(LDD)
{
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.DE, SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL));
	Regs.BC--;
	Regs.DE--;
	Regs.HL--;
	ClearHC;
	ClearNF;
	if (Regs.BC == 0) ClearPV; else SetPV;
	return 16;
}

OPCODE(LDDR)
{
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.DE, SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL));
	Regs.BC--;
	Regs.HL--;
	Regs.DE--;
	ClearHC;
	ClearNF;
	ClearPV;
	if (Regs.BC == 0)
	{
		SetZF;
		return 16;
	}
	else
	{
		_PC -= 2;
		return 21;
	}
}

OPCODE(LDI)
{
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.DE, SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL));
	Regs.BC--;
	Regs.HL++;
	Regs.DE++;
	ClearHC;
	ClearNF;
	if (Regs.BC == 0) ClearPV; else SetPV;
	return 16;
}

OPCODE(LDIR)
{
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.DE, SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL));
	Regs.BC--;
	Regs.HL++;
	Regs.DE++;
	ClearHC;
	ClearNF;
	ClearPV;
	if (Regs.BC == 0)
	{
		SetZF;
		return 16;
	}
	else
	{
		_PC -= 2;
		return 21;
	}
}

OPCODE(NEG)
{
	unsigned char oldValue = A();
	unsigned char newValue = 0 - oldValue;
	short overflow = 0 - (char)oldValue;
	SetA(newValue);
	if (newValue & 0x80) SetSF; else ClearSF;
	if (newValue == 0) SetZF; else ClearZF;
	if (overflow > 127 || overflow < -128) SetPV; else ClearPV;
	if (overflow & 0x100) SetCF; else ClearCF;
	if (((0 - (oldValue & 0x0f)) & 0x10)) SetHC; else ClearHC;
	return 8;
}

OPCODE(NOP)
{
	return 4;
}

OPCODE(OR)
{
	unsigned char cycles = 4;
	unsigned char value;
	switch (op)
	{
	case 0xb0:
		value = B();
		break;
	case 0xb1:
		value = C();
		break;
	case 0xb2:
		value = D();
		break;
	case 0xb3:
		value = E();
		break;
	case 0xb4:
		value = H();
		break;
	case 0xb5:
		value = L();
		break;
	case 0xb6:
		cycles = 19;
		unsigned short address;
		char d;
		switch (op2)
		{
		case 0:
			address = Regs.HL;
			cycles = 7;
			break;
		case 0xdd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IX + d;
			break;
		case 0xfd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IY + d;
			break;
		}
		value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, value);
	case 0xb7:
		value = A();
		break;
	case 0xf6:
		cycles = 7;
		value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		break;
	default:
		return 0; // Just in case! But this should never happen!
	}
	value |= A();
	SetA(value);
	ClearNF;
	ClearCF;
	ClearHC;
	if (value == 0) SetZF; else ClearZF;
	if (value & 0x80) SetSF; else ClearSF;
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	return cycles;
}

OPCODE(OTDR)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	SystemBus::Instance()->IOBus()->WriteAddress(C(), value);
	SetB(B() - 1);
	Regs.HL--;
	SetNF;
	if (B() == 0)
	{
		SetZF;
		SetPV;
		return 16;
	}
	else
	{
		ClearZF;
		ClearPV;
		_PC -= 2; //Repeat the instruction
		return 21;
	}
}

OPCODE(OTIR)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	SystemBus::Instance()->IOBus()->WriteAddress(C(), value);
	SetB(B() - 1);
	Regs.HL++;
	SetNF;
	if (B() == 0)
	{
		SetZF;
		return 16;
	}
	else
	{
		ClearZF;
		_PC -= 2;
		return 21;
	}
}

OPCODE(OUTD)
{
	//TODO: Check this against Undocumented docs
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	SystemBus::Instance()->IOBus()->WriteAddress(C(), value);
	SetB(B() - 1);
	Regs.HL--;
	SetNF;
	if (B() == 0) SetZF; else ClearZF;
	return 16;
}

OPCODE(OUTI)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	SystemBus::Instance()->IOBus()->WriteAddress(C(), value);
	SetB(B() - 1);
	Regs.HL++;
	SetNF;
	if (B() == 0) SetZF; else ClearZF;
	return 16;
}

OPCODE(OUTOP)
{
	unsigned char cycles = 12;
	unsigned char value;
	unsigned char port;
	if (op == 0xd3)
	{
		port = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		value = A();
		cycles = 11;
	}
	else if (op2 == 0xed)
	{
		port = C();
		switch (op)
		{
		case 0x79:
			value = A();
			break;
		case 0x41:
			value = B();
			break;
		case 0x49:
			value = C();
			break;
		case 0x51:
			value = D();
			break;
		case 0x59:
			value = E();
			break;
		case 0x61:
			value = H();
			break;
		case 0x69:
			value = L();
			break;
		default:
			return 0; //Should NEVER happen!
		}
	}
	else
	{
		return 0; //Should NEVER happen!
	}
	SystemBus::Instance()->IOBus()->WriteAddress(port, value);
	return cycles;
}

OPCODE(POP)
{
	unsigned char cycles = 10;
	switch (op)
	{
	case 0xc1:
		SetC(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		SetB(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		break;
	case 0xd1:
		SetE(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		SetD(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		break;
	case 0xf1:
		SetF(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		SetA(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
		break;
	case 0xe1:
		switch (op2)
		{
		case 0:
			SetL(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
			SetH(SystemBus::Instance()->AddressBus()->ReadAddress(_sp++));
			break;
		case 0xdd:
			cycles = 14;
			_IX = SystemBus::Instance()->AddressBus()->ReadAddress(_sp++);
			_IX |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_sp++) << 8);
			break;
		case 0xfd:
			cycles = 14;
			_IY = SystemBus::Instance()->AddressBus()->ReadAddress(_sp++);
			_IY |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_sp++) << 8);
			break;
		}
		break;
	}
	return cycles;
}

OPCODE(PUSH)
{
	unsigned cycles = 10;
	switch (op)
	{
	case 0xc5:
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, B());
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, C());
		break;
	case 0xd5:
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, D());
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, E());
		break;
	case 0xf5:
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, A());
		SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, F());
		break;
	case 0xe5:
		switch (op2)
		{
		case 0:
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, H());
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, L());
			break;
		case 0xdd:
			cycles = 14;
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_IX >> 8));
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_IX & 0xff));
			break;
		case 0xfd:
			cycles = 14;
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_IY >> 8));
			SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_IY & 0xff));
			break;
		}
		break;
	default: //Should NEVER happen!
		return 0;
	}
	return cycles;
}

OPCODE(RES)
{
	unsigned char mask = (op >> 4) - 8;
	mask *= 2;
	if (op & 0x08)
		mask++;
	mask = 1 << mask;
	mask = ~mask;
	unsigned char value = 0;
	int cycles = 8;
	switch (op & 0x0f)
	{
	case 0:
	case 8:
		value = B() & mask;
		SetB(value);
		break;
	case 1:
	case 9:
		value = C() & mask;
		SetC(value);
		break;
	case 2:
	case 0xa:
		value = D() & mask;
		SetD(value);
		break;
	case 3:
	case 0xb:
		value = E() & mask;
		SetE(value);
		break;
	case 4:
	case 0xc:
		value = H() & mask;
		SetH(value);
		break;
	case 5:
	case 0xd:
		value = L() & mask;
		SetL(value);
		break;
	case 6:
	case 0xe:
	{
		cycles = 20;
		unsigned short address = 0;
		unsigned char d;
		switch (op2)
		{
		case 0x0:
			address = Regs.HL;
			cycles = 12;
			break;
		case 0xdd:
			address = _IX + displacement;
			break;
		case 0xfe:
			address = _IY + displacement;
			break;
		}
		d = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, d);
		value = d & mask;
		SystemBus::Instance()->AddressBus()->WriteAddress(address, value);
		UpdateMemAccessed(address, value);
	}
	break;
	case 7:
	case 0xf:
		value = A() & mask;
		SetA(value);
		break;
	}
	if (value) SetZF; else ClearZF;
	ClearNF;
	SetHC;
	return cycles;
}

OPCODE(RET)
{
	unsigned char cycles = 5;
	bool doRET = false;
	switch (op)
	{
	case 0xc0:
		if (ZFIsClear)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xc8:
		if (ZFIsSet)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xc9:
		doRET = true;
		cycles = 10;
	case 0xd0:
		if (CFIsClear)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xd8:
		if (CFIsSet)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xe0:
		if (PVIsClear)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xe8:
		if (PVIsSet)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xf0:
		if (SFIsClear)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	case 0xf8:
		if (SFIsSet)
		{
			cycles = 11;
			doRET = true;
		}
		break;
	default:	//Should NEVER happen!
		cycles = 0;
	}
	if (doRET)
	{
		unsigned short newPC = SystemBus::Instance()->AddressBus()->ReadAddress(_sp++);
		newPC |= ((unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_sp++) << 8);
		_PC = newPC;
	}
	return cycles;
}

OPCODE(RETI)
{
	_PC = SystemBus::Instance()->AddressBus()->ReadAddress(_sp++);
	_PC |= (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_sp++) << 8;
	//Needs to notify the bus that it is done
	return 14;
}

OPCODE(RETN)
{
	_PC = SystemBus::Instance()->AddressBus()->ReadAddress(_sp++);
	_PC |= (unsigned short)SystemBus::Instance()->AddressBus()->ReadAddress(_sp++) << 8;
	IFF1 = IFF2;
	return 14;
}

OPCODE(RL)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x10:
		value = B();
		break;
	case 0x11:
		value = C();
		break;
	case 0x12:
		value = D();
		break;
	case 0x13:
		value = E();
		break;
	case 0x14:
		value = H();
		break;
	case 0x15:
		value = L();
		break;
	case 0x16:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x17:
		value = A();
		break;
	}
	//Now, do the math
	bool CFWasSet = CFIsSet;
	if (value & 0x80) SetCF; else ClearCF;
	value <<= 1;
	if (CFWasSet) value |= 1;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x10:
		SetB(value);
		break;
	case 0x11:
		SetC(value);
		break;
	case 0x12:
		SetD(value);
		break;
	case 0x13:
		SetE(value);
		break;
	case 0x14:
		SetH(value);
		break;
	case 0x15:
		SetL(value);
		break;
	case 0x16:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x17:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(RLA)
{
	ClearHC;
	ClearNF;
	bool carry = CFIsSet;
	unsigned char value = A();
	if (value & 0x80) SetCF; else ClearCF;
	value <<= 1;
	if (carry) value |= 1;
	SetA(value);
	return 4;
}

OPCODE(RLC)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x00:
		value = B();
		break;
	case 0x01:
		value = C();
		break;
	case 0x02:
		value = D();
		break;
	case 0x03:
		value = E();
		break;
	case 0x04:
		value = H();
		break;
	case 0x05:
		value = L();
		break;
	case 0x06:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x07:
		value = A();
		break;
	}
	//Now, do the math
	if (value & 0x80) SetCF; else ClearCF;
	value <<= 1;
	if (CFIsSet) value |= 1;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x00:
		SetB(value);
		break;
	case 0x01:
		SetC(value);
		break;
	case 0x02:
		SetD(value);
		break;
	case 0x03:
		SetE(value);
		break;
	case 0x04:
		SetH(value);
		break;
	case 0x05:
		SetL(value);
		break;
	case 0x06:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x07:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(RLCA)
{
	unsigned short value = A();
	value <<= 1;
	if (value & (1 << 8))
	{
		value |= 1;
		SetCF;
	}
	else
		ClearCF;
	ClearNF;
	ClearHC;
	return 4;
}

OPCODE(RLD)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	unsigned char newValue = value << 4 || (A() & 0x0f);
	SetA((A() & 0xf0) || (value >> 4));
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, newValue);
	value = A();

	if (value && 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value && (1 << i)) count++;
	if (count % 2 == 0) SetPV; else ClearPV;
	return 18;
}

OPCODE(RR)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x18:
		value = B();
		break;
	case 0x19:
		value = C();
		break;
	case 0x1a:
		value = D();
		break;
	case 0x1b:
		value = E();
		break;
	case 0x1c:
		value = H();
		break;
	case 0x1d:
		value = L();
		break;
	case 0x1e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x1f:
		value = A();
		break;
	}
	//Now, do the math
	bool CFWasSet = CFIsSet;
	if (value & 0x01) SetCF; else ClearCF;
	value >>= 1;
	if (CFWasSet) value |= 0x80;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x10:
		SetB(value);
		break;
	case 0x11:
		SetC(value);
		break;
	case 0x12:
		SetD(value);
		break;
	case 0x13:
		SetE(value);
		break;
	case 0x14:
		SetH(value);
		break;
	case 0x15:
		SetL(value);
		break;
	case 0x16:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x17:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(RRA)
{
	unsigned char value = A();
	bool setCarry = (value & 0x01) == 1;
	ClearHC;
	ClearNF;
	value >>= 1;
	if (CFIsSet) value |= 0x80;
	if (setCarry) SetCF; else ClearCF;
	SetA(value);
	return 4;
}

OPCODE(RRC)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x08:
		value = B();
		break;
	case 0x09:
		value = C();
		break;
	case 0x0a:
		value = D();
		break;
	case 0x0b:
		value = E();
		break;
	case 0x0c:
		value = H();
		break;
	case 0x0d:
		value = L();
		break;
	case 0x0e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x0f:
		value = A();
		break;
	}
	//Now, do the math
	if (value & 0x01) SetCF; else ClearCF;
	value >>= 1;
	if (CFIsSet) value |= 0x80;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x00:
		SetB(value);
		break;
	case 0x01:
		SetC(value);
		break;
	case 0x02:
		SetD(value);
		break;
	case 0x03:
		SetE(value);
		break;
	case 0x04:
		SetH(value);
		break;
	case 0x05:
		SetL(value);
		break;
	case 0x06:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x07:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(RRCA)
{
	unsigned char value = A();
	if (value & 0x01) SetCF; else ClearCF;
	value >>= 1;
	if (CFIsSet) value |= 0x80;
	SetA(value);
	return 4;
}

OPCODE(RRD)
{
	unsigned char value = SystemBus::Instance()->AddressBus()->ReadAddress(Regs.HL);
	unsigned char newValue = value >> 4 || ((A() & 0x0f) << 4);
	SetA((A() & 0xf0) || (value & 0x0f));
	SystemBus::Instance()->AddressBus()->WriteAddress(Regs.HL, newValue);
	value = A();
	if (value && 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value && (1 << i)) count++;
	if (count % 2 == 0) SetPV; else ClearPV;
	return 18;
}

OPCODE(RST)
{
	//Push the return address
	SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_PC >> 8));
	SystemBus::Instance()->AddressBus()->WriteAddress(--_sp, (unsigned char)(_PC & 0xff));
	unsigned char vector = ((op >> 3) & 0x07) << 3;
	_PC = (unsigned short)vector;
	return 11;
}

OPCODE(SBC)
{
	unsigned char cycles;
	char value;
	short lvalue;
	if (((op & 0x90) == 0x90) || op == 0xde)
	{
		cycles = 4;
		switch (op)
		{
		case 0x98:
			value = (char)B();
			break;
		case 0x99:
			value = (char)C();
			break;
		case 0x9a:
			value = (char)D();
			break;
		case 0x9b:
			value = (char)E();
			break;
		case 0x9c:
			value = (char)H();
			break;
		case 0x9d:
			value = (char)L();
			break;
		case 0x9e:
			unsigned short address;
			char d;
			switch (op2)
			{
			case 0:
				address = Regs.HL;
				cycles = 7;
				break;
			case 0xdd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IX + d;
				cycles = 19;
				break;
			case 0xfd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IY + d;
				cycles = 19;
				break;
			}
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(address);
			UpdateMemAccessed(address, (unsigned char)value);
			break;
		case 0x9f:
			value = (char)A();
			break;
		case 0xde:
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			cycles = 7;
			break;
		}
		lvalue = (short)(unsigned char)A();
		unsigned char hcCheck = A() & 0x0f;
		lvalue -= (value + (CFIsSet ? 1 : 0));
		SetA((lvalue & 0xff));
		SetNF;
		if ((lvalue & 0x100)) SetCF; else ClearCF;
		if (lvalue > 127 || lvalue < -128) SetPV; else ClearPV;
		if (A() == 0) SetZF; else ClearZF;
		if ((A() & 0x80)) SetSF; else ClearSF;
		if ((hcCheck - ((value & 0x0f) + (CFIsSet ? 1 : 0))) & 0x10) SetHC; else ClearHC;
	}
	else if (op == 0x42 || op == 0x52 || op == 0x62 || op == 0x72)
	{
		int llvalue;
		switch (op)
		{
		case 0x42:
			lvalue = (short)Regs.BC;
			break;
		case 0x52:
			lvalue = (short)Regs.DE;
			break;
		case 0x62:
			if (op2 == 0xed) lvalue = (short)Regs.HL;
			else if (op2 == 0xdd) lvalue = (short)_IX;
			else if (op2 == 0xfd) lvalue = (short)_IY;
			else return 0;
			break;
		case 0x72:
			lvalue = (short)_sp;
			break;
		}
		switch (op2)
		{
		case 0:
		case 0xed:
			llvalue = (int)(short)Regs.HL;
			cycles = 11;
			break;
		case 0xdd:
			llvalue = (int)(short)_IX;
			cycles = 15;
			break;
		case 0xfd:
			llvalue = (int)(short)_IY;
			cycles = 15;
			break;
		}
		int oldll = llvalue;
		llvalue -= (lvalue + (CFIsSet ? 1 : 0));
		if (llvalue & 0xffff0000) SetCF; else ClearCF;
		llvalue &= 0xffff;
		ClearNF;
		//Check for the half carry
		oldll &= 0x0fff;
		oldll -= ((lvalue & 0xfff) + (CFIsSet ? 1 : 0));
		if (oldll & 0x1000) SetHC; else ClearHC;
		switch (op2)
		{
		case 0xed:
			Regs.HL = (unsigned short)llvalue;
			break;
		case 0xdd:
			_IX = (unsigned short)llvalue;
			break;
		case 0xfd:
			_IY = (unsigned short)llvalue;
			break;
		}
	}
	return cycles;
}

OPCODE(SCF)
{
	SetCF;
	return 4;
}

OPCODE(SET)
{
	unsigned char mask = (op >> 4) - 0x0c;
	mask *= 2;
	if (op & 0x08)
		mask++;
	mask = 1 << mask;
	unsigned char value = 0;
	int cycles = 8;
	switch (op & 0x0f)
	{
	case 0:
	case 8:
		value = B() | mask;
		SetB(value);
		break;
	case 1:
	case 9:
		value = C() | mask;
		SetC(value);
		break;
	case 2:
	case 0xa:
		value = D() | mask;
		SetD(value);
		break;
	case 3:
	case 0xb:
		value = E() | mask;
		SetE(value);
		break;
	case 4:
	case 0xc:
		value = H() | mask;
		SetH(value);
		break;
	case 5:
	case 0xd:
		value = L() | mask;
		SetL(value);
		break;
	case 6:
	case 0xe:
	{
		cycles = 20;
		unsigned short address = 0;
		unsigned char d;
		switch (op2)
		{
		case 0x0:
			address = Regs.HL;
			cycles = 12;
			break;
		case 0xdd:
			address = _IX + displacement;
			break;
		case 0xfe:
			address = _IY + displacement;
			break;
		}
		d = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, d);
		value = d | mask;
		SystemBus::Instance()->AddressBus()->WriteAddress(address, value);
		UpdateMemAccessed(address, value);
	}
	break;
	case 7:
	case 0xf:
		value = A() | mask;
		SetA(value);
		break;
	}
	if (value) SetZF; else ClearZF;
	ClearNF;
	SetHC;
	return cycles;
}

OPCODE(SLA)
{
	unsigned char value;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x20:
		value = B();
		break;
	case 0x21:
		value = C();
		break;
	case 0x22:
		value = D();
		break;
	case 0x23:
		value = E();
		break;
	case 0x24:
		value = H();
		break;
	case 0x25:
		value = L();
		break;
	case 0x26:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x27:
		value = A();
		break;
	}
	//Now, do the math
	if (value & 0x80) SetCF; else ClearCF;
	value <<= 1;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	if (value & 0x01) ClearPV; else SetPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x20:
		SetB(value);
		break;
	case 0x21:
		SetC(value);
		break;
	case 0x22:
		SetD(value);
		break;
	case 0x23:
		SetE(value);
		break;
	case 0x24:
		SetH(value);
		break;
	case 0x25:
		SetL(value);
		break;
	case 0x26:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x27:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(SLL)
{
	unsigned char value;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x30:
		value = B();
		break;
	case 0x31:
		value = C();
		break;
	case 0x32:
		value = D();
		break;
	case 0x33:
		value = E();
		break;
	case 0x34:
		value = H();
		break;
	case 0x35:
		value = L();
		break;
	case 0x36:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x37:
		value = A();
		break;
	}
	//Now, do the math
	if (value & 0x80) SetCF; else ClearCF;
	value <<= 1;
	value |= 0x01;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	if (value & 0x01) ClearPV; else SetPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x30:
		SetB(value);
		break;
	case 0x31:
		SetC(value);
		break;
	case 0x32:
		SetD(value);
		break;
	case 0x33:
		SetE(value);
		break;
	case 0x34:
		SetH(value);
		break;
	case 0x35:
		SetL(value);
		break;
	case 0x36:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x37:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(SRA)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x28:
		value = B();
		break;
	case 0x29:
		value = C();
		break;
	case 0x2a:
		value = D();
		break;
	case 0x2b:
		value = E();
		break;
	case 0x2c:
		value = H();
		break;
	case 0x2d:
		value = L();
		break;
	case 0x2e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x2f:
		value = A();
		break;
	default: //TODO: Error reporting
		return 0;
	}
	//Now, do the math
	if (value & 0x01) SetCF; else ClearCF;
	bool setIt = (value & 0x80); //Preserve d7
	value >>= 1;
	if (setIt) value |= 0x80;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	unsigned count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x28:
		SetB(value);
		break;
	case 0x29:
		SetC(value);
		break;
	case 0x2a:
		SetD(value);
		break;
	case 0x2b:
		SetE(value);
		break;
	case 0x2c:
		SetH(value);
		break;
	case 0x2d:
		SetL(value);
		break;
	case 0x2e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x2f:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(SRL)
{
	unsigned char value = 0;
	unsigned char cycles = 8;
	switch (op)
	{
	case 0x38:
		value = B();
		break;
	case 0x39:
		value = C();
		break;
	case 0x3a:
		value = D();
		break;
	case 0x3b:
		value = E();
		break;
	case 0x3c:
		value = H();
		break;
	case 0x3d:
		value = L();
		break;
	case 0x3e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			value = SystemBus::Instance()->AddressBus()->ReadAddress(HL());
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IX() + displacement);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			value = SystemBus::Instance()->AddressBus()->ReadAddress(IY() + displacement);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x3f:
		value = A();
		break;
	default: //TODO: Error reporting
		return 0;
	}
	//Now, do the math
	if (value & 0x01) SetCF; else ClearCF;
	value >>= 1;
	if (value & 0x80) SetSF; else ClearSF;
	if (value == 0) SetZF; else ClearZF;
	ClearHC;
	ClearNF;
	if (value & 0x01) ClearPV; else SetPV;
	//And then put it back where it belongs
	switch (op)
	{
	case 0x38:
		SetB(value);
		break;
	case 0x39:
		SetC(value);
		break;
	case 0x3a:
		SetD(value);
		break;
	case 0x3b:
		SetE(value);
		break;
	case 0x3c:
		SetH(value);
		break;
	case 0x3d:
		SetL(value);
		break;
	case 0x3e:
		cycles = 23;
		switch (op2)
		{
		case 0x00:
			cycles = 15;
			SystemBus::Instance()->AddressBus()->WriteAddress(HL(), value);
			UpdateMemAccessed(HL(), value);
			break;
		case 0xdd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IX() + displacement, value);
			UpdateMemAccessed(IX() + displacement, value);
			break;
		case 0xfd:
			SystemBus::Instance()->AddressBus()->WriteAddress(IY() + displacement, value);
			UpdateMemAccessed(IY() + displacement, value);
			break;
		}
		break;
	case 0x3f:
		SetA(value);
		break;
	}
	return cycles;
}

OPCODE(SUB)
{
	unsigned char cycles = 0;
	char value;
	short lvalue;
	if (((op & 0x90) == 0x90) || op == 0xD6)
	{
		cycles = 4;
		switch (op)
		{
		case 0x90:
			value = (char)B();
			break;
		case 0x91:
			value = (char)C();
			break;
		case 0x92:
			value = (char)D();
			break;
		case 0x93:
			value = (char)E();
			break;
		case 0x94:
			value = (char)H();
			break;
		case 0x95:
			value = (char)L();
			break;
		case 0x96:
			unsigned short address;
			char d;
			switch (op2)
			{
			case 0:
				address = Regs.HL;
				cycles = 7;
				break;
			case 0xdd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IX + d;
				cycles = 19;
				break;
			case 0xfd:
				d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
				address = _IY + d;
				cycles = 19;
				break;
			}
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(address);
			UpdateMemAccessed(address, (unsigned char)value);
			break;
		case 0x97:
			value = (char)A();
			break;
		case 0xd6:
			value = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			cycles = 7;
			break;
		}
		lvalue = (short)(unsigned char)A();
		unsigned char hcCheck = A() & 0x0f;
		lvalue -= value;
		SetA((lvalue & 0xff));
		SetNF;
		if ((unsigned short)lvalue > 255) SetCF; else ClearCF;
		if (lvalue > 127 || lvalue < -128) SetPV; else ClearPV;
		if (A() == 0) SetZF; else ClearZF;
		if ((A() & 0x80)) SetSF; else ClearSF;
		if ((hcCheck - (value & 0x0f)) & 0x10) SetHC; else ClearHC;
	}
	return cycles;
}

OPCODE(XOR)
{
	unsigned char cycles = 4;
	unsigned char value;
	switch (op)
	{
	case 0xa8:
		value = B();
		break;
	case 0xa9:
		value = C();
		break;
	case 0xaa:
		value = D();
		break;
	case 0xab:
		value = E();
		break;
	case 0xac:
		value = H();
		break;
	case 0xad:
		value = L();
		break;
	case 0xae:
		cycles = 19;
		unsigned short address;
		char d;
		switch (op2)
		{
		case 0:
			address = Regs.HL;
			cycles = 7;
			break;
		case 0xdd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IX + d;
			break;
		case 0xfd:
			d = (char)SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
			address = _IY + d;
			break;
		}
		value = SystemBus::Instance()->AddressBus()->ReadAddress(address);
		UpdateMemAccessed(address, value);
	case 0xaf:
		value = A();
		break;
	case 0xee:
		cycles = 7;
		value = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		break;
	default:
		return 0; // Just in case! But this should never happen!
	}
	value ^= A();
	SetA(value);
	ClearNF;
	ClearCF;
	ClearHC;
	if (value == 0) SetCF; else ClearCF;
	if (value & 0x80) SetSF; else ClearSF;
	unsigned char count = 0;
	for (int i = 0; i < 8; ++i)
		if (value & (1 << i))
			count++;
	if (count % 2 == 0)	SetPV; else ClearPV;
	return cycles;
}

OPCODE(ExtCB)
{
	op = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	if (op2 == 0xdd || op2 == 0xfd)
	{
		displacement = op;
		op = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	}
	if (CBHandlers[op] != nullptr)
		return std::invoke(CBHandlers[op], this, op, op2);
	else
		return 0; //There is no opcode handler, so return 0 as an illegal opcode
}

OPCODE(ExtIR)
{
	unsigned char nextOP = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	if (nextOP == 0xdd || nextOP == 0xed || nextOP == 0xfd)
	{
		//Ignore this one and take care of the next one!
		if (opcodeHandlers[nextOP] != nullptr)
			return 4 + std::invoke(opcodeHandlers[nextOP], this, nextOP, 0);
		else
		{
			_PC--; //Back the PC up so this will get taken care of next instruction...
			return 4; //There is no opcode handler, so return 0 as an illegal opcode 
		}
	}
	if (nextOP != 0xcb)
	{
		if (opcodeHandlers[nextOP] != nullptr)
			return std::invoke(opcodeHandlers[nextOP], this, nextOP, op);
		else
		{
			_PC--; //Back the PC up so this will get taken care of next instruction...
			return 4; //There is no opcode handler, so return 0 as an illegal opcode 
		}
	}
	else
	{
		//This is a FD CD or DD CB instruction
		nextOP = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
		if (CBHandlers[nextOP] != nullptr)
			return std::invoke(CBHandlers[nextOP], this, nextOP, op);
		else
			return 0;
	}
}

OPCODE(ExtED)
{
	unsigned char nextOP = SystemBus::Instance()->AddressBus()->ReadAddress(_PC++);
	if (opcodeHandlers[nextOP] != nullptr)
		return std::invoke(opcodeHandlers[nextOP], this, nextOP, op);
	return 8;
}

OPCODE(JAM)
{
	halted = true;
	return 0; //Something went WRONG!
}

#undef OPCODE

//Here are the pointers to the opcode handlers

Z80::OpcodeHandler Z80::opcodeHandlers[256] =
{
	//0x00   0x01  0x02  0x03  0x04   0x05   0x06   0x07   0x08  0x09  0x0a  0x0b    0x0c   0x0d    0x0e  0x0f
	  &NOP,  &LD,  &LD,  &INC, &INC,  &DEC,  &LD,   &RLCA, &EX,  &ADD, &LD,  &DEC,   &INC,  &DEC,   &LD,  &RRCA,	//0X00
	  &DJNZ, &LD,  &LD,  &INC, &INC,  &DEC,  &LD,   &RLA,  &JR,  &ADD, &LD,  &DEC,   &INC,  &DEC,   &LD,  &RRA,	//0X10
	  &JR,   &LD,  &LD,  &INC, &INC,  &DEC,  &LD,   &DAA,  &JR,  &ADD, &LD,  &DEC,   &INC,  &DEC,   &LD,  &CPL,	//0X20
	  &JR,   &LD,  &LD,  &INC, &INC,  &DEC,  &LD,   &SCF,  &JR,  &ADD, &LD,  &DEC,   &INC,  &DEC,   &LD,  &CCF,	//0X30
	  &LD,   &LD,  &LD,  &LD,  &LD,   &LD,   &LD,   &LD,   &LD,  &LD,  &LD,  &LD,    &LD,   &LD,    &LD,  &LD,	//0X40
	  &LD,   &LD,  &LD,  &LD,  &LD,   &LD,   &LD,   &LD,   &LD,  &LD,  &LD,  &LD,    &LD,   &LD,    &LD,  &LD,	//0X50
	  &LD,   &LD,  &LD,  &LD,  &LD,   &LD,   &LD,   &LD,   &LD,  &LD,  &LD,  &LD,    &LD,   &LD,    &LD,  &LD,	//0X60
	  &LD,   &LD,  &LD,  &LD,  &LD,   &LD,   &HALT, &LD,   &LD,  &LD,  &LD,  &LD,    &LD,   &LD,    &LD,  &LD,	//0X70
	  &ADD,  &ADD, &ADD, &ADD, &ADD,  &ADD,  &ADD,  &ADD,  &ADC, &ADC, &ADC, &ADC,   &ADC,  &ADC,   &ADC, &ADC,	//0X80
	  &SUB,  &SUB, &SUB, &SUB, &SUB,  &SUB,  &SUB,  &SUB,  &SBC, &SBC, &SBC, &SBC,   &SBC,  &SBC,   &SBC, &SBC,	//0X90
	  &AND,  &AND, &AND, &AND, &AND,  &AND,  &AND,  &AND,  &XOR, &XOR, &XOR, &XOR,   &XOR,  &XOR,   &XOR, &XOR,	//0XA0
	  &OR,   &OR,  &OR,  &OR,  &OR,   &OR,   &OR,   &OR,   &CP,  &CP,  &CP,  &CP,    &CP,   &CP,    &CP,  &CP,	//0XB0
	  &RET,  &POP, &JP,  &JP,  &CALL, &PUSH, &ADD,  &RST,  &RET, &RET, &JP,  &ExtCB, &CALL, &CALL,  &ADC, &RST,	//0XC0
	  &RET,  &POP, &JP, &OUTOP,&CALL, &PUSH, &SUB,  &RST,  &RET, &EXX, &JP,  &INOP,  &CALL, &ExtIR, &SBC, &RST,	//0XD0
	  &RET,  &POP, &JP,  &EX,  &CALL, &PUSH, &AND,  &RST,  &RET, &JP,  &JP,  &EX,    &CALL, &ExtED, &XOR, &RST,	//0XE0
	  &RET,  &POP, &JP,  &DI,  &CALL, &PUSH, &OR,   &RST,  &RET, &LD,  &JP,  &EI,    &CALL, &ExtIR, &CP,  &RST	//0XF0
};

Z80::OpcodeHandler Z80::CBHandlers[256] =
{
	//0x00   0x01  0x02  0x03  0x04   0x05   0x06   0x07   0x08  0x09  0x0a  0x0b    0x0c   0x0d    0x0e  0x0f
	  &RLC,  &RLC, &RLC, &RLC, &RLC,  &RLC,  &RLC,  &RLC,  &RRC, &RRC, &RRC, &RRC,   &RRC,  &RRC,   &RRC, &RRC, //0X00
	  &RL,   &RL,  &RL,  &RL,  &RL,   &RL,   &RL,   &RL,   &RR,  &RR,  &RR,  &RR,    &RR,   &RR,    &RR,  &RR, //0X10
	  &SLA,  &SLA, &SLA, &SLA, &SLA,  &SLA,  &SLA,  &SLA,  &SRA, &SRA, &SRA, &SRA,   &SRA,  &SRA,   &SRA, &SRA, //0X20
	  &SLL,  &SLL, &SLL, &SLL, &SLL,  &SLL,  &SLL,  &SLL,  &SRL, &SRL, &SRL, &SRL,   &SRL,  &SRL,   &SRL, &SRL, //0X30
	  &BIT,  &BIT, &BIT, &BIT, &BIT,  &BIT,  &BIT,  &BIT,  &BIT, &BIT, &BIT, &BIT,   &BIT,  &BIT,   &BIT, &BIT, //0X40
	  &BIT,  &BIT, &BIT, &BIT, &BIT,  &BIT,  &BIT,  &BIT,  &BIT, &BIT, &BIT, &BIT,   &BIT,  &BIT,   &BIT, &BIT, //0X50
	  &BIT,  &BIT, &BIT, &BIT, &BIT,  &BIT,  &BIT,  &BIT,  &BIT, &BIT, &BIT, &BIT,   &BIT,  &BIT,   &BIT, &BIT, //0X60
	  &BIT,  &BIT, &BIT, &BIT, &BIT,  &BIT,  &BIT,  &BIT,  &BIT, &BIT, &BIT, &BIT,   &BIT,  &BIT,   &BIT, &BIT, //0X70
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X80
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X90
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XA0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XB0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XC0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XD0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XF0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM  //0XE0
};

Z80::OpcodeHandler Z80::EDHandlers[256] =
{
	//0x00   0x01  0x02  0x03  0x04   0x05   0x06   0x07   0x08  0x09  0x0a  0x0b    0x0c   0x0d    0x0e  0x0f
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X00
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X10
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X20
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X30
	  &INOP,&OUTOP,&SBC,  &LD, &NEG, &RETN,   &IM,   &LD, &INOP,&OUTOP,&ADC,  &LD,   &NEG, &RETI,   &JAM,  &LD, //0X40
	  &INOP,&OUTOP,&SBC,  &LD, &NEG, &RETN,   &IM,   &LD, &INOP,&OUTOP,&ADC,  &LD,   &NEG, &RETN,    &IM,  &LD, //0X50
	  &INOP,&OUTOP,&SBC,  &LD, &NEG, &RETN,  &JAM,  &RRD, &INOP,&OUTOP,&ADC,  &LD,   &NEG, &RETN,   &JAM, &RLD, //0X60
	  &INOP,&OUTOP,&SBC,  &LD, &NEG, &RETN,  &JAM,  &JAM, &INOP,&OUTOP,&ADC,  &LD,   &NEG, &RETI,   &JAM, &JAM, //0X70
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X80
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0X90
	  &LDI,  &CPI, &INI,&OUTI, &JAM,  &JAM,  &JAM,  &JAM,  &LDD, &CPD, &IND,&OUTD,   &JAM,  &JAM,   &JAM, &JAM, //0XA0
	 &LDIR, &CPIR,&INIR,&OTIR, &JAM,  &JAM,  &JAM,  &JAM, &LDDR,&CPDR,&INDR,&OTDR,   &JAM,  &JAM,   &JAM, &JAM, //0XB0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XC0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XD0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM, //0XF0
	  &JAM,  &JAM, &JAM, &JAM, &JAM,  &JAM,  &JAM,  &JAM,  &JAM, &JAM, &JAM, &JAM,   &JAM,  &JAM,   &JAM, &JAM  //0XE0
};
