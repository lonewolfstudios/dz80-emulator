#include "GUIWindow.h"

class GUIDebuggerWindow : public GUIWindow
{

private:

	unsigned char lastCycles = 0;

public:

	GUIDebuggerWindow(char* name, bool visible = false) : GUIWindow(name, visible) {};
	virtual bool Draw() override;

};
