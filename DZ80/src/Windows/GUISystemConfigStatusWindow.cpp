#include "GUISystemConfigStatusWindow.h"
#include "Devices/SystemConfig.h"

#define LABEL_WIDTH		50

bool GUISystemConfigStatusWindow::Draw()
{
	if (!visible)
		return false;
	
	ImGui::Begin("SystemConfig", &visible, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::SetNextItemWidth(LABEL_WIDTH);
	ImGui::Text("Bank Size: ");
	ImGui::SameLine();	
	switch (SystemConfig::GetBankSize())
	{
	case SystemConfig::BankSize::Sixteen:
		if (ImGui::Button("16 KB"))
			SystemConfig::ToggleBankSize();
		break;
	default:
		if (ImGui::Button("32 KB"))
			SystemConfig::ToggleBankSize();
		break;
	}
	for (int i = 0; i < 4; ++i)
	{
		ImGui::PushID(i);
		ImGui::SetNextItemWidth(LABEL_WIDTH);
		ImGui::Text("Switch %d:", i);
		ImGui::SameLine();
		bool value = SystemConfig::GetSwitch(i);
		ImGui::Checkbox("", &value);
		if (value != SystemConfig::GetSwitch(i))
			SystemConfig::ToggleSwitch(i);
		ImGui::PopID();
	}
	ImGui::End();
	return false;
}