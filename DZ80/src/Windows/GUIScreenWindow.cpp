#include "GUIScreenWindow.h"
#include "nfd.h"
#include <glad/glad.h>
#include "SystemBus/SystemBus.h"
#include "Input/InputManager.h"

typedef unsigned int COLOR;

void GUIScreenWindow::Initialize()
{
	glGenTextures(1, &screenTextureID);
	glBindTexture(GL_TEXTURE_2D, screenTextureID);
}

bool GUIScreenWindow::Draw()
{
	if (!visible)
		return false;
	UpdateScreenTexture();
	ImGui::Begin("DZ80 Screen", &visible, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_MenuBar);
	if (ImGui::BeginMenuBar())
	{
		if (FileMenu())
			return true;
		SettingsMenu();
		ImGui::EndMenuBar();
	}
	ImGui::Image((void*)(intptr_t)screenTextureID, ImVec2((float)(screenWidth * scale), (float)(screenHeight * scale)));
	ImGui::End();
	return false;
}

void GUIScreenWindow::SettingsMenu()
{
	if (ImGui::BeginMenu("Settings##DZ80"))
	{
		if (ImGui::BeginMenu("Scale"))
		{
			if (ImGui::MenuItem("1x"))
				SetScale(1);
			else if (ImGui::MenuItem("2x"))
				SetScale(2);
			else if (ImGui::MenuItem("3x"))
				SetScale(3);
			else if (ImGui::MenuItem("4x"))
				SetScale(4);
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Input"))
		{
			if (ImGui::BeginMenu("Joystick"))
			{
				InputManager::Instance()->OnImGUIJoystickMenu();
				ImGui::EndMenu();
			}
			ImGui::EndMenu();
		}
		ImGui::EndMenu();
	}
}

bool GUIScreenWindow::FileMenu()
{
	if (ImGui::BeginMenu("File##DZ80"))
	{
		if (ImGui::BeginMenu("Cartridge"))
		{
			if (ImGui::MenuItem("Load CRT"))
			{
				nfdchar_t* outPath = nullptr;
				nfdresult_t result = NFD_OpenDialog("crt", NULL, &outPath);
				if (result == NFD_OKAY)
				{
				}
			}
			if (ImGui::MenuItem("Remove CRT"))
			{
			}
			ImGui::EndMenu();
		}		
		if (ImGui::MenuItem("Quit"))
			return true;
		ImGui::EndMenu();
		return false;
	}
	return false;
}

void GUIScreenWindow::UpdateScreenTexture()
{
	GLenum err;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, screenTextureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	if ((err = glGetError()) != GL_NO_ERROR)
	{
		{
			screenTextureID = screenTextureID;
		}
	}
	const COLOR* screenData;
	return;
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenWidth, screenHeight, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, (void*)screenData);
	if ((err = glGetError()) != GL_NO_ERROR)
	{
		{
			screenTextureID = screenTextureID;
		}
	}
}
