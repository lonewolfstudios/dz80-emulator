#include "GUIWindow.h"

class GUIProcessorStatusWindow : public GUIWindow
{

private:
	   
public:

	GUIProcessorStatusWindow(char* name, bool visible = false) : GUIWindow(name, visible) {};
	virtual bool Draw() override;

};
