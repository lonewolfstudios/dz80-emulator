#include "GUI.h"
#include <examples/imgui_impl_glfw.h>
#include <examples/imgui_impl_opengl3.h>
#include "Windows/imgui_memory_editor.h"
#include "nfd.h"
#include "Windows/GUIWindows.h"
#include "IconsFontAwesome5.h"
#include "Input/InputManager.h"

static bool MemVisible = true;
static bool ScreenVisible = true;
static bool CPUDataVisible = true;
static bool DebuggerWindowVisible = true;
static bool BreakpointsVisible = true;
static bool DisassemblyVisible = true;
static bool VicControlVisible = true;

static Console* theConsole;
static GUIScreenWindow* screen;
static ImWchar ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };

ImGuiIO* io;

GUI* GUI::_instance = nullptr;

void GUI::InitializeGUI(GLFWwindow * window, Console* console)
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	io = &(ImGui::GetIO());
	io->ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	io->ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	ImGui::StyleColorsDark();
	io->Fonts->AddFontDefault();
	ImFontConfig config;
	config.MergeMode = true;
	iconFont = io->Fonts->AddFontFromFileTTF("fonts/fontawesome-webfont.ttf", 16.0f, &config, ranges);
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 150");
	theConsole = console;
	screen = new GUIScreenWindow("DZ80 Screen");
	screen->Initialize();
	screen->SetVisible(false);
	windows.push_back(screen);
	windows.push_back(new GUIBreakpointWindow("Breakpoints", true));
	windows.push_back(new GUIDebuggerWindow("Debugger", true));
	windows.push_back(new GUIMemoryWindow("Memory Editor", true));
	windows.push_back(new GUIProcessorStatusWindow("Processor Status", true));
	windows.push_back(new GUIDisassemblyWindow("Disassembly", true));
	windows.push_back(new GUISystemConfigStatusWindow("System Config", true));
}

void GUI::ShutdownGUI()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

bool GUI::DrawGUI()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("Save RAM"))
			{
				nfdchar_t* outPath = nullptr;
				nfdresult_t result = NFD_SaveDialog("bin", NULL, &outPath);
				if (result == NFD_OKAY)
				{
					FILE* fp = fopen(outPath, "wb");
					unsigned char data;
					for (int i = 0; i < 65536; ++i)
					{
						data = SystemBus::Instance()->AddressBus()->ReadAddress(i);
						fwrite((void*)&data, 1, sizeof(unsigned char), fp);
					}
					fflush(fp);
					fclose(fp);
				}
			}
			if (ImGui::MenuItem("Load RAM"))
			{
				nfdchar_t* outPath = nullptr;
				nfdresult_t result = NFD_OpenDialog("bin", NULL, &outPath);
				if (result == NFD_OKAY)
				{
					FILE* fp = fopen(outPath, "rb");
					unsigned char byte;
					for (int i = 0; i < 65536; ++i)
					{
						fread(&byte, sizeof(unsigned char), 1, fp);
						SystemBus::Instance()->AddressBus()->WriteAddress(i, byte);
					}
					fclose(fp);
				}
			}
			if (ImGui::MenuItem("Quit"))
				return true;
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Settings"))
		{
			if (ImGui::BeginMenu("Scale"))
			{
				if (ImGui::MenuItem("1x"))
					screen->SetScale(1);
				else if (ImGui::MenuItem("2x"))
					screen->SetScale(2);
				else if (ImGui::MenuItem("3x"))
					screen->SetScale(3);
				else if (ImGui::MenuItem("4x"))
					screen->SetScale(4);
				ImGui::EndMenu();
			}
			if (ImGui::BeginMenu("Input"))
			{
				if (ImGui::BeginMenu("Joystick"))
				{
					InputManager::Instance()->OnImGUIJoystickMenu();
					ImGui::EndMenu();
				}
				ImGui::EndMenu();
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Tools"))
		{
			if (ImGui::MenuItem("Randomize RAM"))
			{
				for (int i = 0; i < 65536; ++i)
					SystemBus::Instance()->AddressBus()->WriteAddress(i, rand() % 256);
			}
			if (ImGui::MenuItem("Load ROM"))
			{
				nfdchar_t* outPath = nullptr;
				nfdresult_t result = NFD_OpenDialog("bin", NULL, &outPath);
				if (result == NFD_OKAY)
				{
					Console::Instance()->LoadRom(outPath);
				}
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Windows"))
		{
			for (auto window : windows)
			{
				if (ImGui::MenuItem(window->Name()))
				{
					window->SetVisible(!window->IsVisible());
				}
			}
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}
	for (auto window : windows)
		if(window->Draw())
			return true;
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	if (io->ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		GLFWwindow* backup_current_context = glfwGetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		glfwMakeContextCurrent(backup_current_context);
	}
	return false;
}
