#include "GUIDisassemblyWindow.h"
#include "Utility/InstructionDecoder.h"

bool GUIDisassemblyWindow::Draw()
{
	if (!visible)
		return false;
	char decoded[20];
	char line[100];
	ImGui::Begin(name, &visible, ImGuiWindowFlags_AlwaysAutoResize);
	if (ImGui::Button("-"))
	{
		--currentAddress;
	}
	ImGui::SameLine();
	if (ImGui::ArrowButton("AddrUp", ImGuiDir_Up))
	{
		bool legal;
		bool undocumented;
		int cycles;
		int bytes;
		unsigned short currAddr = currentAddress;
		unsigned short minAddr = currAddr - 3;
		--currentAddress;
		InstructionDecoder::Instance()->Decode(decoded, currentAddress, cycles, bytes, undocumented, legal);
		while (currentAddress != minAddr && currentAddress + bytes != currAddr)
		{
			--currentAddress;
			InstructionDecoder::Instance()->Decode(decoded, currentAddress, cycles, bytes, undocumented, legal);
		}
	}
	for (int i = 0; i < 20; ++i)
	{
		ImGui::SameLine();
		ImGui::Spacing();
	}
	ImGui::SameLine();
	if (ImGui::ArrowButton("LinesDown", ImGuiDir_Down))
	{
		numLines--;
		if (numLines < 5) numLines = 5;
	}
	ImGui::SameLine();
	if (ImGui::ArrowButton("LinesUp", ImGuiDir_Up))
	{
		numLines++;
		if (numLines > 30) numLines = 30;
	}
	unsigned short addr = currentAddress;
	unsigned short offset;
	for (int i = 0; i < numLines; ++i)
	{
		bool legal;
		bool undocumented;
		int cycles;
		int bytes;
		InstructionDecoder::Instance()->Decode(decoded, addr, cycles, bytes, undocumented, legal);
		if (i == 0)
			offset = bytes;
		sprintf(line, "(%05d) $%04x: %s", addr, addr, decoded);
		ImVec4 color = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
		if (!legal)
			color = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
		if (undocumented)
			color = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
		ImGui::TextColored(color, line);
		addr += bytes;
	}
	if (ImGui::Button("+"))
	{
		++currentAddress;
	}
	ImGui::SameLine();
	if (ImGui::ArrowButton("AddrDown", ImGuiDir_Down))
	{
		currentAddress += offset;
	}
	ImGui::Spacing();
	ImGui::LabelText("Address:", "");
	ImGui::SameLine();
	sprintf(decoded, "%04x", currentAddress);
	ImGui::InputText("", decoded, 6, ImGuiInputTextFlags_CharsHexadecimal);
	currentAddress = (unsigned short)strtol(decoded, NULL, 16);
	ImGui::End();
	return false;
}