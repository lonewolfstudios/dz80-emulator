#include "GUIWindow.h"

class GUISystemConfigStatusWindow : public GUIWindow
{

private:

public:

	GUISystemConfigStatusWindow(char* name, bool visible = false) : GUIWindow(name, visible) {};
	virtual bool Draw() override;

};
