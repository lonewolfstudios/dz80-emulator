#pragma once

#include <imgui.h>
#include "Console.h"

class GUIWindow
{

protected:

	bool visible = true;
	char* name = nullptr;

public:

	GUIWindow(char* name, bool visible = false) : name(name), visible(visible) {};
	const char* Name() { return name; }
	bool IsVisible() { return visible; }
	void SetVisible(bool isVisible) { visible = isVisible; }
	virtual bool Draw() { return false; };

};
