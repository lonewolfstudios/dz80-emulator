#include "GUIWindow.h"

class GUIScreenWindow : public GUIWindow
{

private:

	void UpdateScreenTexture();
	bool FileMenu();
	void SettingsMenu();

	unsigned int screenTextureID;
	int screenWidth, screenHeight;
	int scale = 2;
	
public:

	GUIScreenWindow(char* name, bool visible = true) : GUIWindow(name, visible) {};
	void Initialize();
	void SetScale(int newScale) { scale = newScale; }
	virtual bool Draw() override;

};
