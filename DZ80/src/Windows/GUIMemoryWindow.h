#include "GUIWindow.h"
#include "imgui_memory_editor.h"

class GUIMemoryWindow : public GUIWindow
{

private:

	MemoryEditor memEdit;

public:

	GUIMemoryWindow(char* name, bool visible = false);
	virtual bool Draw() override;

};
