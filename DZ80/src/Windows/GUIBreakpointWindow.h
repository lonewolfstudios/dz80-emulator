#include "GUIWindow.h"

class GUIBreakpointWindow : public GUIWindow
{

private:

	int inputVerify(ImGuiInputTextCallbackData * data);
	void toLower(char * buf);
	bool containsHex(char * buf);
	int charIndex(char * buf, char chr);
	bool getAddress(char * buf, unsigned short & address);

public:

	GUIBreakpointWindow(char* name, bool visible = false) : GUIWindow(name, visible) {};
	virtual bool Draw() override;

};
