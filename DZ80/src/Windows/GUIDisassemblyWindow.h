#include "GUIWindow.h"

class GUIDisassemblyWindow : public GUIWindow
{

private:
	unsigned short currentAddress = 0;
	unsigned short gotoAddress = 0;
	int numLines = 10;

public:

	GUIDisassemblyWindow(char* name, bool visible = false) : GUIWindow(name, visible) {};
	virtual bool Draw() override;

};
