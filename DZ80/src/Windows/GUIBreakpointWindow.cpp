#include "GUIBreakpointWindow.h"

static int doVerify(ImGuiInputTextCallbackData* data)
{
	if (data->EventChar == '$' && strlen((char*)data->UserData) == 0)
		return 0;
	if ((data->EventChar >= '0' && data->EventChar <= '9') || (data->EventChar >= 'a' && data->EventChar <= 'f') || (data->EventChar >= 'A' && data->EventChar <= 'F'))
	{
		if (strlen((char*)data->UserData) == 5) //Prevent the user from typing more than 5 characters
			return 1;
		return 0;
	}
	else
		return 1;
}

bool GUIBreakpointWindow::Draw()
{
	Z80* z80 = Console::Instance()->CPU();
	if (!visible)
		return false;
	//Use a lambda to allow the class to be used
	auto verify = [this](ImGuiInputTextCallbackData* data) -> int { return inputVerify(data); };

	bool editable = z80->isSingleStep();
	static char address[10] = "";
	ImGui::Begin("Breakpoints", &visible, ImGuiWindowFlags_AlwaysAutoResize);
	if (editable)
	{
		ImGui::InputText("Address", address, 10, ImGuiInputTextFlags_CallbackCharFilter, doVerify, address);
		if (strlen(address) > 0)
			toLower(address);
		unsigned short addr;
		if (getAddress(address, addr))
		{
			if (ImGui::Button("Add"))
			{
				z80->AddExecuteBreakpoint(addr);
				address[0] = 0; //Clear the address field
			}
		}
		ImGui::Separator();
	}
	const std::vector<unsigned short>& breakpoints = z80->GetExecBreakpoints();
	int i = 0;
	for (unsigned short addr : breakpoints)
	{
		ImGui::PushID(i++);
		if (editable)
		{
			if (ImGui::Button("X"))
			{
				z80->RemoveExecuteBreakpoint(addr);
				ImGui::PopID();
				break;
			}
			ImGui::SameLine();
		}
		ImGui::Text("$%.4x (%.5d)", addr, addr);
		ImGui::PopID();
	}
	if (editable && ImGui::Button("Load"))
	{
		FILE* fp = fopen("breakpoints.save", "rb");
		if (fp != NULL)
		{
			z80->ClearExecuteBreakpoints();
			unsigned short size;
			fread((void*)&size, sizeof(size), 1, fp);
			for (int i = 0; i < size; ++i)
			{
				unsigned short addr = 0;
				fread((void*)&addr, sizeof(addr), 1, fp);
				z80->AddExecuteBreakpoint(addr);
			}
			fclose(fp);
		}
	}
	if (breakpoints.size() > 0)
	{
		if (editable)
			ImGui::SameLine();
		if (ImGui::Button("Save"))
		{
			FILE* fp = fopen("breakpoints.save", "wb");
			if (fp != NULL)
			{
				unsigned short size = (unsigned short)(breakpoints.size());
				fwrite((void*)(&size), sizeof(size), 1, fp);
				for (unsigned short addr : breakpoints)
				{
					fwrite((void*)&addr, sizeof(addr), 1, fp);
				}
				fflush(fp);
				fclose(fp);
			}
		}
	}
	ImGui::End();
	return false;
}

int GUIBreakpointWindow::inputVerify(ImGuiInputTextCallbackData* data)
{
	if (data->EventChar == '$' && strlen((char*)data->UserData) == 0)
		return 0;
	if ((data->EventChar >= '0' && data->EventChar <= '9') || (data->EventChar >= 'a' && data->EventChar <= 'f') || (data->EventChar >= 'A' && data->EventChar <= 'F'))
	{
		if (strlen((char*)data->UserData) == 5) //Prevent the user from typing more than 5 characters
			return 1;
		return 0;
	}
	else
		return 1;
}

void GUIBreakpointWindow::toLower(char* buf)
{
	for (int i = 0; i < strlen(buf); ++i)
		if (buf[i] >= 'A' && buf[i] <= 'F')
			buf[i] = buf[i] + 'a' - 'A';
}

bool GUIBreakpointWindow::containsHex(char* buf)
{
	for (int i = 0; i < strlen(buf); ++i)
		if (buf[i] == '$' || (buf[i] >= 'a' && buf[i] <= 'f'))
			return true;
	return false;
}

int GUIBreakpointWindow::charIndex(char* buf, char chr)
{
	for (int i = 0; i < strlen(buf); ++i)
		if (buf[i] == chr)
			return i;
	return -1;
}

bool GUIBreakpointWindow::getAddress(char* buf, unsigned short& address)
{
	if (strlen(buf) == 0)
	{
		address = 0;
		return false;
	}
	if (strlen(buf) == 1 && buf[0] == '$')
	{
		address = 0;
		return false;
	}
	char digits[] = "0123456789abcdef";
	int multiplier = containsHex(buf) ? 16 : 10;
	address = 0;
	for (int i = 0; i < strlen(buf); ++i)
	{
		address *= multiplier;
		address += charIndex(digits, buf[i]);
	}
	return true;
}
