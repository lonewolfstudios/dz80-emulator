#include "GUIProcessorStatusWindow.h"
#include "Utility/InstructionDecoder.h"
#include "Z80.h"

bool GUIProcessorStatusWindow::Draw()
{
	Z80* z80 = Console::CPU();
	if (!visible)
		return false;
	ImGui::Begin("Processor Status", &visible, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::Text("PC: %.4x (%.5d)  ", z80->PC(), z80->PC());
	ImGui::SameLine();
	ImGui::Text("SP: %.4x (%.5d)", z80->SP(), z80->SP());
	ImGui::SameLine();
	ImGui::Text("IX: %.4x (%.5d)", z80->IX(), z80->IX());
	ImGui::SameLine();
	ImGui::Text("IY: %.4x (%.5d)", z80->IY(), z80->IY());
	ImGui::Text("AF: %.4x (%.5d)   ", z80->AF(), z80->AF());
	ImGui::SameLine();
	ImGui::Text("BC: %.4x (%.5d)   ", z80->BC(), z80->BC());
	ImGui::SameLine();
	ImGui::Text("DE: %.4x (%.5d)   ", z80->DE(), z80->DE());
	ImGui::SameLine();
	ImGui::Text("HL: %.4x (%.5d)   ", z80->HL(), z80->HL());
	ImGui::Text("Processor Flags: ");
	if (z80->flag_S())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "S");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "S");
	ImGui::SameLine();
	if (z80->flag_Z())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "Z");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "Z");
	ImGui::SameLine();
	ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "-");
	ImGui::SameLine();
	if (z80->flag_H())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "H");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "H");
	ImGui::SameLine();
	ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "-");
	ImGui::SameLine();
	if (z80->flag_P())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "P");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "P");
	ImGui::SameLine();
	if (z80->flag_N())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "N");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "N");
	ImGui::SameLine();
	if (z80->flag_C())
		ImGui::TextColored(ImVec4(0.0f, 1.0f, 0.0f, 1.0f), "C");
	else
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "C");
	ImGui::Spacing();
	if (z80->Halted())
	{
		ImGui::TextColored(ImVec4(1.0f, 0.0f, 0.0f, 1.0f), "HALT");
		ImGui::Spacing();
	}
	ImGui::Text("Next Opcode");
	bool legal, undocumented;
	int cycles, bytes;
	char opcode[25];
	InstructionDecoder::Instance()->Decode(opcode, z80->PC(), cycles, bytes, undocumented, legal);
	ImGui::TextColored(legal ? undocumented ? ImVec4(0.0f, 1.0f, 1.0f, 1.0f) : ImVec4(0.0f, 1.0f, 0.0f, 1.0f) : ImVec4(1.0f, 0.0f, 0.0f, 1.0f),
		opcode);
	ImGui::Text("Total Cycles:");
	ImGui::Text("%.20llu", SystemBus::Instance()->CyclesElapsed());
	ImGui::End();
	return false;
}
