#include "GUIDebuggerWindow.h"
#include "GUI.h"
#include "IconsFontAwesome5.h"

bool GUIDebuggerWindow::Draw()
{
	Z80* z80 = Console::Instance()->CPU();
	if (!visible)
		return false;
	ImGui::Begin("Debugger", &visible, ImGuiWindowFlags_AlwaysAutoResize);
	if (!z80->LoggingEnabled())
	{
		if (ImGui::Button("Enable Logging"))
			z80->ActivateLogging();
	}
	else
	{
		if (ImGui::Button("Disable Logging"))
			z80->DeactivateLogging();
	}
	ImGui::PushFont(GUI::Instance()->IconFont());
	if (ImGui::Button(ICON_FA_UNDO, ImVec2(24, 24)))
		z80->Reset(); //Restart the CPU
	ImGui::SameLine();
	if (z80->isSingleStep())
	{
		if (ImGui::Button(ICON_FA_PLAY, ImVec2(24, 24)))
		{
			z80->DoSingleStep(); //First, single step to get past the possible current breakpoint
			z80->CancelSingleStep(); //Then start running again
		}
	}
	else
	{
		if (ImGui::Button(ICON_FA_PAUSE, ImVec2(24, 24)))
			z80->SingleStep();
	}
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_ANGLE_RIGHT, ImVec2(24, 24)))
		lastCycles = z80->DoSingleStep();
	ImGui::PopFont();
	ImGui::Text("Last Cycles Used: %d", lastCycles);
	ImGui::End();
	return false;
}