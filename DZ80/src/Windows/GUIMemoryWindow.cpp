#include "GUIMemoryWindow.h"
#include "SystemBus/SystemBus.h"

static unsigned char ReadFromBus(const unsigned char* pointless, size_t addr)
{
	return SystemBus::Instance()->AddressBus()->ReadAddress((unsigned short)addr);
}

static void WriteToBus(unsigned char* pointless, size_t addr, unsigned char data)
{
	SystemBus::Instance()->AddressBus()->WriteAddress((unsigned short)addr, data);
}

static bool Highlight(const unsigned char* pointless, size_t addr)
{
	return false;
}

GUIMemoryWindow::GUIMemoryWindow(char* name, bool visible) : GUIWindow(name, visible)
{
	memEdit.ReadFn = ReadFromBus;
	memEdit.WriteFn = WriteToBus;
	memEdit.HighlightFn = Highlight;
}

bool GUIMemoryWindow::Draw()
{
	if (!visible)
		return false;
	memEdit.DrawWindow(name, NULL, 65535, visible);
	return false;
}