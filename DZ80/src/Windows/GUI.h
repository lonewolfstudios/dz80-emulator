#include <glad/glad.h>
#include <imgui.h>
#include <GLFW/glfw3.h>
#include "Console.h"
#include "Windows/GUIWindow.h"
#include <vector>

class GUI
{

private:

	static GUI* _instance;
	GUI() = default;

	std::vector<GUIWindow*> windows;
		
	ImFont* iconFont;

public:

	static GUI* Instance() { if (_instance == nullptr) _instance = new GUI(); return _instance; }

	ImFont* IconFont() { return iconFont; }
	void InitializeGUI(GLFWwindow* window, Console* theConsole);
	void ShutdownGUI();
	bool DrawGUI();

};
