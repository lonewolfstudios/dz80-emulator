#pragma once

#include <fstream>

class ifstream : public std::ifstream
{

public:

	unsigned long length()
	{
		//This is C++'s stupid way of getting a file size
		auto currentPos = this->tellg();
		this->ignore(std::numeric_limits<std::streamsize>::max());
		auto length = this->gcount();
		this->clear();
		this->seekg(currentPos, std::ios::beg);
		return length;
	}
};
