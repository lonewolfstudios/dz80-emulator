workspace "DZ80"
	architecture "x64"
	startproject "DZ80"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}
IncludeDir["GLFW"] = "DZ80/submodules/GLFW/include"
IncludeDir["Glad"] = "DZ80/dependencies/Glad/include"
IncludeDir["ImGui"] = "DZ80/submodules/imgui"
IncludeDir["nativefiledialog"] = "DZ80/submodules/nativefiledialog/src/include"

group "Dependencies"
	include "DZ80/submodules/GLFW"
	include "DZ80/dependencies/Glad"
	include "DZ80/submodules/imgui"
	include "DZ80/submodules/nativefiledialog/build"
group ""

project "DZ80"
	location "DZ80"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

	
	targetdir = "bin\\" .. outputdir .. "\\%{prj.name}"
	debugdir(targetdir)

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
	}

	defines
	{
	}

	includedirs
	{
		"%{prj.name}/src",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.Glad}",
		"%{IncludeDir.ImGui}",
		"%{IncludeDir.nativefiledialog}"
	}

	--libdirs "libraries/libsoundio/Windows/x86_64"

	links 
	{ 
		"GLFW",
		"Glad",
		"ImGui",
		"nfd",
		"opengl32.lib",
		"comctl32.lib"
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{	
			"_CRT_SECURE_NO_WARNINGS"
		}

	filter "system:macosx"
		systemversion "latest"
		
		defines
		{
			"_OSX_"
		}
		
	filter "configurations:Debug"
		runtime "Debug"
		symbols "on"

	filter "configurations:Release"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		runtime "Release"
		optimize "on"

	configuration "windows"
		postbuildcommands
		{
			"if not exist ..\\\"%{targetdir}\\fonts\" mkdir ..\\\"%{targetdir}\\fonts\"",
			"copy src\\Fonts\\* ..\\\"%{targetdir}\\Fonts\\*\"",
			"copy src\\rom.bin ..\\\"%{targetdir}\""
		}
